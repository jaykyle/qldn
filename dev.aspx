﻿
<!DOCTYPE html>
<html lang="en" data-ng-app="app">

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="HoaDev">
    <meta name="keyword" content="ICT24H,sharepoint,sharepoint app">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="template/assets/images/favicon.png">
    <title data-ng-bind="'ICT24H >' + pageTitle"></title>

    <link rel="stylesheet" href="https://static2.sharepointonline.com/files/fabric/office-ui-fabric-core/9.6.1/css/fabric.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    
   
    <!-- Custom CSS -->
    <link href="template/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="template/css/colors/blue.css" id="theme" rel="stylesheet">
    

    <script>
        var cdnUrl = "http://127.0.0.1:5500/";
        var sharepointCustomerUrl = "https://i24h.sharepoint.com/sites/bxiu/";
    </script>
    <style>
        body {background-color: #1e88e5}
        /** Đẩy thông báo cách thanh Nav trên Top ra*/
        .toast-top-right { top: 80px !important; }
        /** Xóa kích thước font của icon Google */
        .material-icons { font-size: inherit !important; }
        

        /* Đọa CSS đẩy bảng dọc xuống
         @media screen and (max-width: 600px) {
            table {width:100%;}
            thead {display: none;}
            tr:nth-of-type(2n) {background-color: inherit;}
            tr td:first-child { font-weight:normal;font-size:1.3em;}
            tbody td {display: block;  text-align:left;}
            tbody td:before { 
                content: attr(data-th); 
                display: block;
                text-align:left;  
            font-weight: normal;
            }
            tbody td::before {
            content: attr(data-th);
            display: none;
            }
        } */

        /*Hiệu ứng rating bằng sao*/
@media only screen and (max-width:600px){
	div.stars {
		width:80% !important;
		display: inline-block !important;
	  }
  } 
div.stars {
	width:60% !important;
	display: inline-block !important;
  }

  
  input[type=radio].star { display: none !important; }
  
  label.star {
	float: right !important;
	 padding: 5px !important; 
	font-size: 36px !important;
	color: #444 !important;
	transition: all .2s ;
  }
  
  input[type=radio].star:checked ~ label.star:before {
	content: '\2605' !important;
	color: #FD4 !important;
	transition: all .25s ;
  }
  
  input[type=radio].star-5:checked ~ label.star:before {
	color: #FE7 !important;
	text-shadow: 0 0 20px #952 !important;
  }
  
  input[type=radio].star-1:checked ~ label.star:before { color: #F62 !important; }
  
  label.star:hover { transform: rotate(-15deg) scale(1.3); }
  
  label.star:before {
	content: '\2605' !important;
	}
    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <!-- <div class="circular"> 
            <div class="custom-preload-bar">
                    <div class="custom-preload-circle"></div>
                    <p class="custom-preload-p">Loading</p>
                </div>
        </div> -->
        <div class="custom-preload-bar">
            <div class="custom-preload-circle"></div>
            <p style="color:#fff">Đang tải...</p>
            <!-- <p class="custom-preload-p">Loading</p> -->
            <img class="custom-preload-p" src="https://ict24h.cdn.hoadev.com/images/logo.png" height="20px">
        </div>
    </div>
    <!-- <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div> -->
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" /> -->
                            <!-- Light Logo icon -->
                            <img src="template/assets/images/logo_ict24h_icon.png" height="30" alt="homepage" class="light-logo" id="logo-icon" />
                        
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                            <!-- dark Logo text -->
                            <!-- <img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" /> -->
                            <!-- Light Logo text -->
                            <img src="template/assets/images/logo_ict24h_50.png" height="30" alt="homepage" class="light-logo"/></span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="fa fa-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i
                                        class="ti-close"></i></a> </form>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
                                href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-th-large"></i></a>
                            <div class="dropdown-menu scale-up-left">
                                <ul class="mega-dropdown-menu row">
                                    <li class="col-lg-3 col-xlg-2 m-b-30">
                                        <h4 class="m-b-20">CAROUSEL</h4>
                                        <!-- CAROUSEL -->
                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner" role="listbox">
                                                <div class="carousel-item active">
                                                    <div class="container">
                                                        <!-- <img class="d-block img-fluid" src="../assets/images/big/img1.jpg"
                                                            alt="First slide">
                                                        </div> -->
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container">
                                                        <!-- <img class="d-block img-fluid" src="../assets/images/big/img2.jpg" alt="Second slide"> -->
                                                        </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container">
                                                        <!-- <img class="d-block img-fluid" src="../assets/images/big/img3.jpg" alt="Third slide"> -->
                                                        </div>
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                                data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span> </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                                data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span> </a>
                                        </div>
                                        <!-- End CAROUSEL -->
                                    </li>
                                    <li class="col-lg-3 m-b-30">
                                        <h4 class="m-b-20">ACCORDION</h4>
                                        <!-- Accordian -->
                                        <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                                            aria-expanded="true" aria-controls="collapseOne">
                                                            Collapsible Group Item #1
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseOne" class="collapse show" role="tabpanel"
                                                    aria-labelledby="headingOne">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim
                                                        eiusmod high. </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            Collapsible Group Item #2
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim
                                                        eiusmod high life accusamus terry richardson ad squid. </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            Collapsible Group Item #3
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseThree" class="collapse" role="tabpanel"
                                                    aria-labelledby="headingThree">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim
                                                        eiusmod high life accusamus terry richardson ad squid. </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-lg-3  m-b-30">
                                        <h4 class="m-b-20">CONTACT US</h4>
                                        <!-- Contact -->
                                        <form>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputname1"
                                                    placeholder="Enter Name"> </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Enter email">
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" id="exampleTextarea" rows="3"
                                                    placeholder="Message"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </form>
                                    </li>
                                    <li class="col-lg-3 col-xlg-4 m-b-30">
                                        <h4 class="m-b-20">List style</h4>
                                        <!-- List style -->
                                        <ul class="list-style-none">
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                    You can give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                    Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                    Another Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                    Forth link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i>
                                                    Another fifth link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href=""
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new
                                                        admin!</span> <span class="time">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that
                                                        you have event</span> <span class="time">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this
                                                        template as you want</span> <span class="time">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                                    <span class="time">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all
                                                notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-email"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox dropdown-menu-right scale-up" aria-labelledby="2">
                                <ul>
                                    <li>
                                        <div class="drop-title">You have 4 new messages</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="user-img">
                                                    <!-- <img src="../assets/images/users/1.jpg" alt="user"
                                                        class="img-circle"> <span class="profile-status online pull-right"> -->

                                                        </span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                                    <span class="time">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="user-img"> 
                                                    <!-- <img src="../assets/images/users/2.jpg" alt="user"
                                                        class="img-circle">  -->
                                                        <span class="profile-status busy pull-right"></span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See
                                                        you at</span> <span class="time">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="user-img">
                                                     <!-- <img src="../assets/images/users/3.jpg" alt="user"
                                                        class="img-circle">  -->
                                                        <span class="profile-status away pull-right">

                                                        </span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span>
                                                    <span class="time">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="user-img"> 
                                                    <!-- <img src="../assets/images/users/4.jpg" alt="user"
                                                        class="img-circle"> -->
                                                         <span class="profile-status offline pull-right"></span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                                    <span class="time">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all
                                                e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"><img ng-src="/_layouts/15/userphoto.aspx?size=L&accountname={{$root.CurrentUser.Email}}" alt="User image" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img ng-src="/_layouts/15/userphoto.aspx?size=L&accountname={{$root.CurrentUser.Email}}" alt="{{$root.CurrentUser.Title}}"></div>
                                            <div class="u-text">
                                                <h4 ng-bind="$root.CurrentUser.Title"></h4>
                                                <p class="text-muted" ng-bind="$root.CurrentUser.Email"></p><a href="https://portal.office.com/account/#personalinfo" class="btn btn-rounded btn-danger btn-sm" target="_blank">View Profile</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="https://portal.office.com/account/" target="_blank"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="https://portal.office.com/account/#subscriptions" target="_blank"><i class="ti-wallet"></i> My Subscriptions</a></li>
                                    <li><a href="https://outlook.office365.com/owa/" target="_blank"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="https://portal.office.com/account/#settings" target="_blank"><i class="ti-settings"></i> Account Setting</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="./_layouts/closeConnection.aspx?loginasanotheruser=true"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"> <i class="ms-Icon ms-Icon--WaffleOffice365" title="Office 365" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <a class="dropdown-item" href="https://outlook.office365.com/owa/" target="_blank">
                                    <i class="ms-Icon ms-Icon--OutlookLogo" title="Outlook" aria-hidden="true" style="color: rgb(0, 120, 215);"></i>
                                    Outlook
                                </a>
                                <a class="dropdown-item" href="https://outlook.office365.com/owa/?exsvurl=1&ll-cc=1033&modurl=1" target="_blank">
                                    <i class="ms-Icon ms-Icon--CalendarWorkWeek" title="Calendar" aria-hidden="true" style="color: rgb(0, 120, 215);"></i>
                                    Calendar
                                </a>
                                <a class="dropdown-item" href="https://www.office.com/launch/word" target="_blank">
                                    <i class="ms-Icon ms-Icon--WordLogo" title="Microsoft Word" aria-hidden="true" style="color: rgb(43, 87, 154);"></i>
                                    Word
                                </a>
                                <a class="dropdown-item" href="https://www.office.com/launch/excel" target="_blank">
                                    <i class="ms-Icon ms-Icon--ExcelLogo" title="Microsoft Excel" aria-hidden="true" style="color: rgb(33, 115, 70);"></i>
                                    Excel
                                </a>
                                <a class="dropdown-item" href="https://www.office.com/launch/powerpoint" target="_blank">
                                    <i class="ms-Icon ms-Icon--PowerPointLogo" title="Microsoft PowerPoint" aria-hidden="true" style="color: rgb(183, 71, 42);"></i>
                                    PowerPoint
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar d-print-none">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <!-- <div class="user-profile" style="background: url(../assets/images/background/user-info.jpg) no-repeat;">
                    <div class="profile-img"> <img ng-src="/_layouts/15/userphoto.aspx?size=S&accountname={{$root.CurrentUser.Email}}" alt="{{$root.CurrentUser.Title}}" /> </div>
                    <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="true">Markarn Doe</a>
                        <div class="dropdown-menu animated flipInY"> <a href="#" class="dropdown-item"><i class="ti-user"></i>
                                My Profile</a> <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                            <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                            <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i>
                                Account Setting</a>
                            <div class="dropdown-divider"></div> 
                            <a href="login.html" class="dropdown-item">
                                <i class="fa fa-power-off"></i>
                                Logout
                            </a>
                        </div>
                    </div>
                </div> -->
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav"></nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="https://outlook.office365.com/owa/" target="_blank" class="link" data-toggle="tooltip" title="Email"><i class="material-icons">mail_outline</i></a>
                <!-- item--><a href="https://portal.office.com/account/#settings" class="link" data-toggle="tooltip" target="_blank" title="Account Settings"><i class="material-icons">calendar_today</i></a>
                <!-- item--><a href="./_layouts/closeConnection.aspx?loginasanotheruser=true" class="link" data-toggle="tooltip" title="Logout"><i class="material-icons">power_settings_new</i></a>
            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" data-ng-include="'App/layout/shell.html'">
               
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-right">
                    © 2019 Sharepoint Dev by <span class="align-top"> <img class="align-top" src="https://ict24h.cdn.hoadev.com/images/logo.png" height="20px"></span>
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.5/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="template/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="template/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="template/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="template/js/custom.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->


    <!-- Add Angular Ver 1.5.11 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular-cookies.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular-route.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular-resource.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular-sanitize.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.11/angular-animate.js"></script>

  <!-- jQuery Util -->
  <script src="http://127.0.0.1:5500/App/util/jquery-extensions.js"></script>
  <!-- <script src="App/util/js-polyfill.js"></script> -->
  <!-- app bootstrapping (used to get the app going) -->
  <script src="http://127.0.0.1:5500/App/app.js"></script>
  <script src="http://127.0.0.1:5500/App/config.js"></script>
  <script src="http://127.0.0.1:5500/App/config.angular.http.js"></script>
  <script src="http://127.0.0.1:5500/App/config.exceptionHandler.js"></script>
  <script src="http://127.0.0.1:5500/App/config.route.js"></script>

  <!-- common modules -->
  <script src="http://127.0.0.1:5500/App/common/common.js"></script>
  <script src="http://127.0.0.1:5500/App/common/logger.js"></script>

  <!-- app core -->
  <!-- <script src="/App/layout/workingonit.js"></script>
  <script src="/App/layout/spAppChrome.js"></script>-->

  <script src="http://127.0.0.1:5500/App/models/ict24h.js"></script>
  <script src="http://127.0.0.1:5500/App/layout/header.js"></script>
  <script src="http://127.0.0.1:5500/App/layout/quicklaunch.js"></script>
  <script src="http://127.0.0.1:5500/App/layout/shell.js"></script>


  <!-- app services -->
  <script src="http://127.0.0.1:5500/App/services/spContext.js"></script>
  <script src="http://127.0.0.1:5500/App/services/datacontext.angular.js"></script>
  <!-- <script src="App/services/currentUser.js"></script> -->
  <!-- Custom filter -->
  <script src="http://127.0.0.1:5500/App/filter/percent-filter.js"></script>
  <script src="http://127.0.0.1:5500/App/filter/props-filter.js"></script>
  <!-- Module Upload File -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.2.13/ng-file-upload-all.min.js"></script> -->

  <script src="http://127.0.0.1:5500/Scripts/angular-file-upload/dist/angular-file-upload.min.js"></script>
  <script src="http://127.0.0.1:5500/App/services/attachFiles.js"></script>
  <script src="http://127.0.0.1:5500/App/services/itemCount.js"></script>
  <script src="http://127.0.0.1:5500/App/services/currentUser.js"></script>

  <!-- Custom directives -->

  <!-- app controllers -->
  <script src="http://127.0.0.1:5500/App/dashboard/dashboard.js"></script>
  <script src="http://127.0.0.1:5500/App/controllers/Users/currentUser.js"></script>

  <!-- Dự án -->
  <script src="http://127.0.0.1:5500/App/controllers/DuAn/ctDuAn.js"></script>
  <script src="http://127.0.0.1:5500/App/controllers/DuAn/dsDuAn.js"></script>
    <!-- *
   Models *
   -->
  <!-- Cấp công trình -->
  <script src="http://127.0.0.1:5500/App/models/QuanLyDoanhNghiep.js"></script>
  <!-- *
   Controller chung *
   -->
  <script src="http://127.0.0.1:5500/App/controllers/QuanLyDoanhNghiep/dsQuanLyDoanhNghiep.js"></script>
   <script src="http://127.0.0.1:5500/App/controllers/QuanLyDoanhNghiep/ctQuanLyDoanhNghiep.js"></script>


  

  <!-- Start ulti plugin -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js"></script>

  <!-- Hiển thị thông báo -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" rel="stylesheet"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
  <script>
    // Cấu hình mặc định cho thông báo
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "positionClass" : "toast-top-right"
    }
  </script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script> -->


  <!-- angular-ui-select: Select 2 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.20.0/select.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.20.0/select.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">


  <!-- angular-ui-datetime-picker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-ui-datetime-picker/2.6.3/datetime-picker.min.js"></script>

  <!-- Angular Moment directive and filters  -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment-with-locales.min.js"></script>

  <!-- Thêm lớp overlayloading -->
  <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.5/dist/loadingoverlay.min.js"></script>

  <!-- Thêm bản đồ -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-chart.js/1.1.1/angular-chart.min.js"></script>

  <!-- Ladda BTN loading -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda-themeless.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/spin.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ladda/0.4.3/angular-ladda.min.js"></script>

  <!-- Format input form -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.3.7/cleave-angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.3.7/addons/cleave-phone.vn.js"></script>

  <!-- Font biểu tượng -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

  <link rel="stylesheet" href="https://rawgit.com/ytlabs/ng-slim-scroll/master/ng-slim-scroll.css" />
  <script src="https://rawgit.com/ytlabs/ng-slim-scroll/master/ng-slim-scroll.js"></script>

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/silver/pace-theme-minimal.min.css" />

  <!-- Chèn mã tự reload lại khi lưu -->
<script type="text/javascript">
    // <![CDATA[  <-- For SVG support
    if ('WebSocket' in window) {
        (function () {
            function refreshCSS() {
                var sheets = [].slice.call(document.getElementsByTagName("link"));
                var head = document.getElementsByTagName("head")[0];
                for (var i = 0; i < sheets.length; ++i) {
                    var elem = sheets[i];
                    head.removeChild(elem);
                    var rel = elem.rel;
                    if (elem.href && typeof rel != "string" || rel.length == 0 || rel.toLowerCase() == "stylesheet") {
                        var url = elem.href.replace(/(&|\?)_cacheOverride=\d+/, '');
                        elem.href = url + (url.indexOf('?') >= 0 ? '&' : '?') + '_cacheOverride=' + (new Date().valueOf());
                    }
                    head.appendChild(elem);
                }
            }
            var protocol = window.location.protocol === 'http:' ? 'ws://' : 'wss://';
            var address = 'ws://127.0.0.1:5500/ws';
            var socket = new WebSocket(address);
            socket.onmessage = function (msg) {
                if (msg.data == 'reload') window.location.reload();
                else if (msg.data == 'refreshcss') refreshCSS();
            };
            if (sessionStorage && !sessionStorage.getItem('IsThisFirstTime_Log_From_LiveServer')) {
                console.log('Live reload enabled.');
                sessionStorage.setItem('IsThisFirstTime_Log_From_LiveServer', true);
            }
        })();
    }
    else {
        console.error('Upgrade your browser. This Browser is NOT supported WebSocket for Live-Reloading.');
    }
    // ]]>
</script>
<link href="template/css/custom.css" id="theme-custom" rel="stylesheet">

   
</body>

</html>