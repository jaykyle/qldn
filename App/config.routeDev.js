﻿(function () {
    'use strict';

    var app = angular.module('app');

    // get all the routes
    app.constant('routes', getRoutes());

    // config routes & their resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);

    function routeConfigurator($routeProvider, routes) {
        routes.forEach(function (route) {
            $routeProvider.when(route.url, route.config);
        });

        $routeProvider.otherwise({ redirectTo: '/' });
    }
    

    // build the routes
    function getRoutes() {
        const localUrl = "http://127.0.0.1:5500/";
        return [
            {
                url: '/',
                config: {
                    templateUrl: localUrl + 'App/dashboard/dashboard.html',
                    title: 'dashboard',
                    settings: {
                        nav: 0,
                        content: 'dashboard',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/saler',
                config: {
                    templateUrl: localUrl + 'App/controllers/Saler/dsSaler.html',
                    title: 'Nhân viên bán hàng',
                    settings: {
                        nav: 1,
                        content: 'Nhân viên bán hàng',
                        quickLaunchEnabled: true
                    }
                }
            },
            {
                url: '/saler/:idSaler',
                config: {
                    templateUrl: localUrl + 'App/controllers/Saler/ctSaler.html',
                    title: 'Nhân viên bán hàng',
                    settings: {
                        nav: 1.1,
                        content: 'Nhân viên bán hàng',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/modelsanpham',
                config: {
                    templateUrl: localUrl + 'App/controllers/ModelSanPham/dsModelSanPham.html',
                    title: 'Model sản phẩm',
                    settings: {
                        nav: 1.1,
                        content: 'Model sản phẩm',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/modelsanpham/:idModelSanPham',
                config: {
                    templateUrl: localUrl + 'App/controllers/ModelSanPham/ctModelSanPham.html',
                    title: 'Model sản phẩm',
                    settings: {
                        nav: 1.1,
                        content: 'Model sản phẩm',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/banhang',
                config: {
                    templateUrl: localUrl + 'App/controllers/BanHang/dsBanHang.html',
                    title: 'Danh sách phiếu bán hàng',
                    settings: {
                        nav: 2,
                        content: 'Danh sách phiếu bán hàng',
                        quickLaunchEnabled: true
                    }
                }
            },
            {
                url: '/banhang/them',
                config: {
                    templateUrl: localUrl + 'App/controllers/BanHang/themBanHang.html',
                    title: 'Tạo phiếu bán hàng',
                    settings: {
                        nav: 2.1,
                        content: 'Tạo phiếu bán hàng',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/banhang/:idBanHang/xem',
                config: {
                    templateUrl: localUrl + 'App/controllers/BanHang/xemBanHang.html',
                    title: 'Phiếu bán hàng',
                    settings: {
                        nav: 2.1,
                        content: 'Phiếu bán hàng',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/banhang/:idBanHang/sua',
                config: {
                    templateUrl: localUrl + 'App/controllers/BanHang/suaBanHang.html',
                    title: 'Phiếu bán hàng',
                    settings: {
                        nav: 2.1,
                        content: 'Phiếu bán hàng',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/yeucau',
                config: {
                    templateUrl: localUrl + 'App/controllers/YeuCau/dsYeuCau.html',
                    title: 'Danh sách yêu cầu',
                    settings: {
                        nav: 2.1,
                        content: 'Danh sách yêu cầu',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/yeucau/them',
                config: {
                    templateUrl: localUrl + 'App/controllers/YeuCau/themYeuCau.html',
                    title: 'thêm Danh sách yêu cầu',
                    settings: {
                        nav: 2.1,
                        content: 'thêm Danh sách yêu cầu',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/yeucau/:idYeuCau/sua',
                config: {
                    templateUrl: localUrl + 'App/controllers/YeuCau/ctYeuCau.html',
                    title: 'thêm Danh sách yêu cầu',
                    settings: {
                        nav: 2.1,
                        content: 'thêm Danh sách yêu cầu',
                        quickLaunchEnabled: false
                    }
                }
            },
            {
                url: '/checkmoney',
                config: {
                    templateUrl: localUrl + 'App/controllers/CheckMoney/CheckMoney.html',
                    title: 'Kiểm tra tiền đã nhận từ khách hàng',
                    settings: {
                        nav: 2.1,
                        content: 'Kiểm tra tiền đã nhận từ khách hàng',
                        quickLaunchEnabled: false
                    }
                }
            }
            
            



        ];
    }
})();