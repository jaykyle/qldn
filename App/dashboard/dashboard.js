﻿(function () {
  'use strict';

  // define controller
  var controllerId = 'dashboard';
  angular.module('app').controller(controllerId,
    ['common', '$location','ItemCount' , dashboard]);

  // init controller
  function dashboard(common, $location, ItemCount) {
    var vm = this;
      var logger = common.logger;
      vm.vars = {
          welcome: 'Chào mừng đến với ICT24H App'
      }
    const listName = "NhanVien";

    // init controller
    init();

    // init controller
    function init() {
      logger.log("controller loaded", null, controllerId);
      common.activateController([], controllerId);
      
      //$location.path('/banhang/');
      //getAllNhanVien();

    }

    // Thống kê tổng nhân viên
    function getAllNhanVien() {


      const select = undefined;
      const expand = undefined; //'NhanVien,PhongBan';
      const filter = undefined;

      // Tham số: listName, select, expand, filter
      ItemCount.getSpCount(listName, null, null, null)
          .then(function (data) {
              if (data) {
                  vm.dsTinhThanh = data;
              } else {
                  throw new Error('Lỗi không thể lấy danh sách tỉnh thành');
              }
          }).catch(function (error) {
              common.logger.logError('Lỗi danh sách tỉnh thành', error, controllerId);
          });
    }
    

  }
})();