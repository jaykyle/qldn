﻿(function () {
  'use strict';

  var app = angular.module('app');

  var events = {
    // event when the controller has been successfully activated
    controllerActivateSuccess: 'controller.activateSuccess',
    // event when to toggle the working on it dialog
    workingOnItToggle: 'workingonit.toggle',
    // event when the security digest has been obtained and/or refreshed
    securityDigestRefreshed: 'spContext.digestRefreshed'
  };

  var config = {
    title: 'ICT24H Sharepoint App',

    // config the exceptionHandler decorator
    appErrorPrefix: '[SYSERR] ',
    // app events
    events: events,
    // app version
    version: '0.0.0.9',
    // debug notification settings
    showDebugNotiSetting: false
  };

  // create a global variable on app called 'config'
  app.value('config', config);

  // configure the angular logging service before startup
  app.config(['$logProvider', function ($logProvider) {
    // turn debugging off/on (no info or warn)
    if ($logProvider.debugEnabled) {
      $logProvider.debugEnabled(true);
    }
  }]);

  // configure the common configuration
  app.config(['commonConfigProvider', function (cfg) {
    // setup events
    cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
    cfg.config.workingOnItToggleEvent = config.events.workingOnItToggle;
    cfg.config.spContextSecurityDigestRefreshedEvent = config.events.securityDigestRefreshed;
  }]);

  // Cấu hình cho phép tải CDN từ bên ngoài
  app.config(function ($sceDelegateProvider) {
		$sceDelegateProvider.resourceUrlWhitelist([
			// Allow same origin resource loads.
			'self',
			// Allow loading from our assets domain.  Notice the difference between * and **.
      'https://*.netlify.com/**',
      'https://ict24h.cdn.hoadev.com/**',
      'http://127.0.0.1:5500/**',
      'http://localhost:8080/**',
		]);

		// The blacklist overrides the whitelist so the open redirect here is blocked.
		$sceDelegateProvider.resourceUrlBlacklist([
			'https://myapp.example.com/clickThru**'
    ]);
    
  });

  //Bổ sung cấu hình cho ui-select
  var uis = angular.module('ui.select', [])
  .constant('uiSelectConfig', {
    closeOnSelect: false // Tắt tính năng tắt sau khi chọn
  })


})();