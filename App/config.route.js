﻿(function () {
  'use strict';

  var app = angular.module('app');

  // get all the routes
  app.constant('routes', getRoutes());

  // config routes & their resolvers
  app.config(['$routeProvider', 'routes', routeConfigurator]);

  function routeConfigurator($routeProvider, routes) {
    routes.forEach(function (route) {
      $routeProvider.when(route.url, route.config);
    });

    $routeProvider.otherwise({ redirectTo: '/' });
  }

  // build the routes
  function getRoutes() {
    return [
      {
        url: '/',
        config: {
          templateUrl: cdnUrl + 'App/controllers/DuAn/dsDuAn.html',//'App/dashboard/dashboard.html',
          title: 'dashboard',
          settings: {
            nav: 0,
            content: 'dashboard',
            icon: 'dashboard',
            quickLaunchEnabled: false
          }
        }
      },{
        url: '/duan',
        config: {
          templateUrl: cdnUrl + 'App/controllers/DuAn/dsDuAn.html',
          title: 'Dự án',
          settings: {
            nav: 1,
            content: 'Dự án',
            icon: 'folder_special',
            quickLaunchEnabled: true
          }
        }
      },
      {
        url: '/duan/:idDuAn/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/DuAn/ctDuAn.html',
          title: 'Thông tin dự án',
          settings: {
            nav: 1,
            content: 'Thông tin dự án',
            icon: 'folder_special',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/duan/:idDuAn/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/DuAn/xemDuAn.html',
          title: 'Thông tin dự án',
          settings: {
            nav: 1,
            content: 'Thông tin dự án',
            icon: 'folder_special',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/hosovanban',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HoSoVanBan/dsHoSoVanBan.html',
          title: 'Hồ sơ văn bản',
          settings: {
            nav: 1,
            content: 'Hồ sơ văn bản',
            icon: 'all_inbox',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/hosovanban/:idHoSoVanBan/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HoSoVanBan/ctHoSoVanBan.html',
          title: 'Hồ sơ văn bản',
          settings: {
            nav: 1,
            content: 'Hồ sơ văn bản',
            icon: 'all_inbox',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/hosovanban/:idHoSoVanBan/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HoSoVanBan/xemHoSoVanBan.html',
          title: 'Hồ sơ văn bản',
          settings: {
            nav: 1,
            content: 'Hồ sơ văn bản',
            icon: 'all_inbox',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/canhan',
        config: {
          templateUrl: cdnUrl + 'App/controllers/CaNhan/dsCaNhan.html',
          title: 'Quản lý cá nhân',
          settings: {
            nav: 3,
            content: 'Quản lý cá nhân',
            icon: 'account_box',
            quickLaunchEnabled: true
          }
        }
      },
      {
        url: '/canhan/:idCaNhan/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/CaNhan/ctCaNhan.html',
          title: 'Quản lý cá nhân',
          settings: {
            nav: 1,
            content: 'Quản lý cá nhân',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/canhan/:idCaNhan/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/CaNhan/xemCaNhan.html',
          title: 'Quản lý cá nhân',
          settings: {
            nav: 1,
            content: 'Quản lý cá nhân',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/chiphi',
        config: {
          templateUrl: cdnUrl + 'App/controllers/ChiPhi/dsChiPhi.html',
          title: 'Chi phí, dự toán',
          settings: {
            nav: 4,
            content: 'Chi phí, dự toán',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/chiphi/:idChiPhi/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/ChiPhi/ctChiPhi.html',
          title: 'Chi tiết chi phí',
          settings: {
            nav: 1,
            content: 'Chi tiết chi phí',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/chiphi/:idChiPhi/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/ChiPhi/xemChiPhi.html',
          title: 'Chi tiết chi phí',
          settings: {
            nav: 1,
            content: 'Chi tiết chi phí',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/loaichiphiduan',
        config: {
          templateUrl: cdnUrl + 'App/controllers/LoaiChiPhiDuAn/dsLoaiChiPhiDuAn.html',
          title: 'Loại chi phí',
          settings: {
            nav: 5,
            content: 'Loại chi phí',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/loaichiphiduan/:idLoaiChiPhiDuAn/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/LoaiChiPhiDuAn/suaLoaiChiPhiDuAn.html',
          title: 'Chi tiết loại chi phí',
          settings: {
            nav: 1,
            content: 'Chi tiết loại chi phí',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/loaichiphiduan/:idLoaiChiPhiDuAn/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/LoaiChiPhiDuAn/xemLoaiChiPhiDuAn.html',
          title: 'Chi tiết loại chi phí',
          settings: {
            nav: 1,
            content: 'Chi tiết loại chi phí',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/loaichiphiduan/them',
        config: {
          templateUrl: cdnUrl + 'App/controllers/LoaiChiPhiDuAn/themLoaiChiPhiDuAn.html',
          title: 'Thêm loại chi phí',
          settings: {
            nav: 1,
            content: 'Thêm loại chi phí',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/hopdong',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HopDong/dsHopDong.html',
          title: 'hợp đồng',
          settings: {
            nav: 4,
            content: 'Hợp đồng',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/hopdong/:idHopDong/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HopDong/ctHopDong.html',
          title: 'Chi tiết hợp đồng',
          settings: {
            nav: 1,
            content: 'Chi tiết hợp đồng',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/hopdong/:idHopDong/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HopDong/xemHopDong.html',
          title: 'Chi tiết hợp đồng',
          settings: {
            nav: 1,
            content: 'Chi tiết hợp đồng',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/phuluchopdong',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HopDong/PhuLuc/dsPhuLuc.html',
          title: 'phụ lục hợp đồng',
          settings: {
            nav: 4.1,
            content: 'Phụ lục hợp đồng',
            icon: 'account_box',
            quickLaunchEnabled: true
          }
        }
      },
      {
        url: '/phuluchopdong/:idPhuLuc/sua',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HopDong/PhuLuc/ctPhuLuc.html',
          title: 'Chi tiết phụ lục hợp đồng',
          settings: {
            nav: 1,
            content: 'Chi tiết phụ lục hợp đồng',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/phuluchopdong/:idPhuLuc/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/HopDong/PhuLuc/xemPhuLuc.html',
          title: 'Chi tiết phụ lục hợp đồng',
          settings: {
            nav: 1,
            content: 'Chi tiết phụ lục hợp đồng',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/generalctrl/',
        config: {
          templateUrl: cdnUrl + 'App/controllers/General/listItem.html',
          title: 'General Controller List',
          settings: {
            nav: 1,
            content: 'General Controller List',
            icon: 'face',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/generalctrl/:idItem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/General/detailItem.html',
          title: 'General Controller Detail',
          settings: {
            nav: 1,
            content: 'General Controller Detail',
            icon: 'face',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/quanlykhoiluongdutoan',
        config: {
          templateUrl: cdnUrl + 'App/controllers/QuanLyKhoiLuongDuToan/dsQuanLyKhoiLuongDuToan.html',
          title: 'Khối lượng dự toán',
          settings: {
            nav: 1,
            content: 'Khối lượng dự toán',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/quanlykhoiluongdutoan/:idItem/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/QuanLyKhoiLuongDuToan/ctQuanLyKhoiLuongDuToan.html',
          title: 'Khối lượng dự toán',
          settings: {
            nav: 2,
            content: 'Khối lượng dự toán',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/quanlykhoiluongdutoan/them',
        config: {
          templateUrl: cdnUrl + 'App/controllers/QuanLyKhoiLuongDuToan/themQuanLyKhoiLuongDuToan.html',
          title: 'Khối lượng dự toán',
          settings: {
            nav: 3,
            content: 'Khối lượng dự toán ',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/kehoachchonnhathau',
        config: {
          templateUrl: cdnUrl + 'App/controllers/KeHoachChonNhaThau/dsKeHoachChonNhaThau.html',
          title: 'Kế hoạch chọn nhà thầu',
          settings: {
            nav: 1,
            content: 'Kế hoạch chọn nhà thầu',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/kehoachchonnhathau/:idItem/xem',
        config: {
          templateUrl: cdnUrl + 'App/controllers/KeHoachChonNhaThau/ctKeHoachChonNhaThau.html',
          title: 'Kế hoạch chọn nhà thầu',
          settings: {
            nav: 2,
            content: 'Kế hoạch chọn nhà thầu',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      {
        url: '/kehoachchonnhathau/them',
        config: {
          templateUrl: cdnUrl + 'App/controllers/KeHoachChonNhaThau/themKeHoachChonNhaThau.html',
          title: 'Kế hoạch chọn nhà thầu',
          settings: {
            nav: 3,
            content: 'Kế hoạch chọn nhà thầu',
            icon: 'account_box',
            quickLaunchEnabled: false
          }
        }
      },
      // {
      //   url: '/khachhang/:idKhachHang',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/KhachHang/ctKhachHang.html',
      //     title: 'Chi tiết Khách hàng',
      //     settings: {
      //       nav: 1.1,
      //       content: 'Chi tiết khách hàng',
      //       icon: 'face',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/khachhang/:idKhachHang/donhang',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/KhachHang/dsDonHangKhachHang.html',
      //     title: 'Chi tiết Khách hàng',
      //     settings: {
      //       nav: 1.1,
      //       content: 'Chi tiết khách hàng',
      //       icon: 'face',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/nhanvien',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/NhanVien/dsNhanVien.html',
      //     title: 'Danh sách nhân viên',
      //     settings: {
      //       nav: 2,
      //       content: 'Danh sách Nhân viên',
      //       icon: 'supervised_user_circle',
      //       quickLaunchEnabled: true
      //     }
      //   }
      // },
      // {
      //   url: '/nhanvien/:idNhanVien',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/NhanVien/ctNhanVien.html',
      //     title: 'Chi tiết Nhân viên',
      //     settings: {
      //       nav: 2.1,
      //       content: 'Chi tiết Nhân viên',
      //       icon: 'supervised_user_circle',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/nhasanxuat',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/NhaSanXuat/dsNhaSanXuat.html',
      //     title: 'Danh sách Nhà sản xuất',
      //     settings: {
      //       nav: 3,
      //       content: 'Nhà sản xuất',
      //       icon: 'domain',
      //       quickLaunchEnabled: true
      //     }
      //   }
      // },
      // {
      //   url: '/nhasanxuat/:idNhaSanXuat',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/NhaSanXuat/ctNhaSanXuat.html',
      //     title: 'Chi tiết Nhà sản xuất',
      //     settings: {
      //       nav: 3.1,
      //       content: 'Chi tiết Nhà sản xuất',
      //       icon: 'domain',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/sanpham',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/Product/dsProduct.html',
      //     title: 'Danh sách Sản phẩm',
      //     settings: {
      //       nav: 3,
      //       content: 'Sản phẩm',
      //       icon: 'local_pharmacy',
      //       quickLaunchEnabled: true
      //     }
      //   }
      // },
      // {
      //   url: '/sanpham/:idProduct',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/Product/ctProduct.html',
      //     title: 'Chi tiết Sản phẩm',
      //     settings: {
      //       nav: 3.1,
      //       content: 'Chi tiết Sản phẩm',
      //       icon: 'local_pharmacy',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/donhang',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/DonHang/dsDonHang.html',
      //     title: 'Danh sách Đơn hàng',
      //     settings: {
      //       nav: 3,
      //       content: 'Đơn hàng',
      //       icon: 'receipt',
      //       quickLaunchEnabled: true
      //     }
      //   }
      // },
      // {
      //   url: '/donhang/:idDonHang',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/DonHang/ctDonHang.html',
      //     title: 'Chi tiết Đơn hàng',
      //     settings: {
      //       nav: 3.1,
      //       content: 'Chi tiết Đơn hàng',
      //       icon: 'receipt',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/donhang/:idDonHang/xem',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/DonHang/xemDonHang.html',
      //     title: 'Chi tiết Đơn hàng',
      //     settings: {
      //       nav: 3.1,
      //       content: 'Chi tiết Đơn hàng',
      //       icon: 'receipt',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/donhang/:idDonHang/duyet',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/DonHang/duyetDonHang.html',
      //     title: 'Duyệt Đơn hàng',
      //     settings: {
      //       nav: 3.1,
      //       content: 'Duyệt Đơn hàng',
      //       icon: 'receipt',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/khaosat/:idKhaoSat/update',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/KhaoSat/updateKhaoSat.html',
      //     title: 'Tạo khảo sát',
      //     settings: {
      //       nav: 4.1,
      //       content: 'Tạo khảo sát',
      //       icon: 'receipt',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/khaosat/them',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/KhaoSat/themKhaoSat.html',
      //     title: 'Tạo khảo sát',
      //     settings: {
      //       nav: 4.1,
      //       content: 'Tạo khảo sát',
      //       icon: 'receipt',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // },
      // {
      //   url: '/khaosat/thuviencauhoi',
      //   config: {
      //     templateUrl: cdnUrl + 'App/controllers/KhaoSat/ThuVienCauHoi.html',
      //     title: 'Thư viện câu hỏi',
      //     settings: {
      //       nav: 4.1,
      //       content: 'Thư viện câu hỏi',
      //       icon: 'receipt',
      //       quickLaunchEnabled: false
      //     }
      //   }
      // }
      {
        url: '/quanlydoanhnghiep',
        config: {
          templateUrl: cdnUrl + 'App/controllers/QuanLyDoanhNghiep/dsQuanLyDoanhNghiep.html',
          title: 'Thư viện câu hỏi',
          settings: {
            nav: 4.1,
            content: 'Thư viện câu hỏi',
            icon: 'receipt',
            quickLaunchEnabled: false
          }
        }
      }



    ];
  }
})();