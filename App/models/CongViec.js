// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

// Loại công việc
ict24h.models.LoaiCongViec = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.LoaiCongViecListItem'
  };
};