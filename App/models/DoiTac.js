// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

// Loại đối tác
ict24h.models.LoaiDoiTac = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.LoaiDoiTacListItem'
  };
};

// Đối tác
ict24h.models.DoiTac = function () {
    this.Id = undefined;
    this.Title = undefined;
    this.Ma = undefined;
    this.LoaiDoiTacId = undefined;
    this.DiaChi = undefined;
    this.NguoiDaiDien = undefined;
    this.ChucVuDaiDien = undefined;
    this.DienThoai = undefined;
    this.Fax = undefined;
    this.Email = undefined;
    this.Website = undefined;
    this.SoTaiKhoan = undefined;
    this.MaSoThue = undefined;
    this.NguoiLienHe = undefined;
    this.DienThoaiLienHe = undefined;
    this.TinhThanhId = undefined;
    this.GhiChu = undefined;
    this.__metadata = {
      type: 'SP.Data.DoiTacListItem'
    };
  };