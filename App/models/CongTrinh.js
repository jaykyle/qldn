// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

// Cấp công trình
ict24h.models.CapCongTrinh = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.CapCongTrinhListItem'
  };
};
// Hạng mục công trình
ict24h.models.HangMucCongTrinh = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.HangMucCongTrinhListItem'
  };
};
// Loại công trình
ict24h.models.LoaiCongTrinh = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.LoaiCongTrinhListItem'
  };
};
// Gói thầu
ict24h.models.GoiThau = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.HangMucCongTrinhId = undefined;
  this.DuAnId = undefined;
  this.__metadata = {
    type: 'SP.Data.GoiThauListItem'
  };
};