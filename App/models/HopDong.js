// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};
// Phân loại hợp đồng
ict24h.models.PhanLoaiHopDong = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.PhanLoaiHopDongListItem'
  };
};
// Hợp đồng
ict24h.models.HopDong = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.DuAnId = undefined;
  this.SoHopDong = undefined;
  this.NgayKy = undefined;
  this.NgayThucHien = undefined;
  this.ThoiGianThucHien = undefined;
  this.ThoiGianDieuChinh = undefined;
  this.NgayKetThucDuKien = undefined;
  this.NgayNghiemThu = undefined;
  this.NgayThanhLy = undefined;
  this.NgayThucTeBatDau = undefined;
  this.NgayThucTeKetThuc = undefined;
  this.GiaTriHopDong = undefined;
  this.GiaTriDieuChinh = undefined;
  this.GoiThauId = undefined;
  this.NguonVonId = undefined;
  this.HangMucId = undefined;
  this.DoiTacId = undefined;
  this.PhanLoaiHopDongId = undefined;
  this.LoaiChiPhiId = undefined;
  this.ChiPhiId = undefined;
  this.HienTrang = undefined;
  this.NoiDung = undefined;
  this.__metadata = {
    type: 'SP.Data.HopDongListItem'
  };
};
// phụ lục hợp đồng
ict24h.models.PhuLucHopDong = function () {
  this.Id = undefined;
  this.Ma = undefined;
  this.Title = undefined;
  this.HopDongId = undefined;
  this.NgayKyPhuLuc = undefined;
  this.GiaTriPhuLuc = undefined;
  this.SuaDoiThoiGian = undefined;
  this.ThoiGianPhuLuc = undefined;
  this.NoiDung = undefined;
  this.__metadata = {
    type: 'SP.Data.PhuLucHopDongListItem'
  };
};