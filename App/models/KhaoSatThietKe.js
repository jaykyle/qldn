// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

//Khảo sát - thiết kế
ict24h.models.KhaoSatThietKe = function () {
    this.Id = undefined;
    this.DuAnId = undefined;
    this.ChuNhiemKhaoSatId = undefined;
    this.ChuNhiemDoAnTKId = undefined;
    this.ToChucKhaoSatId = undefined;
    this.ToChucTuVanTKId = undefined;
    this.ToChucThamDinhTKId = undefined;
    this.NoiDungQuyMoGiaiPhapTK = undefined;
    this.__metadata = {
      type: 'SP.Data.KhaoSatThietKeListItem'
    };
};
// Điều chỉnh thiết kế - Xử lý kỹ thuật
ict24h.models.DieuChinhThietKe = function () {
    this.Id = undefined;
    this.DuAnId = undefined;
    this.HangMucCongTrinhId = undefined;
    this.NgayXuLyKyThuat = undefined;
    this.Title = undefined;
    this.GiaTri = undefined;
    this.__metadata = {
      type: 'SP.Data.DieuChinhThietKeListItem'
    };
};