// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

// Phân loại vốn
ict24h.models.LoaiVon = function () {
    this.Id = undefined;
    this.Title = undefined;
    this.Ma = undefined;
    this.__metadata = {
        type: 'SP.Data.LoaiVonListItem'
    };
};
// Danh sách vốn
ict24h.models.NguonVon = function () {
    this.Id = undefined;
    this.Title = undefined;
    this.Ma = undefined;
    this.DuAnId = undefined;
    this.KeHoachThu = undefined;
    this.KeHoachChi = undefined;
    this.ThucTeThu = undefined;
    this.ThucTeChi = undefined;
    this.SoDuVon = undefined;
    this.LoaiVonId = undefined;
    this.__metadata = {
        type: 'SP.Data.NguonVonListItem'
    };
};
// Phân loại chi phí
ict24h.models.LoaiChiPhi = function () {
    this.Id = undefined;
    this.Title = undefined;
    this.Ma = undefined;
    this.GiaTriDuAnDuocDuyet = undefined;
    this.GiaTriDuToanDuocDuyet = undefined;
    this.__metadata = {
        type: 'SP.Data.LoaiChiPhiListItem'
    };
};
// Loại chi phí của dự án
ict24h.models.LoaiChiPhiDuAn = function () {
    this.Id = undefined;
    this.Title = undefined;
    this.Ma = undefined;
    this.GiaTriDuAnDuocDuyet = undefined;
    this.GiaTriDuToanDuocDuyet = undefined;
    this.DuAnId = undefined;
    this.LoaiChiPhiId = undefined;
    this.__metadata = {
        type: 'SP.Data.LoaiChiPhiDuAnListItem'
    };
};
// Chi tiết chi phí
ict24h.models.ChiPhi = function () {
    this.Id = undefined;
    this.Ma = undefined;
    this.Title = undefined;
    this.DuAnId = undefined;
    this.LoaiChiPhiId = undefined;
    this.ChiPhiDuAn = undefined;
    this.ChiPhiDuToan = undefined;
    this.GiaTriHopDongDieuChinh = undefined;
    this.KLTTKH = undefined;
    this.KLTTThucTe = undefined;
    this.KLNTKH = undefined;
    this.KLNTThucTe = undefined;
    this.KLThucTeThucHien = undefined;
    this.TTVaTamUng = undefined;
    this.TienChiTT = undefined;
    this.TienChiTamUng = undefined;
    this.TienThuTT = undefined;
    this.TienThuTamUng = undefined;
    this.GoiThauId = undefined;
    this.HangMucCongTrinhId = undefined;
    this.NguonVonId = undefined;
    this.__metadata = {
        type: 'SP.Data.ChiPhiListItem'
    };
};