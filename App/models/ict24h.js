﻿// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

//*/#Begin quan ly khoi luong du toan----------------------------------------------------


//Loại chứng từ
ict24h.models.LoaiChungTu = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.TenLoaiChungTu = undefined;
  this.__metadata = {
    type: 'SP.Data.LoaiChungTuListItem'
  };
};

//Khối lượng công tác
ict24h.models.KhoiLuongCongTac = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.QuanLyKhoiLuongDuToanId = undefined;
  this.TenCongTac = undefined;
  this.DonViTinh = undefined;
  this.KhoiLuong = undefined;
  this.DonGiaCoThue = undefined;
  this.TienSauThue = undefined;
  this.GoiThauId=undefined;
  this.LoaiChiPhiId=undefined;

  this.__metadata = {
    type: 'SP.Data.KhoiLuongCongTacListItem'
  };
};

//Quản lý khối lượng dự toán
ict24h.models.QuanLyKhoiLuongDuToan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.MaChungTu = undefined;
  this.NgayChungTu = undefined;
  this.GoiThauId = undefined;
  this.DuAnId = undefined;
  this.LoaiChiPhiId = undefined;
  this.LoaiChungTuId = undefined;
  this.Active = undefined;
  this.GhiChu = undefined;
  this.__metadata = {
    type: 'SP.Data.QuanLyKhoiLuongDuToanListItem'
  };
};


//*/#End quan ly khoi luong du toan--------------------------------------------------


//**/#Kế hoạch lựa chọn nhà thầu------------------------------------------------------*/

//KeHoachChonNhaThau
ict24h.models.KeHoachChonNhaThau = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.DuAnId = undefined;
  this.ChiPhiId = undefined;
  this.GoiThauId =undefined;
  this.DoiTacId = undefined;
  this.NgonVonId = undefined;
  this.HinhThucChonNhaThauId = undefined;
  this.PhuongThucChonNhaThauId = undefined;
  this.ThoiGianChonNhaThau = undefined;
  this.ThoiGianThucHienHopDong = undefined;
  this.KqThoiGianChonNhaThau = undefined;
  this.KqThoiGianThucHienHopDong = undefined;
  this.GiaGoiThau = undefined;
  this.GiaDuThau = undefined;
  this.GiaTrungThau = undefined;
  this.__metadata = {
    type: 'SP.Data.KeHoachChonNhaThauListItem'
  };
};








//**/#END kế hoạch lựa chọn nhà thầu------------------------------------------------------*/ */



// Dự án
ict24h.models.DuAn = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.LinhVuc = undefined;
  this.TinhTrang = undefined;
  this.NamThucHien = undefined;
  this.MoTa = undefined;
  this.TienTrinh = undefined;
  this.ChuDauTuId = undefined;
  this.NguoiQuetDinhDauTuId = undefined;
  this.ToChucThamDinhDuAnId = undefined;
  this.DiaDiemId = undefined;
  this.DienTichDat = undefined;
  this.ToChucQuanLyDuAnId = undefined;
  this.ToChucLapDuAnId = undefined;
  this.ChuNhiemLapDuAnId = undefined;
  this.NhomDuAnId = undefined;
  this.LoaiCongTrinhId = undefined;
  this.CapCongTrinhId = undefined;
  this.HinhThucQuanLy = undefined;
  this.PhuongAnGiaiPhongMatBang = undefined;
  this.__metadata = {
    type: 'SP.Data.DuAnListItem'
  };
};
// Nhóm dự án
ict24h.models.NhomDuAn = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.NhomDuAnListItem'
  };
};
// Phòng ban
ict24h.models.PhongBan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.PhongBanListItem'
  };
};
// Chức vụ
ict24h.models.ChucVu = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.ChucVuListItem'
  };
};
// Học vấn
ict24h.models.HocVan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.HocVanListItem'
  };
};
// Bộ phận
ict24h.models.BoPhan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.__metadata = {
    type: 'SP.Data.BoPhanListItem'
  };
};
// Tỉnh thành
ict24h.models.TinhThanh = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.KhuVuc = undefined; // Choice: Bắc, Trung, Nam
  this.WeatherCode = undefined;
  this.__metadata = {
    type: 'SP.Data.TinhThanhListItem'
  };
};
// QL cá nhân
ict24h.models.CaNhan = function () {
  this.Id = undefined;
  this.Ma = undefined;
  this.Title = undefined;
  this.GioiTinh = undefined;
  this.NgaySinh = undefined;
  this.SoCMND = undefined;
  this.NgayCap = undefined;
  this.NoiCapId = undefined;
  this.DiaChiThuongTru = undefined;
  this.DiaChiTamTru = undefined;
  this.DienThoai = undefined;
  this.Email = undefined;
  this.NgayThuViec = undefined;
  this.NgayTiepNhan = undefined;
  this.NgayNghiViec = undefined;
  this.PhongBanId = undefined;
  this.ChucVuId = undefined;
  this.BoPhanId = undefined;
  this.HocVanId = undefined;
  this.CongTyId = undefined;
  this.OfficeAccId = undefined;
  this.GhiChu = undefined;
  this.__metadata = {
    type: 'SP.Data.CaNhanListItem'
  };
};
