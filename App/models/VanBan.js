// create namespace for this project
var ict24h = ict24h || {};
ict24h.models = ict24h.models || {};

// Nhóm văn bản
ict24h.models.NhomVanBan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.__metadata = {
    type: 'SP.Data.NhomVanBanListItem'
  };
};
// Loại văn bản
ict24h.models.LoaiVanBan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.NhomVanBanId = undefined;
  this.__metadata = {
    type: 'SP.Data.LoaiVanBanListItem'
  };
};
// Hồ sơ văn bản
ict24h.models.HoSoVanBan = function () {
  this.Id = undefined;
  this.Title = undefined;
  this.Ma = undefined;
  this.SoHieuVanBan = undefined;
  this.DuAnId = undefined;
  this.NgayKy = undefined;
  this.NgayPhatHanh = undefined;
  this.NgayNop = undefined;
  this.NgayHetHan = undefined;
  this.GiaTri = undefined;
  this.KieuVanBan = undefined;
  this.LoaiVanBanId = undefined;
  this.NhomVanBanId = undefined;
  this.MucDoBaoMat = undefined;
  this.MucDoKhanCap = undefined;
  this.HangMucCongTrinhId = undefined;
  this.PhongBanId = undefined;
  this.ToChucPhatHanhId = undefined;
  this.NoiPhatHanh = undefined;
  this.NoiDung = undefined;
  this.__metadata = {
    type: 'SP.Data.HoSoVanBanListItem'
  };
};