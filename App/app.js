﻿(function () {
  'use strict';

  // create app
  var app = angular.module('app', [
    // ootb angular modules
    'ngRoute',      // app route (url path) support
    'ngCookies',    // cookie read/write support
    'ngSanitize',   // fixes HTML issues in data binding
    'ngResource', 
    'ngAnimate',
    'ui.bootstrap',
    'ui.select',
    'ui.bootstrap.datetimepicker',
    'cleave.js',
    'chart.js',//chart 
    'cleave.js',
    'angularFileUpload',
    'angular-ladda',
    'ngSlimScroll', //https://git.io/fpRWT
    // my custom modules
    'common'
  ]);

  // startup code
  //app.run(['$route','angular.config', 'editableOptions', 'editableThemes', function($route, angularConfig, editableOptions,editableThemes) {
  //  editableThemes.bs3.inputClass = 'input-sm';
  //  editableThemes.bs3.buttonsClass = 'btn-sm';
  //  editableOptions.theme = 'bs3';

  //}]);

    // startup code
    app.run(['$route', 'angular.config', function ($route, angularConfig) {

    }]);

})();