// In app.js or main.js or whatever:
// var myApp = angular.module('askchisne', ['ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tpls']);

// This filter makes the assumption that the input will be in decimal form (i.e. 17% is 0.17).
angular.module('app').filter('ceil', [ function () {
    return function(input) {
        return Math.ceil(input);
    };
  }]);
  
  
  /* 
  // Usage:
  <p>The percentage is {{ (100*count/total) | ceil }}%</p>
  */