(function () {
    'use strict';

    // define controller
    var controllerId = "ctNhanVien";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

        vm.pickerSinhNhat = {
            date: new Date()
        };

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const NhanVienId = +$routeParams.idNhanVien;

        const listName = "NhanVien";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (NhanVienId && +NhanVienId > 0) {
                getItem(NhanVienId);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctNhanVien) {
                return moment(vm.ctNhanVien.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctNhanVien) {
                return moment(vm.ctNhanVien.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.NhanVien();
                saveItem = vm.ctNhanVien;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin nhân viên.", null, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin Nhân viên', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctNhanVien)
                .then(function () {
                    common.logger.logSuccess("Đã xóa nhân viên.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa nhân viên', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctNhanVien = new ict24h.models.NhanVien();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Title,PhongBan,ChucVu,Email,SDT,GioiTinh,SinhNhat";
            const expand = undefined;

            let itemObj = new ict24h.models.NhanVien();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctNhanVien = data;
                });
        }

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();