(function () {
    'use strict';

    // define controller
    var controllerId = "themLoaiChiPhiDuAn";
    angular.module('app').
    controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        vm.deleteFile = deleteFile;
        vm.checkLoaiChiPhiDuAn = checkLoaiChiPhiDuAn;
        vm.CheckLoaiChiPhiDuAn = false; // Set mặc định là chưa có

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const idItem = +$routeParams.idLoaiChiPhiDuAn;

        const listName = "LoaiChiPhiDuAn";

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayKy = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
		vm.formFormat = {
            number: { numeral: true }
        };

        // initalize controller
        init();


        // initalize controller
        function init() {
            createItem();
            // Lấy danh sách từ list lọc
            getListExpand('DuAn', 'Id,Title,Ma', null, null); 
            getListExpand('LoaiChiPhi', 'Id,Title,Ma', null, null); 

        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.LoaiChiPhiDuAn();
                saveItem = vm.ctLoaiChiPhiDuAn;

                // Xóa các thuộc tính expand
                [
                    'DuAn','LoaiChiPhi',
                    'Author','Editor','AttachmentFiles'
                ].forEach( (e) => delete saveItem[e] )

                // Set trường dữ liệu tĩnh
                saveItem.Title = vm.CacheLoaiChiPhiSelected.Title;
                saveItem.Ma = vm.CacheLoaiChiPhiSelected.Ma;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (+vm.uploader.queue.length > 0){
                        const itemId = data.Id || data.d.Id;
                        saveUploadFile(null, listName, itemId)
                    } else {
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        goBack();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Tham số listName, currentItem
                    datacontext.deleteItemOfList(listName, vm.ctLoaiChiPhiDuAn)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thông tin.", data, controllerId);
                    })
                    .then(function () {
                        goBack();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa.', error, controllerId);
                    });

                }
              })

            
        }

        // create a Item
        function createItem() {
            vm.ctLoaiChiPhiDuAn = new ict24h.models.LoaiChiPhiDuAn();
        }

        // load the item specified in the route
        function getItem(itemId) {

            // set array property fields item
            const select = [
                'Id','Ma','Title','Created','Modified',
                'LoaiChiPhiId,GiaTriDuAnDuocDuyet,GiaTriDuToanDuocDuyet',
                'DuAnId,DuAn/Title',
                'Editor/Title,Author/Title',
                'AttachmentFiles/FileName,AttachmentFiles/ServerRelativeUrl'
            ].join();
            const expand = [
                'DuAn',
                'Author','Editor','AttachmentFiles'
            ].join();

            let itemObj = new ict24h.models.LoaiChiPhiDuAn();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand).
            then(function (data) {
                vm.ctLoaiChiPhiDuAn = data;
            }).
            catch(function (error) {
                common.logger.logError('Không thể lấy thông tin.', error, controllerId);
            });
        }

        /**
         * Kiểm tra Loại chi phí đã có trong dự án chưa
         */
        function checkLoaiChiPhiDuAn(){
            vm.CheckLoaiChiPhiDuAn = false; // Gọi đến là gắn cờ để ẩn
            if(+vm.ctLoaiChiPhiDuAn.DuAnId > 0 && +vm.ctLoaiChiPhiDuAn.LoaiChiPhiId > 0){
                const select = 'Id',
                filter = '(DuAnId eq ' + vm.ctLoaiChiPhiDuAn.DuAnId + ') and (LoaiChiPhiId eq ' + vm.ctLoaiChiPhiDuAn.LoaiChiPhiId +')';
                // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
                datacontext.getDsItem(listName, null, null, select, null, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm.CheckLoaiChiPhiDuAn = false;
                        vm.MsgLoaiChiPhiDuAn = 'Đã có Loại chi phí cho dự án này!';
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    } else if (data && data.length === 0) {
                        vm.CheckLoaiChiPhiDuAn = true;
                        vm.MsgLoaiChiPhiDuAn = '';
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
            } else {
                common.logger.logError('Bạn chưa chọn Dự án hoặc Loại chi phí', null, controllerId);
            }
            
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {


        });

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Xử lý UPLOAD
         */
		vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

		//Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                goBack();
            }
            
        }
        
        // Handle deletel attachment file
        //delFile(listName, itemId, fileName)
        function deleteFile(fileName){
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Khai báo tham số
                    const itemId = vm.ctLoaiChiPhiDuAn.Id;
                    // Ẩn nút lưu
                    vm.btnSave = true;
                    AttachmentSvc.delFile(listName, itemId, fileName).
                    then((data) => {
                        // Khi xóa xong thì xuất thông tin
                        common.logger.logSuccess("Xóa tệp tin thành công", data, controllerId);
                        
                        // get item data
                        getItem(itemId);
                        // Active Save button
                        vm.btnSave = false;

                    } ).catch( (error) => {
                        common.logger.logError('Không thể xóa tệp tin!', error, controllerId)
                        // Active Save button
                        vm.btnSave = false;
                    } );

                }
            })
        }

        

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();