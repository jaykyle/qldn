(function () {
    'use strict';

    // define controller
    var controllerId = "themQuanLyKhoiLuongDuToan";
    angular.module('app').controller(controllerId,
        ['$location', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;
       
        vm.gotoItem =gotoItem;
        vm.getDsDuAn = getDsDuAn;
        vm.getDsLoaiChungTu = getDsLoaiChungTu;
        vm.getLoaiChiPhi = getLoaiChiPhi;
        vm.goSave = goSave;
        vm.getDsGoiThau = getDsGoiThau;
        vm.getChiPhi = getChiPhi;

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayChungTu = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
		vm.formFormat = {
            number: { numeral: true }
        };



        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách        
            getDsDuAn();
            getDsLoaiChungTu();
            getLoaiChiPhi();
            getDsGoiThau();
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path(`/quanlykhoiluongdutoan/${item.Id}/xem`);
            }
        }

        //getDsDuAn
        function getDsDuAn(){
            var listName = "DuAn";
            const select = "Id,Title";
            const expand = undefined; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsDuAn = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });

        }

        //getLoaiChiPhi
        function getLoaiChiPhi(){
            var listName = "LoaiChiPhi";
            const select = "Id,Title";
            const expand = undefined; 
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsLoaiChiPhi = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }
        
        //get danh sach chi phi
        function getChiPhi(Id){
            var listName = "ChiPhi";
            const select = "Id,Title";
            const expand = undefined; 
            const filter = `LoaiChiPhiId eq ${Id}`;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data.length>0) {
                        vm.dsChiPhi = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        //getDsLoaiChungTu
        function getDsLoaiChungTu(){
            var listName = "LoaiChungTu";
            const select = "Id,TenLoaiChungTu";
            const expand = undefined; 
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsLoaiChungTu = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });

        }
 
        //getDsGoiThau
        function getDsGoiThau(){
                var listName = "GoiThau";
            const select = "*";
            const expand = undefined; 
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsGoiThau = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });

        }

        //Thêm mới
        function goSave() {
            var listName = "QuanLyKhoiLuongDuToan";
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.QuanLyKhoiLuongDuToan();

                saveItem.MaChungTu = vm.MaChungTu;
                saveItem.DuAnId = vm.DuAn.Id;
                //saveItem.TenGoiThau =vm.TenGoiThau;
                saveItem.LoaiChungTuId=vm.LoaiChungTu.Id;
                saveItem.LoaiChiPhiId =vm.LoaiChiPhi.Id;
                saveItem.ChiPhiId=vm.ChiPhi.Id;
                saveItem.NgayChungTu = new Date(vm.NgayChungTu).toISOString();
                saveItem.Active = vm.Active;
                saveItem.GoiThauId = vm.GoiThau.Id;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (data.d){
                        
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        gotoItem(data.d);
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);               
                });
        }

    };

})();