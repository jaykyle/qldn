(function () {
    'use strict';

    // define controller
    var controllerId = "ctQuanLyKhoiLuongDuToan";
    angular.module('app').controller(controllerId,
        ['$location','$routeParams', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location,$routeParams, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        vm.delItem = delItem;
        vm.goSave = goSave;
        vm.ThanhTien=ThanhTien;

        const idItem = +$routeParams.idItem;

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };

        vm.pickerNgayKy = {
            date: new Date()
        };
        vm.pickerNgayPhatHanh = {
            date: new Date()
        };
        vm.pickerNgayNop = {
            date: new Date()
        };
        vm.pickerNgayHetHan = {
            date: new Date()
        };
 
          // Tùy chọn cho định dạng input form
		vm.formFormat = {
			// Số đt
			phone: {
				phone: true,
				phoneRegionCode: 'VN'
			},
			// Dãy số
			number: {
				numeral: true
			},
			cmnd: {
				blocks: [3, 3, 3, 3, 3, 3],
				//prefix: 'PREFIX',
				//numeral: true,
				///delimiters: [' '],
			}
		}

        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();
            getKhoiLuongCongTac();
      
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/chiphi/' + item.Id);
            }
        }

        //Lấy thông tin chi tiết quan ly khoi luong du toan
        function getListItems() {
            var listName = "QuanLyKhoiLuongDuToan";
            const select = "Id,ChiPhiId,LoaiChiPhiId,GoiThauId,Title,GoiThau/Title,GoiThau/Id,LoaiChiPhi/Id,MaChungTu,NgayChungTu,Active,DuAn/Title,LoaiChiPhi/Title,LoaiChungTu/TenLoaiChungTu";
            const expand = 'DuAn,LoaiChiPhi,LoaiChungTu,GoiThau'; //'NhanVien,PhongBan';
            const filter = `Id eq ${idItem}`;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data.length >0) {
                        vm.ctKhoiLuongDuToan = data[0];
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }


        //Lấy danh sach khoi luong cong tac
        function getKhoiLuongCongTac() {
            var listName = "KhoiLuongCongTac";
            const select = "*";
            const expand = undefined; //'NhanVien,PhongBan';
            const filter = `QuanLyKhoiLuongDuToanId eq ${idItem}`;
                
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {                                   
                        vm.dsKhoiLuongCongTac = data;                   
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }



        // Xoa mot item
        function delItem(item) {
            
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                
                if (result.value) {
                    var listName="KhoiLuongCongTac";
                    var ItemDel={};
                    ItemDel.__metadata = item.__metadata;
                    ItemDel.Id = item.Id;
                    datacontext.deleteItemOfList(listName, ItemDel)
                        .then(function () {

                             // Tải toàn bộ danh sách
                             getKhoiLuongCongTac();
                            common.logger.logSuccess("Đã xóa thành công!", null, controllerId);
                           
                        })
                        .catch(function (error) {
                            common.logger.logError('Không thể xóa!', error, controllerId);
                        });
                }
            })
        }


        
        function goSave(){
            var listName = "KhoiLuongCongTac";
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.KhoiLuongCongTac();

                saveItem.QuanLyKhoiLuongDuToanId = idItem;
                saveItem.TenCongTac = vm.TenCongTac;
                saveItem.DonViTinh =vm.DonViTinh;
                saveItem.KhoiLuong = +vm.KhoiLuong;
                saveItem.DonGiaCoThue = +vm.DonGiaCoThue;
                saveItem.TienSauThue = +vm.TienSauThue; 
                saveItem.GoiThauId =vm.ctKhoiLuongDuToan.GoiThauId;
                saveItem.LoaiChiPhiId=vm.ctKhoiLuongDuToan.LoaiChiPhiId;
                saveItem.Ma=vm.Ma;
                saveItem.ChiPhiId=vm.ctKhoiLuongDuToan.ChiPhiId;
            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (data){
                        debugger
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        getKhoiLuongCongTac();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);               
                });
        }

        function ThanhTien(){
            vm.TienSauThue = +vm.KhoiLuong*+vm.DonGiaCoThue;
        }

    };

})();