(function () {
    'use strict';

    // define controller
    var controllerId = "dsQuanLyKhoiLuongDuToan";
    angular.module('app').controller(controllerId,
        ['$location', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;
     
        vm.delItem = delItem;
        vm.gotoItem =gotoItem;
        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();

         
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/quanlykhoiluongdutoan/' + item.Id);
            }
        }

        // Lấy danh sách tất cả
        function getListItems() {
            var listName = "QuanLyKhoiLuongDuToan";
            const select = "Id,Title,MaChungTu,NgayChungTu,Active,DuAn/Title,LoaiChiPhi/Title,LoaiChungTu/TenLoaiChungTu,GoiThau/Title";
            const expand = 'DuAn,LoaiChiPhi,LoaiChungTu,GoiThau'; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsKhoiLuongDuToan = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        // Xoa mot item
        function delItem(item) {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                if (result.value) {

                    datacontext.deleteItemOfList(listName, item)
                        .then(function () {
                            common.logger.logSuccess("Đã xóa thành công!", null, controllerId);

                            // Tải toàn bộ danh sách
                            getListItems();
                        })
                        .catch(function (error) {
                            common.logger.logError('Không thể xóa!', error, controllerId);
                        });
                }
            })
        }

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }

    };

})();