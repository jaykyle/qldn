(function () {
    'use strict';

    // define controller
    var controllerId = "ctKhachHang";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const KhachHangId = +$routeParams.idKhachHang;

        const listName = "KhachHang";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (KhachHangId && +KhachHangId > 0) {
                getItem(KhachHangId);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctKhachHang) {
                return moment(vm.ctKhachHang.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctKhachHang) {
                return moment(vm.ctKhachHang.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.KhachHang();
                saveItem = vm.ctKhachHang;


            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin khách hàng.", null, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin khách hàng', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctKhachHang)
                .then(function () {
                    common.logger.logSuccess("Đã xóa khách hàng.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa khách hàng', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctKhachHang = new ict24h.models.KhachHang();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Title,Ma,DiaChi,SDT,Note,NgayMuaCuoi";
            const expand = undefined;

            let itemObj = new ict24h.models.KhachHang();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctKhachHang = data;
                });
        }

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();