(function () {
    'use strict';

    // define controller
    var controllerId = "dsKhachHang";
    angular.module('app').controller(controllerId,
        ['$location', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($location, $routeParams, common, datacontext) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        
        vm.getListItemsFilter = getListItemsFilter;

        vm.cacheFilter = {}; // Để lọc ở ngoài bộ tìm kiếm

        const listName = "KhachHang";

        // init controller
        init();


        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/khachhang/' + item.Id );
            }
        }

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const select = "Id,Title,Ma,SDT,NgayMuaCuoi";
            const expand = undefined; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsKhachHang = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách khách hàng');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách khách hàng', error, controllerId);
                });
        }

        /**
         * Hàm lấy danh sách theo bộ lọc
         * Keyword
         * ThoiGian
         * @param {Object} fiterObj 
         */
        function getListItemsFilter(fiterObj) {

            const select = "Id,Title,Ma,SDT,NgayMuaCuoi",
                expand = undefined; //'NhanVien,PhongBan';
            let filter = '';
            let filterArr = [];

            //Nếu bộ lọc có Product
            if (vm.cacheFilter.Keyword && vm.cacheFilter.Keyword !== '') {
                filterArr.push("((substringof('"+ vm.cacheFilter.Keyword +"',Ma)) or (substringof('"+ vm.cacheFilter.Keyword +"',Title)) or (substringof('"+ vm.cacheFilter.Keyword +"',SDT)) or (substringof('"+ vm.cacheFilter.Keyword +"',DiaChi)))");
            }
            //Nếu bộ lọc có thời gian
            if (vm.cacheFilter.ThoiGian && vm.cacheFilter.ThoiGian !== '') {
                const TODAY = new moment();
                if (vm.cacheFilter.ThoiGian === '1week'){
                    // Ngày hiện tại trừ đi 7 ngày
                    const oneWeek = TODAY.subtract(1, "weeks").toISOString();
                    filterArr.push("(NgayMuaCuoi le datetime'"+ oneWeek +"')");
                } else if (vm.cacheFilter.ThoiGian === '1month'){
                    // Ngày hiện tại trừ đi 1 tháng
                    const oneMonth = TODAY.subtract(1, "months").toISOString();
                    filterArr.push("(NgayMuaCuoi le datetime'"+ oneMonth +"')");
                } else if (vm.cacheFilter.ThoiGian === '1year'){
                    // Ngày hiện tại trừ đi 1 năm
                    const oneYear = TODAY.subtract(1, "years").toISOString();
                    filterArr.push("(NgayMuaCuoi le datetime'"+ oneYear +"')");
                }
            }
            // Nối các phần tử trong Arr lại
            if (filterArr.length > 1){
                filter = filterArr.join(' and ');
            } else if (filterArr.length === 1){
                filter = filterArr[0];
            }

            console.log('filter: ' + filter);
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsKhachHang = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách khách hàng');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách khách hàng', error, controllerId);
                });
        }
        

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }
    };

})();