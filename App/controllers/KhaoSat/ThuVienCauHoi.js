(function () {
    'use strict';

    // define controller
    var controllerId = "ThuVienCauHoi";
    angular.module('app').controller(controllerId,
        ['$location', '$routeParams', '$window', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($location, $routeParams, $window, common, datacontext) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        // đường dẫn để thêm Item mới
        //vm.newItem = newItem;
        vm.goDelTraLoi = goDelTraLoi;
        vm.goSave = goSave;
        vm.goSaveTraLoi = goSaveTraLoi;

        const listName = "ThuVienCauHoi";

        // init controller
        init();


        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/nhanvien/' + item.Id );
            }
        }

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const select = "Id,Title,Status,KieuTraLoi,ThuVienCauTraLoi/Id,ThuVienCauTraLoi/TraLoi";
            const expand = "ThuVienCauTraLoi"; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsThuVienCauHoi = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách Câu hỏi');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách Câu hỏi', error, controllerId);
                });
        }

        // handle delete action
        function goDelTraLoi(item) {
            const listName = "ThuVienCauTraLoi";
            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, item)
                .then(function () {
                    common.logger.logSuccess("Đã xóa thành công.", null, controllerId);
                })
                .then(function () {
                    $window.location.reload();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa.', error, controllerId);
                });
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.ThuVienCauHoi();
                saveItem.Title = vm.ctThuVienCauHoi.Title;
                saveItem.Status = vm.ctThuVienCauHoi.Status;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu câu hỏi.", data, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.location.reload();
                    //$window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu câu hỏi', error, controllerId);
                    vm.btnSave = false;
                });
        }
        // handle save action
        function goSaveTraLoi(item) {
            vm.btnSave = true;
            
            const listNameTraLoi = "ThuVienCauTraLoi"

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.ThuVienCauTraLoi();
                saveItem.TraLoi = item.TraLoi;

            datacontext.saveItem(listNameTraLoi, saveItem)
                .then(function (data) {

                    // Lưu Id Câu trả lời trên vào cha
                    item.ThuVienCauTraLoiId = { results: [] };
                    item.ThuVienCauTraLoi.results.forEach( (e) => item.ThuVienCauTraLoiId.results.push(e.Id) );

                    item.ThuVienCauTraLoiId.results.push(data.d.Id);


                    //Xóa thuộc tính thừa lưu tạm ngoài view
                    delete item.TraLoi;
                    delete item.ThuVienCauTraLoi;
                    
                    datacontext.saveItem(listName, item)
                        .then(function (data) {
                            // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                            common.logger.logSuccess("Đã lưu câu hỏi.", data, controllerId);
                            vm.btnSave = false;
                        })
                        .then(function () {
                            $window.location.reload();
                            //$window.history.back();
                        })
                        .catch(function (error) {
                            common.logger.logError('Lỗi không thể lưu câu hỏi', error, controllerId);
                            vm.btnSave = false;
                        });

                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu câu trả lời.", data, controllerId);
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu câu trả lời', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }
    };

})();