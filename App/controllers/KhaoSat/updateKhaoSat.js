(function () {
    'use strict';

    // define controller
    var controllerId = "updateKhaoSat";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;

        // vm.getDsTraLoi = getDsTraLoi;

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const KhaoSatId = +$routeParams.idKhaoSat;

        const listName = "KhaoSat";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (KhaoSatId && +KhaoSatId > 0) {
                getItem(KhaoSatId);
                vm.showDelBtn = true;
            } else {
                $window.history.back();
            }

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctNhanVien) {
                return moment(vm.ctNhanVien.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctNhanVien) {
                return moment(vm.ctNhanVien.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.NhanVien();
                saveItem = vm.ctNhanVien;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin nhân viên.", null, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin Nhân viên', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctNhanVien)
                .then(function () {
                    common.logger.logSuccess("Đã xóa nhân viên.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa nhân viên', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctNhanVien = new ict24h.models.NhanVien();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Status,KhachTraLoi/CauHoi";
            const expand = "KhachTraLoi";

            let itemObj = new ict24h.models.KhaoSat();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctKhaoSat = data;
                })
                .then(()=> getDsTraLoi(vm.ctKhaoSat.KhachTraLoiId.results))
                ;
        }

        // Lấy danh sách tất cả
        function getDsTraLoi(idItem) {

            const listName = "KhachtraLoi",
             select = "Id,CauHoi,ThuVienCauTraLoi/TraLoi";
            const expand = "ThuVienCauTraLoi"; //'NhanVien,PhongBan';
            //const filter = "(Id eq "+ idItem +")";
            let filter = '';
            let filterArr = [];

            for (let i in idItem){
                filterArr.push('(Id eq '+ idItem[i] +')');
            }
            // Nối các phần tử trong Arr lại
            if (filterArr.length > 1){
                filter = filterArr.join(' or ');
            } else if (filterArr.length === 1){
                filter = filterArr[0];
            }

            debugger

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.DsCauTraLoi = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách Câu hỏi');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách Câu hỏi', error, controllerId);
                });
        }

        





    }

})();