(function () {
    'use strict';

    // define controller
    var controllerId = "themKhaoSat";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;

        vm.TaoCauHoi = postMultiItem;

        vm.cacheDsCauHoi = [];

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const NhanVienId = +$routeParams.idNhanVien;

        const listName = "KhaoSat";

        // initalize controller
        init();


        // initalize controller
        function init() {

            createItem();

            // Lấy danh sách câu hỏi
            getDsCauHoi();

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctKhaoSat) {
                return moment(vm.ctKhaoSat.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctKhaoSat) {
                return moment(vm.ctKhaoSat.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem = new ict24h.models.KhaoSat();
                saveItem.KhachTraLoiId.results = vm.cacheDsCauHoi;
                saveItem.Status = 'Draft';

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã tạo mới khảo sát.", null, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin Khảo sát', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctKhaoSat)
                .then(function () {
                    common.logger.logSuccess("Đã xóa khảo sát.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa khảo sát', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctKhaoSat = new ict24h.models.KhaoSat();
        }

        /**
         * Post multi item
         * @param {*} itemSave Thông tin mã hàng nếu cần update
         */
        function postMultiItem() { // Có item là lưu đè (overwrite)
            
            vm.btnSave = true;

            if(vm.dsThuVienCauHoi.length > 0){

                const listName = "KhachTraLoi"
                let saveItem = new ict24h.models.KhachTraLoi();
                saveItem.ThuVienCauHoiId = vm.dsThuVienCauHoi[0].Id;
                saveItem.ThuVienCauTraLoiId = vm.dsThuVienCauHoi[0].ThuVienCauTraLoiId;
                saveItem.CauHoi = vm.dsThuVienCauHoi[0].Title;

                debugger

                datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    //common.logger.logSuccess("Đã lưu thông tin khảo sát.", data, controllerId);
                    vm.cacheDsCauHoi.push(data.d.Id);
                })
                .then(function () {
                    vm.dsThuVienCauHoi.shift(); // Lấy Id con để lưu list cha
                })
                .then(function () {
                    postMultiItem(); // Gọi lại hàm tìm trùng lặp và đăng
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin Khảo sát', error, controllerId);
                });
            } else {
                // Không có phần tử con nữa thì lưu list cha
                goSave();
            }
            
        }

        // Lấy danh sách tất cả
        function getDsCauHoi() {

            const listName = "ThuVienCauHoi",
             select = "Id,Title,Status,KieuTraLoi,ThuVienCauTraLoiId";
            const expand = undefined; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsThuVienCauHoi = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách Câu hỏi');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách Câu hỏi', error, controllerId);
                });
        }


        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();