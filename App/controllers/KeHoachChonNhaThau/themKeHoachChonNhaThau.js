(function () {
    'use strict';

    // define controller
    var controllerId = "themKeHoachChonNhaThau";
    angular.module('app').controller(controllerId,
        ['$location', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;
       
        vm.gotoItem =gotoItem;
        vm.getDsDuAn = getDsDuAn;
        vm.getDsDoiTac = getDsDoiTac;
        vm.getHinhThucChonNhaThau =getHinhThucChonNhaThau;
        vm.getPhuongThucChonNhaThau = getPhuongThucChonNhaThau;
        vm.getLoaiHopDong=getLoaiHopDong;
        vm.goSave = goSave;
        vm.getDsThongTinChiPhiGoiThau = getDsThongTinChiPhiGoiThau;


        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayChungTu = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
		vm.formFormat = {
            number: { numeral: true }
        };



        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách        
            getDsDuAn();
            getDsDoiTac();     
            getHinhThucChonNhaThau();
            getPhuongThucChonNhaThau();
            getLoaiHopDong();
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path(`/kehoachchonnhathau`);
            }
        }

        //getDsDuAn
        function getDsDuAn(){
            var listName = "DuAn";
            const select = "Id,Title";
            const expand = undefined; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsDuAn = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });

        }

        //get danh sach doi tac
        function getDsDoiTac(){
            var listName = "DoiTac";
            const select = "*";
            const expand = undefined; 
            const filter = undefined;
            
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    
                    if (data.length >0) {
                        vm.dsDoiTac = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        //getDsThongTinChiPhiGoiThau
        function getDsThongTinChiPhiGoiThau(id){
            var listName = "ChiPhi";
            const select = "*,GoiThau/Title,GoiThau/Id,NguonVon/Id,NguonVon/Title";
            const expand = "GoiThau,NguonVon"; 
            const filter = `DuAnId eq ${id}`;
            
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    
                    if (data.length >0) {
                        vm.dsChiPhiGoiThau = data;
                        debugger
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        //get hình thức chọn nhà thầu
        function getHinhThucChonNhaThau(){
            var listName = "HinhThucChonNhaThau";
            const select = "*";
            const expand = undefined; 
            const filter = undefined;
            
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    
                    if (data.length >0) {
                        vm.dsHinhThucChonNhaThau = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }


        //get danh sách phương thức chọn nhà thầu
        function getPhuongThucChonNhaThau(){
            var listName = "PhuongThucChonNhaThau";
            const select = "*";
            const expand = undefined; 
            const filter = undefined;
            
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    
                    if (data.length >0) {
                        vm.dsPhuongThucChonNhaThau = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        //Get danh sách loại hợp đồng
        function getLoaiHopDong(){
            var listName = "LoaiHopDong";
            const select = "*";
            const expand = undefined; 
            const filter = undefined;
            
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    
                    if (data.length >0) {
                        vm.dsLoaiHopDong = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        //Thêm mới
        function goSave() {
            var listName = "KeHoachChonNhaThau";
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.KeHoachChonNhaThau();            
   
            saveItem.DuAnId = vm.DuAn.Id;
            saveItem.ChiPhiId = vm.ChiPhiGoiThau.Id; //Id chi phí
            saveItem.GoiThauId =vm.ChiPhiGoiThau.GoiThau.Id;
            saveItem.DoiTacId = vm.DoiTac.Id;     
            saveItem.NguonVonId = vm.ChiPhiGoiThau.NguonVon.Id;
            saveItem.HinhThucChonNhaThauId = vm.HinhThucChonNhaThau.Id;
            saveItem.PhuongThucChonNhaThauId = vm.PhuongThucChonNhaThau.Id;
            saveItem.ThoiGianChonNhaThau = +vm.ThoiGianChonNhaThau;
            saveItem.ThoiGianThucHienHopDong = +vm.ThoiGianThucHienHopDong;
            saveItem.KqThoiGianChonNhaThau = +vm.KqThoiGianChonNhaThau;
            saveItem.KqThoiGianThucHienHopDong = +vm.kqThoiGianTHucHienHopDong;
            saveItem.GiaDuThau = +vm.GiaDuThau;
            saveItem.GiaTrungThau = +vm.GiaTrungThau;
            saveItem.TrangThai = vm.TrangThai;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (data.d){
                        debugger
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        gotoItem();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);               
                });
        }

    };

})();