(function () {
    'use strict';

    // define controller
    var controllerId = "dsKeHoachChonNhaThau";
    angular.module('app').controller(controllerId,
        ['$location', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;
     
        vm.delItem = delItem;
        vm.gotoItem =gotoItem;
        vm.CapNhatTrangThai = CapNhatTrangThai;
        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();

         
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/quanlykhoiluongdutoan/' + item.Id);
            }
        }

        //CapNhatTrangThai
        function CapNhatTrangThai(item){
            var listName = "KeHoachChonNhaThau";
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.KeHoachChonNhaThau();  
            
            saveItem.TrangThai = item.TrangThai;
            saveItem.__metadata = item.__metadata;
            saveItem.__metadata.etag="*";
            saveItem.Id=item.Id;
            
            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (data){                        
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        getListItems();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);               
                });
        }

        // Lấy danh sách tất cả
        function getListItems() {
            var listName = "KeHoachChonNhaThau";
            const select = "*,DuAn/Title,NguonVon/Title,ChiPhi/ChiPhiDuToan,GoiThau/Title,DoiTac/Title,HinhThucChonNhaThau/Title,PhuongThucChonNhaThau/Title,LoaiHopDong/Title";
            const expand = 'DuAn,ChiPhi,NguonVon,GoiThau,DoiTac,HinhThucChonNhaThau,PhuongThucChonNhaThau,LoaiHopDong'; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsKeHoachChonNhaThau = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        // Xoa mot item
        function delItem(item) {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                if (result.value) {

                    datacontext.deleteItemOfList(listName, item)
                        .then(function () {
                            common.logger.logSuccess("Đã xóa thành công!", null, controllerId);

                            // Tải toàn bộ danh sách
                            getListItems();
                        })
                        .catch(function (error) {
                            common.logger.logError('Không thể xóa!', error, controllerId);
                        });
                }
            })
        }

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }

    };

})();