(function () {
    'use strict';

    // define controller
    var controllerId = "ctDuAn";
    angular.module('app').
    controller(controllerId, ['$window', '$routeParams','$cookieStore', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, $cookieStore, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        vm.deleteFile = deleteFile;

        vm.DuAnId = null;


        vm.showDelBtn = false;

        const listName = "DuAn";

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayKy = {
            date: new Date()
        };
        vm.pickerNgayThucHien = {
            date: new Date()
        };
        vm.pickerNgayKetThucDuKien = {
            date: new Date()
        };
        vm.pickerNgayNghiemThu = {
            date: new Date()
        };
        vm.pickerNgayThanhLy = {
            date: new Date()
        };
        vm.pickerNgayThucTeBatDau = {
            date: new Date()
        };
        vm.pickerNgayThucTeKetThuc = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
        vm.formFormat = { number: { numeral: true } };

        // initalize controller
        init();


        // initalize controller
        function init() {
            
            const projectId = $routeParams.projectId || $cookieStore.get('projectId');
            if (+projectId > 0){
                vm.DuAnId = projectId; // set global projectId
                $cookieStore.put('projectId', projectId) //Store to cookie to get future

                getItem(vm.DuAnId);
                vm.showDelBtn = true;

                // lấy danh sách văn bản
                getDsVanBan();
                const filterByDuAn = '(DuAnId eq ' + vm.DuAnId +')';
                getListExpand('NguonVon','Id,Title,Ma,LoaiVon/Title,KeHoachThu,KeHoachChi,ThucTeThu,ThucTeChi,SoDuVon','LoaiVon',filterByDuAn); //listName,select,expand,filter
    
                //enbaleEdit();

            } else  {
                common.logger.log('Dự án không tồn tại!', null, controllerId);
                $window.location.href = 'default.aspx'; // Quay về trang chủ
            }

        }

        // Hàm kiểm tra điều kiện cho phép chỉnh sửa
        function enbaleEdit(){
            if ($routeParams.action && $routeParams.action === 'edit') {
                vm.flagCanEdit = true;

                // Lấy danh sách từ list lọc
                getListExpand('GoiThau','Id,Title,HangMucCongTrinhId',null,"(DuAnId eq "+ vm.DuAnId +")"); //listName,select,expand,filter
                getListExpand('DoiTac','Id,Title,Ma',null,null); //listName,select,expand,filter
                getListExpand('PhanLoaiHopDong','Id,Title,Ma',null,null); //listName,select,expand,filter
                getListExpand('HoSoVanBan','Id,Title,SoHieuVanBan',null,"(DuAnId eq "+ vm.DuAnId +")"); //listName,select,expand,filter
            } else {
                vm.flagCanEdit = false;
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.HopDong();
                saveItem = vm.ctDuAn;

                saveItem.DuAnId = vm.DuAnId; //Set Id tĩnh

                // Xóa các thuộc tính expand
                [
                    'DuAn','LoaiChiPhi','GoiThau','HangMuc','NguonVon',
                    'DoiTac','PhanLoaiHopDong','ChiPhi','HoSoVanBan',
                    'Author','Editor','AttachmentFiles'
                ].forEach( (e) => delete saveItem[e] )

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (+vm.uploader.queue.length > 0){
                        const itemId = data.Id || data.d.Id;
                        saveUploadFile(null, listName, itemId)
                    } else {
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        goBack();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Tham số listName, currentItem
                    datacontext.deleteItemOfList(listName, vm.ctDuAn)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thông tin.", data, controllerId);
                    })
                    .then(function () {
                        goBack();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa.', error, controllerId);
                    });

                }
              })

            
        }

        // load the item specified in the route
        function getItem(itemId) {

            // set array property fields item
            const select = [
                'Id','Title','Ma','Created','Modified',
                'LinhVuc','TinhTrang','NamThucHien','MoTa',
                'TienTrinh,DienTichDat,HinhThucQuanLy,PhuongAnGiaiPhongMatBang',
                'ChuDauTuId,ChuDauTu/Title,NguoiQuyetDinhDauTuId,NguoiQuyetDinhDauTu/Title',
                'ToChucThamDinhDuAnId,ToChucThamDinhDuAn/Title,DiaDiemId,DiaDiem/Title',
                'ToChucQuanLyDuAnId,ToChucQuanLyDuAn/Title,ToChucLapDuAnId,ToChucLapDuAn/Title',
                'ChuNhiemLapDuAnId,ChuNhiemLapDuAn/Title,NhomDuAnId,NhomDuAn/Title',
                'LoaiCongTrinhId,LoaiCongTrinh/Title,CapCongTrinhId,CapCongTrinh/Title',
                'Editor/Title,Author/Title',
                'AttachmentFiles/FileName,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileSize'
            ].join();
            const expand = [
                'ChuDauTu','NguoiQuyetDinhDauTu','ToChucThamDinhDuAn',
                'DiaDiem','ToChucQuanLyDuAn','ToChucLapDuAn',
                'ChuNhiemLapDuAn','NhomDuAn','LoaiCongTrinh','CapCongTrinh',
                'Author','Editor','AttachmentFiles'
            ].join();

            let itemObj = new ict24h.models.DuAn();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand).
            then(function (data) {
                vm.ctDuAn = data;
            }).
            catch(function (error) {
                common.logger.logError('Không thể lấy thông tin.', error, controllerId);
            });
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {


        });

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Lấy danh sách văn bản cho phần thiết kế
         * @param {String} listName,select,expand,filter
         * @param {String} tenDanhSach
         */
        function getDsVanBan(){

            const NhomVanBanCanLay = [
                13, // Id của 'Chủ trương lập dự án đầu tư'
                5, // Id của 'Báo cáo đầu tư'
                15, // Id của 'Dự toán chuẩn bị đầu tư'
                14, // Id của 'Hoàn thành công trình đưa vào sử dụng'
                19, // Id của 'Danh mục hồ sơ hoàn thành công trình'
            ]


            const listName = 'HoSoVanBan',
            select ='Id,Title,SoHieuVanBan,NgayKy,GiaTri,NoiDung,HangMucCongTrinhId,HangMucCongTrinh/Title,ToChucPhatHanh/Id,ToChucPhatHanh/Title,LoaiVanBan/Title,LoaiVanBanId',
            expand = 'ToChucPhatHanh,LoaiVanBan,HangMucCongTrinh';
            let filter = '(DuAnId eq '+ vm.DuAnId +') and ';
            for (let i in NhomVanBanCanLay){ // Chỉ lấy theo id nhóm văn bản cho trước
                if (i > 0){ filter += ' or ' }
                filter += '(LoaiVanBanId eq '+ NhomVanBanCanLay[i] +')';
            }

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        
                        for (let e of data){
                            e.TenLoaiVanBan = e.LoaiVanBan.Title;
                        }
                        
                        vm.DsVanBan = groupBy(data, 'TenLoaiVanBan')

                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Hàm nhóm kết quả theo một thuộc tính
         * @param {*} objectArray 
         * @param {*} property 
         */
        function groupBy(objectArray, property) {
            return objectArray.reduce(function (acc, obj) {
                var key = obj[property];
                if (!acc[key]) {
                    acc[key] = [];
                }
                acc[key].push(obj);
                return acc;
            }, {});
        }


        /**
         * Xử lý UPLOAD
         */
		vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

		//Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                goBack();
            }
            
        }
        
        // Handle deletel attachment file
        //delFile(listName, itemId, fileName)
        function deleteFile(fileName){
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Khai báo tham số
                    const itemId = vm.ctDuAn.Id;
                    // Ẩn nút lưu
                    vm.btnSave = true;
                    AttachmentSvc.delFile(listName, itemId, fileName).
                    then((data) => {
                        // Khi xóa xong thì xuất thông tin
                        common.logger.logSuccess("Xóa tệp tin thành công", data, controllerId);
                        
                        // get item data
                        getItem(itemId);
                        // Active Save button
                        vm.btnSave = false;

                    } ).catch( (error) => {
                        common.logger.logError('Không thể xóa tệp tin!', error, controllerId)
                        // Active Save button
                        vm.btnSave = false;
                    } );

                }
            })
        }


    }

})();