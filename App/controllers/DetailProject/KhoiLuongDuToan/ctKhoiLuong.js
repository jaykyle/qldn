(function () {
    'use strict';

    // define controller
    var controllerId = "ctKhoiLuongDuToan";
    angular.module('app').
    controller(controllerId, ['$window', '$routeParams','$cookieStore', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, $cookieStore, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        vm.deleteFile = deleteFile;

        vm.DuAnId = null;

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const idItem = +$routeParams.idKhoiLuongDuToan;

        const listName = "QuanLyKhoiLuongDuToan";

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayKy = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
		vm.formFormat = {
            ChiPhiDuAn: { numeral: true },
            ChiPhiDuToan: { numeral: true },
            GiaTriHopDongDieuChinh: { numeral: true },
            KLTTKH: { numeral: true },
            KLTTThucTe: { numeral: true },
            KLNTKH: { numeral: true },
            KLNTThucTe: { numeral: true },
            KLThucTeThucHien: { numeral: true },
            TTVaTamUng: { numeral: true },
            TienChiTT: { numeral: true },
            TienChiTamUng: { numeral: true },
            TienThuTT: { numeral: true },
            TienThuTamUng: { numeral: true },
        };

        // initalize controller
        init();


        // initalize controller
        function init() {
            
            const projectId = $routeParams.projectId || $cookieStore.get('projectId');
            if (+projectId > 0){
                vm.DuAnId = projectId; // set global projectId
                $cookieStore.put('projectId', projectId) //Store to cookie to get future

                // STAR NORMAL INIT
                if (idItem && +idItem > 0) {
                    getItem(idItem);
                    vm.showDelBtn = true;
                } else {
                    createItem();
                }
    
                enbaleEdit();

            } else  {
                common.logger.log('Dự án không tồn tại!', null, controllerId);
                $window.location.href = 'default.aspx'; // Quay về trang chủ
            }

        }

        // Hàm kiểm tra điều kiện cho phép chỉnh sửa
        function enbaleEdit(){
            if ($routeParams.action && $routeParams.action === 'edit') {
                vm.flagCanEdit = true;

                // Lấy danh sách từ list lọc
                const filterByDuAn = '(DuAnId eq '+ vm.DuAnId + ')';
                getListExpand('LoaiChiPhiDuAn','Id,Title,Ma',null,filterByDuAn); //listName,select,expand,filter
                getListExpand('LoaiChungTu','Id,TenLoaiChungTu',null,null); //listName,select,expand,filter
                getListExpand('GoiThau','Id,Title',null,null); //listName,select,expand,filter
            } else {
                vm.flagCanEdit = false;
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.QuanLyKhoiLuongDuToan();
                saveItem = vm.ctKhoiLuong;

                saveItem.DuAnId = vm.DuAnId; //Set Id tĩnh

                // Xóa các thuộc tính expand
                [
                    'DuAn','LoaiChiPhi','GoiThau','LoaiChungTu',
                    'Author','Editor','AttachmentFiles'
                ].forEach( (e) => delete saveItem[e] )

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (+vm.uploader.queue.length > 0){
                        const itemId = data.Id || data.d.Id;
                        saveUploadFile(null, listName, itemId)
                    } else {
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        goBack();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Tham số listName, currentItem
                    datacontext.deleteItemOfList(listName, vm.ctKhoiLuong)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thông tin.", data, controllerId);
                    })
                    .then(function () {
                        goBack();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa.', error, controllerId);
                    });

                }
              })

            
        }

        // create a Item
        function createItem() {
            vm.ctKhoiLuong = new ict24h.models.QuanLyKhoiLuongDuToan();
        }

        // load the item specified in the route
        function getItem(itemId) {

            // set array property fields item
            const select = [
                'Id','MaChungTu','Created','Modified',
                'NgayChungTu,Active,GhiChu',
                'LoaiChungTuId,LoaiChungTu/TenLoaiChungTu',
                'LoaiChiPhiId,LoaiChiPhi/Title',
                'GoiThauId,GoiThau/Title',
                'Editor/Title,Author/Title',
                'AttachmentFiles/FileName,AttachmentFiles/ServerRelativeUrl'
            ].join();
            const expand = [
                'LoaiChiPhi','GoiThau','LoaiChungTu',
                'Author','Editor','AttachmentFiles'
            ].join();

            let itemObj = new ict24h.models.QuanLyKhoiLuongDuToan();

            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand).
            then(function (data) {
                vm.ctKhoiLuong = data;

                // Có Id Khối lượng toán thì lấy danh sách khối lượng công tác
                const filterByParentId = '(QuanLyKhoiLuongDuToanId eq '+ data.Id +')'
                getListExpand('KhoiLuongCongTac','Id,Ma,TenCongTac,DonViTinh,KhoiLuong,DonGiaCoThue,TienSauThue,ChiPhi/Title','ChiPhi',filterByParentId); //listName,select,expand,filter
            }).
            catch(function (error) {
                common.logger.logError('Không thể lấy thông tin.', error, controllerId);
            });
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {


        });

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Xử lý UPLOAD
         */
		vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

		//Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                goBack();
            }
            
        }
        
        // Handle deletel attachment file
        //delFile(listName, itemId, fileName)
        function deleteFile(fileName){
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Khai báo tham số
                    const itemId = vm.ctKhoiLuong.Id;
                    // Ẩn nút lưu
                    vm.btnSave = true;
                    AttachmentSvc.delFile(listName, itemId, fileName).
                    then((data) => {
                        // Khi xóa xong thì xuất thông tin
                        common.logger.logSuccess("Xóa tệp tin thành công", data, controllerId);
                        
                        // get item data
                        getItem(itemId);
                        // Active Save button
                        vm.btnSave = false;

                    } ).catch( (error) => {
                        common.logger.logError('Không thể xóa tệp tin!', error, controllerId)
                        // Active Save button
                        vm.btnSave = false;
                    } );

                }
            })
        }


    }

})();