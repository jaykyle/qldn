(function () {
    'use strict';

    // define controller
    var controllerId = "dsChiPhiDuToan";
    angular.module('app').controller(controllerId,
        ['$location', '$window','$routeParams','$cookieStore','common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location,$window,$routeParams,$cookieStore, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        vm.delItem = delItem;
        vm.createItem = createItem;
        vm.DuAnId = null;

        const listName = "ChiPhi";

        // Filter item 
        vm.getListItemsFiltered = getListItemsFiltered;
        vm.CacheFilterData = {}; // Lưu trạng thái bộ lọc (filter) ở view
        vm.cacheFilterKeyword = undefined; // Lưu trạng thái từ cần tìm của filter


        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };

        vm.pickerNgayKy = {
            date: new Date()
        };
        vm.pickerNgayPhatHanh = {
            date: new Date()
        };
        vm.pickerNgayNop = {
            date: new Date()
        };
        vm.pickerNgayHetHan = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
        vm.formFormat = {
            ChiPhiDuAn: { numeral: true },
            ChiPhiDuToan: { numeral: true },
            GiaTriHopDongDieuChinh: { numeral: true },
            KLTTKH: { numeral: true },
            KLTTThucTe: { numeral: true },
            KLNTKH: { numeral: true },
            KLNTThucTe: { numeral: true },
            KLThucTeThucHien: { numeral: true },
            TTVaTamUng: { numeral: true },
            TienChiTT: { numeral: true },
            TienChiTamUng: { numeral: true },
            TienThuTT: { numeral: true },
            TienThuTamUng: { numeral: true },
        }
        

        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            const projectId = $routeParams.projectId || $cookieStore.get('projectId');
            if (+projectId > 0){
                vm.DuAnId = projectId; // set global projectId
                $cookieStore.put('projectId', projectId) //Store to cookie to get future

                common.logger.log("controller loaded", null, controllerId);
                common.activateController([], controllerId);

                // Tải toàn bộ danh sách
                getListItems();

                // Lấy danh sách từ list lọc
                const filterByDuAn = '(DuAnId eq '+ vm.DuAnId + ')';
                getListExpand('LoaiChiPhiDuAn','Id,Title,Ma',null,filterByDuAn); //listName,select,expand,filter
                //getListExpand('DuAn','Id,Title,Ma',null,null); //listName,select,expand,filter
                getListExpand('GoiThau','Id,Title,HangMucCongTrinhId',null,null); //listName,select,expand,filter
                //getListExpand('HangMucCongTrinh','Id,Title',null,null); //listName,select,expand,filter
                getListExpand('NguonVon','Id,Title,Ma',null,null); //listName,select,expand,filter

            } else  {
                common.logger.log('Dự án không tồn tại!', null, controllerId);
                $window.location.href = 'default.aspx'; // Quay về trang chủ
            }
            
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/chiphidutoan/' + item.Id);
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const select = "Id,Title,Ma,ChiPhiDuAn,ChiPhiDuToan,DuAn/Title,LoaiChiPhi/Title,GoiThau/Title,HangMucCongTrinh/Title";
            const expand = 'DuAn,LoaiChiPhi,GoiThau,HangMucCongTrinh';
            const filter = "(DuAnId eq "+ vm.DuAnId +")";

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsChiPhi = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        /**
         * Lấy lại danh sách item kèm điều kiện filter
         */
        function getListItemsFiltered() {

            vm.dsChiPhi = undefined; // Để gọi màn hình loading...
            
            const select = "Id,Title,Ma,ChiPhiDuAn,ChiPhiDuToan,DuAn/Title,LoaiChiPhi/Title,GoiThau/Title,HangMucCongTrinh/Title,NguonVon/Title",
            expand = 'DuAn,LoaiChiPhi,GoiThau,HangMucCongTrinh,NguonVon'; //'NhanVien,PhongBan';
            let filter = "";
            let noiDieuKien = '';

            // Luôn luôn lkocj theo Id dự án
            vm.CacheFilterData.DuAnId = {
                TruongCanLoc: 'DuAnId',
                PhepSoSanh: 'eq',
                GiaTriLoc: vm.DuAnId
            }
                let countIndex = 0
                for (let elm in vm.CacheFilterData){
                    if(vm.CacheFilterData[elm]){
                        if(countIndex > 0 ){ //dòng thứ 2 mới nối điều kiện
                            noiDieuKien = ' and ';
                        }
    
                        filter += `${noiDieuKien}(${vm.CacheFilterData[elm].TruongCanLoc} ${vm.CacheFilterData[elm].PhepSoSanh} ${vm.CacheFilterData[elm].GiaTriLoc})`
                        countIndex++;
                    }
                }


            if (vm.cacheFilterKeyword) {
                if (filter !== '') { filter += " and " } // Thêm thêm and vào nếu có điều kiện lọc
                filter += "((substringof('"+ vm.cacheFilterKeyword +"',Ma)) or (substringof('"+ vm.cacheFilterKeyword +"',Title)))";
            }

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
            .then(function (data) {
                if (data) {
                    vm.dsChiPhi = data;
                } else {
                    throw new Error('Không có dữ liệu!');
                }
            }).catch(function (error) {
                common.logger.logError('Không thể tải thông tin', error, controllerId);
            });
        }

        // Xoa mot item
        function delItem(item) {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                if (result.value) {

                    datacontext.deleteItemOfList(listName, item)
                        .then(function () {
                            common.logger.logSuccess("Đã xóa thành công!", null, controllerId);

                            // Tải toàn bộ danh sách
                            getListItems();
                        })
                        .catch(function (error) {
                            common.logger.logError('Không thể xóa!', error, controllerId);
                        });
                }
            })
        }

        // handle save action
        function createItem() {
            vm.btnSave = true;

            // Create new obj model save item data
            let saveItem = new ict24h.models.ChiPhi();

            // Loop property saveItem
            for (let property in saveItem) {
                if (saveItem.hasOwnProperty(property) && vm.ctChiPhi[property]) {
                    saveItem[property] = vm.ctChiPhi[property]
                }
            }

            saveItem.DuAnId = vm.DuAnId; //Set Id dự án tĩnh từ URL

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    
                    /** Upload attachment */
					if (+vm.uploader.queue.length > 0) {
						//get file selected
						const itemId = +data.d.Id;;
                        saveUploadFile(null, listName, itemId)

					} else {
						// Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin", data, controllerId);
                        // Reset form
                        vm.ctChiPhi = {};
                        // Active lại nút lưu
                        vm.btnSave = false;
                        // Ẩn slider bar bên phải
                        toggleRightSidebar();
                        // Gọi lại danh sách Nhóm dự án
                        getListItems();
					}

                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin', error, controllerId);
                    vm.btnSave = false;
                });
        }

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                    common.logger.log('Lấy dữ liệu thành công', data, controllerId);

                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        //Bật/tắt sliderbar bên phải (thêm item)
        function toggleRightSidebar(){
            $(".right-sidebar").toggleClass("shw-rside");
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {

            // ============================================================== 
            // Right sidebar options
            // ============================================================== 

            $(".right-side-action-toggle").click(function () {
                $(".right-sidebar").slideDown(50);
                $(".right-sidebar").toggleClass("shw-rside");
            });

            $('.slimscrollright').slimScroll({
                height: '100%',
                position: 'right',
                size: "5px",
                color: '#dcdcdc'
            });

            $('.floating-labels .form-control').on('focus blur', function (e) {
                $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');

            // ============================================================== 
            // End Right sidebar options
            // ============================================================== 



        });




        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }

        /**
         * Xử lý UPLOAD
         */
        vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

        //Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                // Reset form
                vm.ctChiPhi = {};
                // Active lại nút lưu
                vm.btnSave = false;
                // Ẩn slider bar bên phải
                toggleRightSidebar();
                // Gọi lại danh sách Nhóm dự án
                getListItems();
            }
            
        }

        




    };

})();