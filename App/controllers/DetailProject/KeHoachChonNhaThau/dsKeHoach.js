(function () {
    'use strict';

    // define controller
    var controllerId = "dsKeHoachChonNhaThau";
    angular.module('app').controller(controllerId,
        ['$location','$routeParams','$cookieStore', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location,$routeParams,$cookieStore, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;
     
        vm.delItem = delItem;
        vm.gotoItem =gotoItem;
        vm.CapNhatTrangThai = CapNhatTrangThai;
        const listName = "KeHoachChonNhaThau";

        vm.DuAnId = null; // Khai báo dự án id
        // Filter item 
        vm.getListItemsFiltered = getListItemsFiltered;
        vm.CacheFilterData = {}; // Lưu trạng thái bộ lọc (filter) ở view
        vm.cacheFilterKeyword = undefined; // Lưu trạng thái từ cần tìm của filter

        vm.dsTrangThaiLoc = [ // Tạo danh sách Trạng thái theo Obj để dung select search
            {Id:"'Chưa xác định'",Title:"Chưa xác định"},
            {Id:"'Đã thực hiện'",Title:"Đã thực hiện"},
            {Id:"'Chưa đủ điều kiện lập kế hoạch'",Title:"Chưa đủ điều kiện lập kế hoạch"},
            {Id:"'Không áp dụng chọn nhà thầu'",Title:"Không áp dụng chọn nhà thầu"},
        ];

        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {

            const projectId = $routeParams.projectId || $cookieStore.get('projectId');
            if (+projectId > 0){
                vm.DuAnId = projectId; // set global projectId
                $cookieStore.put('projectId', projectId) //Store to cookie to get future

                common.logger.log("controller loaded", null, controllerId);
                common.activateController([], controllerId);

                // Tải toàn bộ danh sách
                getListItems();

                // Tải danh sách bộ lọc
                getListExpand('HinhThucChonNhaThau','Id,Title',null,null);
                getListExpand('LoaiHopDong','Id,Title',null,null);
                getListExpand('PhuongThucChonNhaThau','Id,Title',null,null);
                getListExpand('DoiTac','Id,Title,Ma,TongDiem',null,null);

            } else  {
                common.logger.log('Dự án không tồn tại!', null, controllerId);
                $window.location.href = 'default.aspx'; // Quay về trang chủ
            }
         
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/quanlykhoiluongdutoan/' + item.Id);
            }
        }

        //CapNhatTrangThai
        function CapNhatTrangThai(item){
            
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.KeHoachChonNhaThau();  
            
            saveItem.TrangThai = item.TrangThai;
            saveItem.__metadata = item.__metadata;
            saveItem.__metadata.etag="*";
            saveItem.Id=item.Id;
            
            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (data){                        
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        getListItems();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);               
                });
        }

        // Lấy danh sách tất cả
        function getListItems() {
            var listName = "KeHoachChonNhaThau";
            const select = "*,ChiPhi/ChiPhiDuToan,GoiThau/Title,DoiTac/Title,HinhThucChonNhaThau/Title,PhuongThucChonNhaThau/Title,LoaiHopDong/Title";
            const expand = 'ChiPhi,GoiThau,DoiTac,HinhThucChonNhaThau,PhuongThucChonNhaThau,LoaiHopDong'; //'NhanVien,PhongBan';
            const filter = "(DuAnId eq "+ vm.DuAnId +")";

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsKeHoachChonNhaThau = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        // Xoa mot item
        function delItem(item) {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                if (result.value) {

                    datacontext.deleteItemOfList(listName, item)
                        .then(function () {
                            common.logger.logSuccess("Đã xóa thành công!", null, controllerId);

                            // Tải toàn bộ danh sách
                            getListItems();
                        })
                        .catch(function (error) {
                            common.logger.logError('Không thể xóa!', error, controllerId);
                        });
                }
            })
        }

        /**
         * Lấy lại danh sách item kèm điều kiện filter
         */
        function getListItemsFiltered() {
            vm.dsKeHoachChonNhaThau = undefined; // Để gọi màn hình loading...
            
            const select = "Id,Title,GoiThau/Title,ChiPhi/ChiPhiDuToan,HinhThucChonNhaThau/Title,LoaiHopDong/Title,PhuongThucChonNhaThau/Title,GiaDuThau,GiaTrungThau,DoiTac/Title,TrangThai",
            expand = 'ChiPhi,GoiThau,DoiTac,HinhThucChonNhaThau,PhuongThucChonNhaThau,LoaiHopDong'; //'NhanVien,PhongBan';
            let filter = "";
            let noiDieuKien = '';

            // Luôn luôn lkocj theo Id dự án
            vm.CacheFilterData.DuAnId = {
                TruongCanLoc: 'DuAnId',
                PhepSoSanh: 'eq',
                GiaTriLoc: vm.DuAnId
            }
                let countIndex = 0
                for (let elm in vm.CacheFilterData){
                    if(vm.CacheFilterData[elm]){
                        if(countIndex > 0 ){ //dòng thứ 2 mới nối điều kiện
                            noiDieuKien = ' and ';
                        }
    
                        filter += `${noiDieuKien}(${vm.CacheFilterData[elm].TruongCanLoc} ${vm.CacheFilterData[elm].PhepSoSanh} ${vm.CacheFilterData[elm].GiaTriLoc})`
                        countIndex++;
                    }
                }


            if (vm.cacheFilterKeyword) {
                if (filter !== '') { filter += " and " } // Thêm thêm and vào nếu có điều kiện lọc
                filter += "(substringof('"+ vm.cacheFilterKeyword +"',Title))";
            }

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
            .then(function (data) {
                if (data) {
                    vm.dsKeHoachChonNhaThau = data;
                } else {
                    throw new Error('Không có dữ liệu!');
                }
            }).catch(function (error) {
                common.logger.logError('Không thể tải thông tin', error, controllerId);
            });
        }

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName Tên của list cần lấy
         * @param {String} select Chọn trường thông tin
         * @param {String} expand trường cần mở rộng
         * @param {String} filter trường lọc
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                    common.logger.log('Lấy dữ liệu thành công', data, controllerId);

                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }

    };

})();