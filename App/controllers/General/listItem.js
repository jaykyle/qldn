(function () {
    'use strict';

    // define controller
    var controllerId = "dsGeneral";
    angular.module('app').controller(controllerId,
        ['$location', '$routeParams', '$window', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($location, $routeParams, $window, common, datacontext) {
        let vm = this;

        vm.delItem = delItem;
        vm.createItem = createItem;
        vm.enabeledTooltips = enabeledTooltips; // Bind tooltip khi cần
        vm.runJqueryReady = runJqueryReady; //Chạy hàm Jquery khi load xong trang
        vm.toggleRightSidebar = toggleRightSidebar; // Bật/tắt thêm item bên phải
        vm.getListItemsFiltered = getListItemsFiltered; //Lấy lại ds item kèm điều kiện filter
        vm.getListExpand = getListExpand; // Lấy danh sách items từ list

        vm.CacheController = {}; // Lưu tạm thông tin Controller đc chọn hiện tại
        vm.CacheFilterData = {}; // Lưu trạng thái bộ lọc (filter) ở view
        vm.cacheFilterKeyword = undefined; // Lưu trạng thái từ cần tìm của filter
        vm.ctChildItem = {} // Thông tin item tạo mới
        
        // Tùy chọn cho định dạng input form
		vm.formFormat = {
			// Số đt
			phone: {
				phone: true,
				phoneRegionCode: 'VN'
			},
			// Dãy số
			number: {
				numeral: true
			},
			cmnd: {
				blocks: [3, 3, 3, 3, 3, 3],
				//prefix: 'PREFIX',
				//numeral: true,
				///delimiters: [' '],
			}
		}

        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {

            // Kiểm tra tên module
            checkModule();

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // Kiểm tra tên Module cần chạy
        function checkModule(){
            const moduleName = $routeParams.mod; // Lấy tên module từ URL ?module=
            if (moduleName && +moduleName.length > 0) { // Kiểm tra tồn tại của module Name
                switch(moduleName) {
                    case 'loaicongtrinh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại công trình',
                            model: new ict24h.models.LoaiCongTrinh(),
                            listName: 'LoaiCongTrinh',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsLoaiCongTrinh.html';
                        break;
                    case 'nhomduan':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'nhóm dự án',
                            model: new ict24h.models.NhomDuAn(),
                            listName: 'NhomDuAn',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsNhomDuAn.html';
                        break;
                    case 'capcongtrinh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'cấp công trình',
                            model: new ict24h.models.CapCongTrinh(),
                            listName: 'CapCongTrinh',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsCapCongTrinh.html';
                        break;
                    case 'loaidoitac':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại đối tác',
                            model: new ict24h.models.LoaiDoiTac(),
                            listName: 'LoaiDoiTac',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsLoaiDoiTac.html';
                        break;
                    case 'tinhthanh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'tỉnh thành',
                            model: new ict24h.models.TinhThanh(),
                            listName: 'TinhThanh',
                            select: "Id,Title,KhuVuc,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            saveProps: ['Title','WeatherCode','KhuVuc']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsTinhThanh.html';
                        break;
                    case 'doitac':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'đối tác',
                            model: new ict24h.models.DoiTac(),
                            listName: 'DoiTac',
                            select: "Id,Title,Ma,LoaiDoiTac/Title,NguoiLienHe,DienThoaiLienHe,TinhThanh/Title",
                            expand: "LoaiDoiTac,TinhThanh",
                            filter: null,
                            saveProps: ['Title','Ma','LoaiDoiTacId','DiaChi','NguoiDaiDien','ChucVuDaiDien','DienThoai','Fax','Email','Website','SoTaiKhoan','MaSoThue','NguoiLienHe','DienThoaiLienHe','TinhThanhId','GhiChu']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsDoiTac.html';
                        // 3 | Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('LoaiDoiTac', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        getListExpand('TinhThanh', 'Id,KhuVuc,Title', null, null); //listName,select,expand,filter
                        break;
                    case 'nhomvanban':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'nhóm văn bản',
                            model: new ict24h.models.NhomVanBan(),
                            listName: 'NhomVanBan',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsNhomVanBan.html';
                        break;
                    case 'loaivanban':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại văn bản',
                            model: new ict24h.models.LoaiVanBan(),
                            listName: 'LoaiVanBan',
                            select: 'Id,Title,Ma,NhomVanBan/Title',
                            expand: 'NhomVanBan',
                            filter: null,
                            saveProps: ['Title','Ma','NhomVanBanId']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsLoaiVanBan.html';
                        // 3 |Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('NhomVanBan', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        break;
                    case 'hangmuccongtrinh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'hạng mục công trình',
                            model: new ict24h.models.HangMucCongTrinh(),
                            listName: 'HangMucCongTrinh',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsHangMucCongTrinh.html';
                        break;
                    case 'phongban':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'phòng ban',
                            model: new ict24h.models.PhongBan(),
                            listName: 'PhongBan',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsPhongBan.html';
                        break;
                    case 'loaivon':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại vốn',
                            model: new ict24h.models.LoaiVon(),
                            listName: 'LoaiVon',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsLoaiVon.html';
                        break;
                    case 'nguonvon':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'nguồn vốn',
                            model: new ict24h.models.NguonVon(),
                            listName: 'NguonVon',
                            select: 'Id,Title,Ma,KeHoachThu,KeHoachChi,ThucTeThu,ThucTeChi,SoDuVon,LoaiVon/Title,DuAn/Title',
                            expand: 'DuAn,LoaiVon',
                            filter: null,
                            saveProps: ['Title','Ma','KeHoachThu','KeHoachChi','ThucTeThu','ThucTeChi','SoDuVon','DuAnId','LoaiVonId']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsNguonVon.html';
                        // 3 |Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('DuAn', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        getListExpand('LoaiVon', 'Id,Ma,Title', null, null);
                        break;
                    case 'loaichiphi':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại chi phí',
                            model: new ict24h.models.LoaiChiPhi(),
                            listName: 'LoaiChiPhi',
                            select: 'Id,Title,Ma,GiaTriDuAnDuocDuyet,GiaTriDuToanDuocDuyet',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma','GiaTriDuAnDuocDuyet','GiaTriDuToanDuocDuyet']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsLoaiChiPhi.html';
                        break;
                    case 'loaicongviec':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại công việc',
                            model: new ict24h.models.LoaiCongViec(),
                            listName: 'LoaiCongViec',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsLoaiCongViec.html';
                        break;
                    case 'chucvu':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'chức vụ',
                            model: new ict24h.models.ChucVu(),
                            listName: 'ChucVu',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsChucVu.html';
                        break;
                    case 'hocvan':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'học vấn',
                            model: new ict24h.models.HocVan(),
                            listName: 'HocVan',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsHocVan.html';
                        break;
                    case 'bophan':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'bộ phận',
                            model: new ict24h.models.BoPhan(),
                            listName: 'BoPhan',
                            select: 'Id,Title',
                            expand: null,
                            filter: null,
                            saveProps: ['Title']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsBoPhan.html';
                        break;
                    case 'goithau':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'gói thầu',
                            model: new ict24h.models.GoiThau(),
                            listName: 'GoiThau',
                            select: 'Id,Title,DuAn/Title,HangMucCongTrinh/Title',
                            expand: 'HangMucCongTrinh,DuAn',
                            filter: null,
                            saveProps: ['Title','HangMucCongTrinhId','DuAnId']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsGoiThau.html';
                        // 3 |Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('HangMucCongTrinh', 'Id,Title,Ma', null, null); //listName,select,expand,filter
                        getListExpand('DuAn', 'Id,Title,Ma', null, null); //listName,select,expand,filter
                        break;
                    case 'phanloaihopdong':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'phân loại hợp đồng',
                            model: new ict24h.models.PhanLoaiHopDong(),
                            listName: 'PhanLoaiHopDong',
                            select: 'Id,Title,Ma',
                            expand: null,
                            filter: null,
                            saveProps: ['Title','Ma']
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/dsPhanLoaiHopDong.html';
                        break;
                    default:
                        goBack(); // Không đáp ứng thì quay lại
                }

                // Tải toàn bộ danh sách
                getListItems();

            } else {
                goBack();
            }
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const listName = vm.CacheController.listName;
            const select = vm.CacheController.select;
            const expand = vm.CacheController.expand; //'NhanVien,PhongBan';
            const filter = vm.CacheController.filter;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsGeneral = data;
                    } else {
                        throw new Error('Không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải thông tin', error, controllerId);
                });
        }

        /**
         * Lấy lại danh sách item kèm điều kiện filter
         */
        function getListItemsFiltered() {
            
            const listName = vm.CacheController.listName;
            const select = vm.CacheController.select;
            const expand = vm.CacheController.expand; //'NhanVien,PhongBan';
            let filter = '';
            let noiDieuKien = '';

                let countIndex = 0
                for (let elm in vm.CacheFilterData){
                    if(vm.CacheFilterData[elm]){
                        if(countIndex > 0 ){ //dòng thứ 2 mới nối điều kiện
                            noiDieuKien = ' and ';
                        }
    
                        filter += `${noiDieuKien}(${vm.CacheFilterData[elm].TruongCanLoc} ${vm.CacheFilterData[elm].PhepSoSanh} ${vm.CacheFilterData[elm].GiaTriLoc})`
                        countIndex++;
                    }
                }


            if (vm.cacheFilterKeyword) {
                if (filter !== '') { filter += " and " } // Thêm thêm and vào nếu có điều kiện lọc
                filter += "((substringof('"+ vm.cacheFilterKeyword +"',Ma)) or (substringof('"+ vm.cacheFilterKeyword +"',Title)))";
            }

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
            .then(function (data) {
                if (data) {
                    vm.dsGeneral = data;
                } else {
                    throw new Error('Không có dữ liệu!');
                }
            }).catch(function (error) {
                common.logger.logError('Không thể tải thông tin', error, controllerId);
            });
        }

        /**
         * Xóa item đc chọn trong row
         * @param {Obj} item 
         */
        function delItem(item){
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    const listName = vm.CacheController.listName;

                    datacontext.deleteItemOfList(listName, item)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thành công!", data, controllerId);

                        // Tải toàn bộ danh sách
                        getListItems();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa!', error, controllerId);
                    });
                }
              })
        }

        /**
         * Tạo và lưu item mới
         */
        function createItem() {
            vm.btnSave = true;

            const listName = vm.CacheController.listName;
            // Tạo obj model mới để lưu dữ liệu
            let saveItem = vm.CacheController.model;

            // Kieemr tra ds props để lưu
            if (vm.CacheController.saveProps && +vm.CacheController.saveProps.length > 0){
                vm.CacheController.saveProps.forEach((elm) => {
                    saveItem[elm] = vm.ctChildItem[elm]
                })
            }

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin", data, controllerId);
                    
                })
                .then(function () {
                    // Reset form
                    vm.ctChildItem = {};
                    // Active lại nút lưu
                    vm.btnSave = false;
                    // Ẩn slider bar bên phải
                    $(".right-sidebar").toggleClass("shw-rside");
                    // Gọi lại danh sách Nhóm dự án
                    checkModule();
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin', error, controllerId);
                    vm.btnSave = false;
                });
        }

        /**
         * Hàm quay lại
         */
        function goBack() {
            $window.history.back();
        }

        /**
         * Khai báo hàm để lấy list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                    common.logger.log('Lấy dữ liệu thành công', data, controllerId);

                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Bật/tắt sliderbar thêm item bên phải
         */
        function toggleRightSidebar(){
                jQuery(".right-sidebar").slideDown(50);
                jQuery(".right-sidebar").toggleClass("shw-rside");
        }


        /**
         ###################################
         # BẮT ĐẦU CHUỐI HÀM TƯƠNG TÁC DOM #
         ###################################
         */

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {
            runJqueryReady();
        });

        /**
         * bind hàm jQuery khi DOM đã tải xong
         */
        function runJqueryReady(){
            // ============================================================== 
            // Right sidebar options
            // ============================================================== 
        
            
            jQuery('.slimscrollright').slimScroll({
                height: '100%'
                , position: 'right'
                , size: "5px"
                , color: '#dcdcdc'
            });
            
            jQuery('.floating-labels .form-control').on('focus blur', function (e) {
                jQuery(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');

            
            // ============================================================== 
            // End Right sidebar options
            // ============================================================== 

            // Tablesaw.init();
        }

        /**
         * Gán thuộc tính tooltip khi cần gọi
         */
        function enabeledTooltips() { $('[data-toggle="tooltip"]').tooltip(); }
        
        
        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }
        
    };

})();