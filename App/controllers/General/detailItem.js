(function () {
    'use strict';

    // define controller
    var controllerId = "ctGeneral";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

        vm.CacheController = {};
        vm.parseInt =parseInt;
        vm.round = Math.round;

        // Tùy chọn cho định dạng input form
		vm.formFormat = {
			// Số đt
			phone: {
				phone: true,
				phoneRegionCode: 'VN'
			},
			// Dãy số
			number: {
				numeral: true
			},
			cmnd: {
				blocks: [3, 3, 3, 3, 3, 3],
				//prefix: 'PREFIX',
				//numeral: true,
				///delimiters: [' '],
			}
		}


        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;

        vm.showDelBtn = false;
        vm.flagCanEdit = false;

        // if an ID is passed in, load the item
        const idItem = +$routeParams.idItem;
        
        //const listName = "LoaiCongTrinh";

        // initalize controller
        init();


        // initalize controller
        function init() {
            // Check module trước khi lấy thông tin item
            checkModule();

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // Kiểm tra tên Module cần chạy
        function checkModule(){
            const moduleName = $routeParams.mod; // Lấy tên module từ URL ?module=
            if (moduleName && +moduleName.length > 0) { // Kiểm tra tồn tại của module Name
                switch(moduleName) {
                    case "loaicongtrinh":
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại công trình',
                            model: new ict24h.models.LoaiCongTrinh(),
                            listName: 'LoaiCongTrinh',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctLoaiCongTrinh.html';
                        break;
                    case 'nhomduan':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'nhóm dự án',
                            model: new ict24h.models.NhomDuAn(),
                            listName: 'NhomDuAn',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctNhomDuAn.html';
                        break;
                    case 'capcongtrinh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'cấp công trình',
                            model: new ict24h.models.CapCongTrinh(),
                            listName: 'CapCongTrinh',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctCapCongTrinh.html';
                        break;
                    case 'loaidoitac':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại đối tác',
                            model: new ict24h.models.LoaiDoiTac(),
                            listName: 'LoaiDoiTac',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctLoaiDoiTac.html';
                        break;
                    case 'tinhthanh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'tỉnh thành',
                            model: new ict24h.models.TinhThanh(),
                            listName: 'TinhThanh',
                            select: "Id,Title,KhuVuc,WeatherCode,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctTinhThanh.html';
                        break;
                    case 'doitac':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'đối tác',
                            model: new ict24h.models.DoiTac(),
                            listName: 'DoiTac',
                            select: "Id,Title,Ma,LoaiDoiTacId,DiaChi,NguoiDaiDien,ChucVuDaiDien,DienThoai,Fax,Email,Website,SoTaiKhoan,MaSoThue,NguoiLienHe,DienThoaiLienHe,TinhThanhId,GhiChu,LoaiDoiTac/Id,LoaiDoiTac/Title,TinhThanh/Id,TinhThanh/Title,Created,Modified,Editor/Title,Author/Title,DanhGiaCungUngVatTu,DanhGiaCongNo,DanhGiaChatLuongThiCong,TongDiem",
                            expand: "Author,Editor,LoaiDoiTac,TinhThanh",
                            filter: null,
                            delProps: ['Author','Editor','LoaiDoiTac','TinhThanh'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctDoiTac.html';
                        // 3 | Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('LoaiDoiTac', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        getListExpand('TinhThanh', 'Id,KhuVuc,Title', null, null); //listName,select,expand,filter
                        break;
                    case 'nhomvanban':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'nhóm văn bản',
                            model: new ict24h.models.NhomVanBan(),
                            listName: 'NhomVanBan',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctNhomVanBan.html';
                        break;
                    case 'loaivanban':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại văn bản',
                            model: new ict24h.models.LoaiVanBan(),
                            listName: 'LoaiVanBan',
                            select: "Id,Title,Ma,NhomVanBanId,NhomVanBan/Id,NhomVanBan/Title,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor,NhomVanBan",
                            filter: null,
                            delProps: ['Author','Editor','NhomVanBan'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctLoaiVanBan.html';
                        // 3 | Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('NhomVanBan', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        break;
                    case 'hangmuccongtrinh':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'hạng mục công trình',
                            model: new ict24h.models.HangMucCongTrinh(),
                            listName: 'HangMucCongTrinh',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctHangMucCongTrinh.html';
                        break;
                    case 'phongban':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'phòng ban',
                            model: new ict24h.models.PhongBan(),
                            listName: 'PhongBan',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctPhongBan.html';
                        break;
                    case 'loaivon':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại vốn',
                            model: new ict24h.models.LoaiVon(),
                            listName: 'LoaiVon',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctLoaiVon.html';
                        break;
                    case 'nguonvon':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'nguồn vốn',
                            model: new ict24h.models.NguonVon(),
                            listName: 'NguonVon',
                            select: "Id,Title,Ma,KeHoachThu,KeHoachChi,ThucTeThu,ThucTeChi,SoDuVon,DuAnId,DuAn/Id,DuAn/Title,LoaiVonId,LoaiVon/Id,LoaiVon/Title,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor,DuAn,LoaiVon",
                            filter: null,
                            delProps: ['Author','Editor','DuAn','LoaiVon'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctNguonVon.html';
                        // 3 | Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('DuAn', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        getListExpand('LoaiVon', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        break;
                    case 'loaichiphi':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại chi phí',
                            model: new ict24h.models.LoaiChiPhi(),
                            listName: 'LoaiChiPhi',
                            select: "Id,Title,Ma,GiaTriDuAnDuocDuyet,GiaTriDuToanDuocDuyet,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctLoaiChiPhi.html';
                        break;
                    case 'loaicongviec':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'loại công việc',
                            model: new ict24h.models.LoaiCongViec(),
                            listName: 'LoaiCongViec',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctLoaiCongViec.html';
                        break;
                    case 'chucvu':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'chức vụ',
                            model: new ict24h.models.ChucVu(),
                            listName: 'ChucVu',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctChucVu.html';
                        break;
                    case 'hocvan':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'chức vụ',
                            model: new ict24h.models.HocVan(),
                            listName: 'HocVan',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctHocVan.html';
                        break;
                    case 'bophan':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'bộ phận',
                            model: new ict24h.models.BoPhan(),
                            listName: 'BoPhan',
                            select: "Id,Title,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctBoPhan.html';
                        break;
                    case 'goithau':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'gói thầu',
                            model: new ict24h.models.GoiThau(),
                            listName: 'GoiThau',
                            select: "Id,Title,DuAnId,DuAn/Id,DuAn/Title,HangMucCongTrinhId,HangMucCongTrinh/Id,HangMucCongTrinh/Title,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor,HangMucCongTrinh,DuAn",
                            filter: null,
                            delProps: ['Author','Editor','HangMucCongTrinh','DuAn'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctGoiThau.html';
                        // 3 | Chạy hàm lấy dữ liệu expand để thêm thông tin
                        getListExpand('HangMucCongTrinh', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        getListExpand('DuAn', 'Id,Ma,Title', null, null); //listName,select,expand,filter
                        break;
                    case 'phanloaihopdong':
                        // 1 | Khai báo thông tin
                        vm.CacheController = {
                            ctrlName: 'phân loại hợp đồng',
                            model: new ict24h.models.PhanLoaiHopDong(),
                            listName: 'PhanLoaiHopDong',
                            select: "Id,Title,Ma,Created,Modified,Editor/Title,Author/Title",
                            expand: "Author,Editor",
                            filter: null,
                            delProps: ['Author','Editor'] // thuộc tính không cho lưu khi expand
                        };
                        // 2 | Khai báo đường dẫn view
                        vm.viewInclude = cdnUrl + 'App/controllers/General/View/ctPhanLoaiHopDong.html';
                        break;
                    default:
                        common.logger.logError('Không tìm thấy module:', moduleName, controllerId);
                        goBack(); // Không đáp ứng thì quay lại
                }

                // Có Id rồi lấy thông tin
                if (idItem && +idItem > 0) {
                    getItem(idItem);
                    vm.showDelBtn = true;
                } else {
                    createItem();
                }
                // Kiểm tra xem cho edit không
                enbaleEdit();

            } else {
                goBack();
            }
        }

        // Hàm kiểm tra điều kiện cho phép chỉnh sửa
        function enbaleEdit(){

            if ($routeParams.action && $routeParams.action === 'edit') {
                vm.flagCanEdit = true;
            } else {
                vm.flagCanEdit = false;
            }
            
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            const listName = vm.CacheController.listName;
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  vm.CacheController.model;
                saveItem = vm.ctChildItem;

                // Kiểm tra và xóa các thuộc tính không cho lưu
                if (vm.CacheController.delProps && +vm.CacheController.delProps .length > 0){
                    vm.CacheController.delProps.forEach(function(elm){
                        delete saveItem[elm];
                    })
                }

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin", data, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    const listName = vm.CacheController.listName;

                    // Tham số listName, currentItem
                    datacontext.deleteItemOfList(listName, vm.ctChildItem)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thông tin.", data, controllerId);
                    })
                    .then(function () {
                        $window.history.back();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa thông tin.', error, controllerId);
                    });

                }
              })

            
        }

        // create a Item
        function createItem() {
            vm.ctChildItem = vm.CacheController.model;
        }

        // load the item specified in the route
        function getItem(itemId) {

            const listName = vm.CacheController.listName;
            const select = vm.CacheController.select;
            const expand = vm.CacheController.expand;

            let itemObj = vm.CacheController.model;


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctChildItem = data;
                }).catch(function (error) {
                    common.logger.logError('Không thể tải thông tin.', error, controllerId);
                });
        }

        // Khai báo hàm để lấy list expand khi cần
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                    common.logger.log('Lấy dữ liệu thành công', data, controllerId);

                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {


        });

        

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();