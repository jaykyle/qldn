(function () {
    'use strict';

    // define controller
    var controllerId = "ctHopDong";
    angular.module('app').
    controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        vm.deleteFile = deleteFile;

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const idItem = +$routeParams.idHopDong;

        const listName = "HopDong";

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayKy = {
            date: new Date()
        };
        vm.pickerNgayThucHien = {
            date: new Date()
        };
        vm.pickerNgayKetThucDuKien = {
            date: new Date()
        };
        vm.pickerNgayNghiemThu = {
            date: new Date()
        };
        vm.pickerNgayThanhLy = {
            date: new Date()
        };
        vm.pickerNgayThucTeBatDau = {
            date: new Date()
        };
        vm.pickerNgayThucTeKetThuc = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
        vm.formFormat = { number: { numeral: true } };

        // initalize controller
        init();


        // initalize controller
        function init() {
            
            if (idItem && +idItem > 0) {
                getItem(idItem);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            enbaleEdit();

        }

        // Hàm kiểm tra điều kiện cho phép chỉnh sửa
        function enbaleEdit(){
            if ($routeParams.action && $routeParams.action === 'edit') {
                vm.flagCanEdit = true;

                // Lấy danh sách từ list expand
                getListExpand('LoaiChiPhi','Id,Title,Ma',null,null); //listName,select,expand,filter
                getListExpand('DuAn','Id,Title,Ma',null,null); //listName,select,expand,filter
                getListExpand('GoiThau','Id,Title,HangMucCongTrinhId',null,null); //listName,select,expand,filter
                //getListExpand('HangMucCongTrinh','Id,Title',null,null); //listName,select,expand,filter
                getListExpand('NguonVon','Id,Title,Ma',null,null); //listName,select,expand,filter
            } else {
                vm.flagCanEdit = false;
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.HopDong();
                saveItem = vm.ctHopDong;

                // Xóa các thuộc tính expand
                [
                    'DuAn','LoaiChiPhi','GoiThau','HangMucCongTrinh','NguonVon',
                    'Author','Editor','AttachmentFiles'
                ].forEach( (e) => delete saveItem[e] )

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (+vm.uploader.queue.length > 0){
                        const itemId = data.Id || data.d.Id;
                        saveUploadFile(null, listName, itemId)
                    } else {
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        goBack();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Tham số listName, currentItem
                    datacontext.deleteItemOfList(listName, vm.ctHopDong)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thông tin.", data, controllerId);
                    })
                    .then(function () {
                        goBack();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa.', error, controllerId);
                    });

                }
              })

            
        }

        // create a Item
        function createItem() {
            vm.ctHopDong = new ict24h.models.HopDong();
        }

        // load the item specified in the route
        function getItem(itemId) {

            // set array property fields item
            const select = [
                'Id','Title','Created','Modified',
                'SoHopDong,NgayKy,NgayThucHien,ThoiGianThucHien',
                'ThoiGianDieuChinh,NgayKetThucDuKien,NgayNghiemThu',
                'NgayThanhLy,NgayThucTeBatDau,NgayThucTeKetThuc',
                'GiaTriHopDong,GiaTriDieuChinh,HienTrang,NoiDung',
                'DuAnId,DuAn/Id,DuAn/Title',
                'GoiThauId,GoiThau/Title',
                'NguonVonId,NguonVon/Title',
                'HangMucId,HangMuc/Title',
                'DoiTacId,DoiTac/Title',
                'HoSoVanBanId,HoSoVanBan/Ma,HoSoVanBan/Title,HoSoVanBan/SoHieuVanBan,HoSoVanBan/NgayKy',
                'PhanLoaiHopDongId,PhanLoaiHopDong/Title',
                'Editor/Title,Author/Title',
                'AttachmentFiles/FileName,AttachmentFiles/ServerRelativeUrl'
            ].join();
            const expand = [
                'HoSoVanBan',
                'DuAn','GoiThau','HangMuc','NguonVon','DoiTac','PhanLoaiHopDong',
                'Author','Editor','AttachmentFiles'
            ].join();

            let itemObj = new ict24h.models.HopDong();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand).
            then(function (data) {
                vm.ctHopDong = data;

                // Lấy danh sách khối lượng công tác theo Gói thầu
                getListExpand('KhoiLuongCongTac','Id,TenCongTac,KhoiLuong,DonGiaCoThue,TienSauThue,ChiPhi/Title','ChiPhi','GoiThauId eq ' + vm.ctHopDong.GoiThauId)
                // Lấy danh sách chi phí (dự toán) theo Gói thầu
                getListExpand('ChiPhi','Id,Title,TTVaTamUng',null,'GoiThauId eq ' + vm.ctHopDong.GoiThauId)

                //Lấy danh sách phụ lục theo Hợp đồng
                getListExpand('PhuLucHopDong','Id,Ma,Title,HopDongId,HopDong/Title,NgayKyPhuLuc,GiaTriPhuLuc,SuaDoiThoiGian,ThoiGianPhuLuc','HopDong','HopDongId eq ' + vm.ctHopDong.Id)

            }).
            catch(function (error) {
                common.logger.logError('Không thể lấy thông tin.', error, controllerId);
            });
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {


        });

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Xử lý UPLOAD
         */
		vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

		//Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                goBack();
            }
            
        }
        
        // Handle deletel attachment file
        //delFile(listName, itemId, fileName)
        function deleteFile(fileName){
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Khai báo tham số
                    const itemId = vm.ctHopDong.Id;
                    // Ẩn nút lưu
                    vm.btnSave = true;
                    AttachmentSvc.delFile(listName, itemId, fileName).
                    then((data) => {
                        // Khi xóa xong thì xuất thông tin
                        common.logger.logSuccess("Xóa tệp tin thành công", data, controllerId);
                        
                        // get item data
                        getItem(itemId);
                        // Active Save button
                        vm.btnSave = false;

                    } ).catch( (error) => {
                        common.logger.logError('Không thể xóa tệp tin!', error, controllerId)
                        // Active Save button
                        vm.btnSave = false;
                    } );

                }
            })
        }

        

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();