(function () {
    'use strict';

    // define controller
    var controllerId = "dsHopDong";
    angular.module('app').controller(controllerId,
        ['$location', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($location, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        vm.delItem = delItem;
        vm.createItem = createItem;
        vm.getListNeedDuAn = getListNeedDuAn;

        const listName = "HopDong";

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };

        vm.pickerNgayKy = {
            date: new Date()
        };
        vm.pickerNgayThucHien = {
            date: new Date()
        };
        vm.pickerNgayKetThucDuKien = {
            date: new Date()
        };
        vm.pickerNgayNghiemThu = {
            date: new Date()
        };
        vm.pickerNgayThanhLy = {
            date: new Date()
        };
        vm.pickerNgayThucTeBatDau = {
            date: new Date()
        };
        vm.pickerNgayThucTeKetThuc = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
        vm.formFormat = { number: { numeral: true } };
        

        // init controller
        init();

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();

            // Lấy danh sách từ list lọc
            getListExpand('DuAn','Id,Title,Ma',null,null); //listName,select,expand,filter
            getListExpand('GoiThau','Id,Title,HangMucCongTrinhId',null,null); //listName,select,expand,filter
            getListExpand('DoiTac','Id,Title,Ma',null,null); //listName,select,expand,filter
            getListExpand('PhanLoaiHopDong','Id,Title,Ma',null,null); //listName,select,expand,filter
            // getListExpand('NguonVon','Id,Title,Ma',null,null); //listName,select,expand,filter
            
        }

        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/hopdong/' + item.Id);
            }
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const select = "Id,Title,DuAn/Title,SoHopDong,NgayKy,NgayThucHien,NgayNghiemThu,GiaTriHopDong,GoiThau/Title,NguonVon/Title,HangMuc/Title,DoiTac/Title,PhanLoaiHopDong/Title,HienTrang";
            const expand = 'DuAn,GoiThau,HangMuc,NguonVon,DoiTac,PhanLoaiHopDong'; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsHopDong = data;
                    } else {
                        throw new Error('Lỗi, không có dữ liệu!');
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể tải danh sách', error, controllerId);
                });
        }

        function getListNeedDuAn(listName){
            if (+vm.ctHopDong.DuAnId > 0 ){
                const filter = 'DuAnId eq ' + vm.ctHopDong.DuAnId;
                getListExpand(listName,'Id,Title,Ma',null,filter); //listName,select,expand,filter
            } else {
                common.logger.logError('Bạn chưa chọn Dự án!', null, controllerId);
            }
            
        }

        // Xoa mot item
        function delItem(item) {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
            }).then((result) => {
                if (result.value) {

                    datacontext.deleteItemOfList(listName, item)
                        .then(function () {
                            common.logger.logSuccess("Đã xóa thành công!", null, controllerId);

                            // Tải toàn bộ danh sách
                            getListItems();
                        })
                        .catch(function (error) {
                            common.logger.logError('Không thể xóa!', error, controllerId);
                        });
                }
            })
        }

        // handle save action
        function createItem() {
            vm.btnSave = true;

            // Create new obj model save item data
            let saveItem = new ict24h.models.HopDong();

            // Loop property saveItem
            for (let property in saveItem) {
                if (saveItem.hasOwnProperty(property) && vm.ctHopDong[property]) {
                    saveItem[property] = vm.ctHopDong[property]
                }
            }

            // Lưu multiple lookup
            if (vm.ctHopDong.HoSoVanBanId && +vm.ctHopDong.HoSoVanBanId.length > 0) {
                saveItem.HoSoVanBanId = { 'results' : vm.ctHopDong.HoSoVanBanId}
            } else {
                saveItem.HoSoVanBanId = undefined;
            }
            

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    
                    /** Upload attachment */
					if (+vm.uploader.queue.length > 0) {
						//get file selected
						const itemId = +data.d.Id;
                        saveUploadFile(null, listName, itemId)

					} else {
						// Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin", data, controllerId);
                        // Reset form
                        vm.ctHopDong = {};
                        // Active lại nút lưu
                        vm.btnSave = false;
                        // Ẩn slider bar bên phải
                        toggleRightSidebar();
                        // Gọi lại danh sách Nhóm dự án
                        getListItems();
					}

                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin', error, controllerId);
                    vm.btnSave = false;
                });
        }

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        //Bật/tắt sliderbar bên phải (thêm item)
        function toggleRightSidebar(){
            $(".right-sidebar").toggleClass("shw-rside");
        }
        // Gán thuộc tính tooltip vì gọi nhiều lần
        function enabeledTooltips() {
            $('[data-toggle="tooltip"]').tooltip();
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {

            // ============================================================== 
            // Right sidebar options
            // ============================================================== 

            $(".right-side-action-toggle").click(function () {
                $(".right-sidebar").slideDown(50);
                $(".right-sidebar").toggleClass("shw-rside");
            });

            $('.slimscrollright').slimScroll({
                height: '100%',
                position: 'right',
                size: "5px",
                color: '#dcdcdc'
            });

            $('.floating-labels .form-control').on('focus blur', function (e) {
                $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');

            // ============================================================== 
            // End Right sidebar options
            // ============================================================== 

            // Gán tooltip
            enabeledTooltips();


        });




        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }

        /**
         * Xử lý UPLOAD
         */
        vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

        //Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                // Reset form
                vm.ctHopDong = {};
                // Active lại nút lưu
                vm.btnSave = false;
                // Ẩn slider bar bên phải
                toggleRightSidebar();
                // Gọi lại danh sách Nhóm dự án
                getListItems();
            }
            
        }

        




    };

})();