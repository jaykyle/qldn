(function () {
    'use strict';

    // define controller
    var controllerId = "dsDonHang";
    angular.module('app').controller(controllerId,
        ['$location', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($location, $routeParams, common, datacontext) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        // đường dẫn để thêm Item mới
        //vm.newItem = newItem;
        vm.getListItemsFilter = getListItemsFilter;

        vm.cacheFilter = {}; // Để lọc ở ngoài bộ tìm kiếm

        const listName = "DonHang";

        // init controller
        init();



        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/donhang/' + item.Id );
            }
        }

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();

            // Lấy danh sách filter
            getDsNhanVien();
            getDsSanPham();
            getDsKhachHang();
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const select = "Id,Title,SoLuong,ThanhTien,NhanVien/Title,KhachHang/Title,Status,CurrentStep";
            const expand = "NhanVien,KhachHang";
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsDonHang = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách đơn hàng');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách đơn hàng', error, controllerId);
                });
        }

        /**
         * Hàm lấy danh sách theo bộ lọc
         * ProductId
         * KhachHangId
         * NhanVienId
         * Status
         * @param {Object} fiterObj 
         */
        function getListItemsFilter(fiterObj) {

            const select = "Id,Title,SoLuong,ThanhTien,NhanVien/Title,KhachHang/Title,Status,CurrentStep";
            const expand = "NhanVien,KhachHang";
            let filter = '';
            let filterArr = [];

            
            //Nếu bộ lọc có Khách hàng
            if (vm.cacheFilter.KhachHangId && +vm.cacheFilter.KhachHangId > 0) {
                filterArr.push('(KhachHangId eq '+ vm.cacheFilter.KhachHangId +')');
            }
            //Nếu bộ lọc có nhân viên
            if (vm.cacheFilter.NhanVienId && +vm.cacheFilter.NhanVienId > 0) {
                filterArr.push('(NhanVienId eq '+ vm.cacheFilter.NhanVienId +')');
            }
            // Nối các phần tử trong Arr lại
            if (filterArr.length > 1){
                filter = filterArr.join(' and ');
            } else if (filterArr.length === 1){
                filter = filterArr[0];
            }
            debugger

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsDonHang = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách đơn hàng');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách đơn hàng', error, controllerId);
                });
        }


        // Lấy danh sách khách hàng
        function getDsKhachHang(keyword) {
			const listName = "KhachHang",
				select = "Id,Title,Ma", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',Ma)) or (substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsKhachHang = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách Khách hàng');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách khách hàng', error, controllerId) );
        }
        
        // Láy danh sách sản phẩm
        function getDsSanPham(keyword) {
			const listName = "Product",
				select = "Id,Title,Ma,Gia,DVT", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',Ma)) or (substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsSanPham = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách Sản phẩm');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách sản phẩm', error, controllerId) );
        }
        
        // Lấy danh sách nhân viên
        function getDsNhanVien(keyword) {
			const listName = "NhanVien",
				select = "Id,Title", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsNhanVien = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách nhân viên');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách nhân viên', error, controllerId) );
		}
        

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }
    };

})();