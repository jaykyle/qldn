(function () {
    'use strict';

    // define controller
    var controllerId = "ctDonHang";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        vm.getDsKhachHang = getDsKhachHang;
        vm.getDsSanPham = getDsSanPham;
        vm.getDsNhanVien = getDsNhanVien;

        // Phần sản phẩm trong đơn hàng
        vm.cacheSPTrongDonHang = [];
        vm.ctSanPhamTrongDonHang = {}
        vm.goSaveSPTrongDonHang = goSaveSPTrongDonHang;

        vm.showDelBtn = false;
        vm.formFormat = {
            number : { numeral: true }
        }

        vm.pickerNgayBan = {
			date: new Date()
		};
		vm.openCalendar = function (e, picker) {
			vm[picker].open = true;
        };
        
        // if an ID is passed in, load the item
        const DonHangId = +$routeParams.idDonHang;

        const listName = "DonHang";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (DonHangId && +DonHangId > 0) {
                getItem(DonHangId);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            // Lấy danh sách chọn lựa
            getDsKhachHang();
            getDsSanPham();
            getDsNhanVien();

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctDonHang) {
                return moment(vm.ctDonHang.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctDonHang) {
                return moment(vm.ctDonHang.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Xử lý mảng Sản phẩm trong đơn hàng
            vm.ctDonHang.SanPhamTrongDonHangId = { results: [] };
            vm.cacheSPTrongDonHang.forEach( (e) => vm.ctDonHang.SanPhamTrongDonHangId.results.push(e.Id) );

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.DonHang();
                saveItem.Title = vm.ctDonHang.Title;
                saveItem.SoLuong = vm.ctDonHang.SoLuong;
                saveItem.ThanhTien = vm.ctDonHang.ThanhTien;
                saveItem.GiaBan = vm.ctDonHang.GiaBan;
                saveItem.NgayBan = vm.ctDonHang.NgayBan;
                saveItem.NhanVienId = vm.ctDonHang.NhanVienId;
                saveItem.SanPhamTrongDonHangId = vm.ctDonHang.SanPhamTrongDonHangId || undefined;
                saveItem.KhachHangId = vm.ctDonHang.KhachHang.Id;
                saveItem.Status = 'Submit';
                saveItem.CurrentStep = 'TP'; // NV | TP | KT

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin đơn hàng.", data, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin đơn hàng', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle save action
        function goSaveSPTrongDonHang() {
            vm.btnSave = true;

            const listName = "SanPhamTrongDonHang"
            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.SanPhamTrongDonHang();
                saveItem.ProductId = vm.ctSanPhamTrongDonHang.Product.Id;
                saveItem.TenProduct = vm.ctSanPhamTrongDonHang.Product.Title;
                saveItem.SoLuong = vm.ctSanPhamTrongDonHang.SoLuong;
                saveItem.DVT = vm.ctSanPhamTrongDonHang.Product.DVT;
                saveItem.GiaBan = vm.ctSanPhamTrongDonHang.GiaBan;
                saveItem.ThanhTien = vm.ctSanPhamTrongDonHang.ThanhTien;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    
                    vm.cacheSPTrongDonHang.push(data.d)
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin sản phẩm.", data, controllerId);
                    vm.btnSave = false;

                    vm.ctSanPhamTrongDonHang = {};
                })
                .then(() => TongDonHang())
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin Nhân viên', error, controllerId);
                    vm.btnSave = false;
                });
        }
        //
        function TongDonHang(){
            vm.ctDonHang.SoLuong = vm.cacheSPTrongDonHang.reduce((a, b) => a + b.SoLuong , 0);
            vm.ctDonHang.ThanhTien = vm.cacheSPTrongDonHang.reduce((a, b) => a + b.ThanhTien , 0);;
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctDonHang)
                .then(function (data) {
                    common.logger.logSuccess("Đã xóa đơn hàng.", data, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa đơn hàng', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctDonHang = new ict24h.models.DonHang();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,SoLuong,ThanhTien,GiaBan,NgayBan,NhanVienId,ProductId,Status,CurrentStep,TPDuyetId,TPDuyetNgay,TPDuyetStatus,TPDuyetNote,KTDuyetId,KTDuyetNgay,KTDuyetStatus,KTDuyetNote";
            const expand = undefined;

            let itemObj = new ict24h.models.DonHang();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctDonHang = data;
                });
        }

        // Lấy danh sách khách hàng
        function getDsKhachHang(keyword) {
			const listName = "KhachHang",
				select = "Id,Title,Ma,NgayMuaCuoi", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',Ma)) or (substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsKhachHang = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách Khách hàng');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách khách hàng', error, controllerId) );
        }
        
        // Láy danh sách sản phẩm
        function getDsSanPham(keyword) {
			const listName = "Product",
				select = "Id,Title,Ma,Gia,DVT", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',Ma)) or (substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsSanPham = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách Sản phẩm');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách sản phẩm', error, controllerId) );
        }
        
        // Lấy danh sách nhân viên
        function getDsNhanVien(keyword) {
			const listName = "NhanVien",
				select = "Id,Title", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsNhanVien = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách nhân viên');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách nhân viên', error, controllerId) );
        }
        





    }

})();