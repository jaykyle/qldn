(function () {
    'use strict';

    // define controller
    var controllerId = "xemDonHang";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

        // navigate backwards in the history stack
        vm.goBack = goBack;
        vm.goDelete = goDelete;
       
        // if an ID is passed in, load the item
        const DonHangId = +$routeParams.idDonHang;

        const listName = "DonHang";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (DonHangId && +DonHangId > 0) {
                getItem(DonHangId);
                vm.showDelBtn = true;
            } else {
                goBack();
            }

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Title,SoLuong,ThanhTien,GiaBan,NgayBan,KhachHang/Id,KhachHang/Title,NhanVien/Title,SanPhamTrongDonHang/TenProduct,SanPhamTrongDonHang/SoLuong,SanPhamTrongDonHang/DVT,SanPhamTrongDonHang/GiaBan,SanPhamTrongDonHang/ThanhTien,Status,CurrentStep,Created,TPDuyet/Title,TPDuyetNgay,TPDuyetStatus,TPDuyetNote,KTDuyet/Title,KTDuyetNgay,KTDuyetStatus,KTDuyetNote,Author/Title";
            const expand = "KhachHang,NhanVien,SanPhamTrongDonHang,TPDuyet,KTDuyet,Author";

            let itemObj = new ict24h.models.DonHang();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctDonHang = data;
                });
        }

        // handle delete action
        function goDelete() {

            // Xóa các thông tin cho đỡ rối
            delete vm.ctDonHang.NhanVien;
            delete vm.ctDonHang.KhachHang;
            delete vm.ctDonHang.SanPhamTrongDonHang;
            delete vm.ctDonHang.KTDuyet;
            delete vm.ctDonHang.TPDuyet;
            delete vm.ctDonHang.Author;

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctDonHang)
                .then(function () {
                    common.logger.logSuccess("Đã xóa đơn hàng.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa đơn hàng', error, controllerId);
                });
        }





    }

})();