(function () {
    'use strict';

    // define controller
    var controllerId = "duyetDonHang";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', '$rootScope', 'common', 'datacontext', 'currentUser', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, $rootScope, common, datacontext, currentUser) {
        let vm = this;

          // navigate backwards in the history stack
        vm.goBack = goBack;
        vm.goSave = goSave;

        vm.showDelBtn = false;
        vm.cacheDuyet = {};
       
        // if an ID is passed in, load the item
        const DonHangId = +$routeParams.idDonHang;

        const listName = "DonHang";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (DonHangId && +DonHangId > 0) {
                getItem(DonHangId);
                vm.showDelBtn = true;
            } else {
                goBack();
            }

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Title,SoLuong,ThanhTien,GiaBan,NgayBan,KhachHang/Id,KhachHang/Title,KhachHang/NgayMuaCuoi,NhanVien/Id,NhanVien/Title,SanPhamTrongDonHang/TenProduct,SanPhamTrongDonHang/SoLuong,SanPhamTrongDonHang/DVT,SanPhamTrongDonHang/GiaBan,SanPhamTrongDonHang/ThanhTien,Status,CurrentStep,Created,TPDuyet/Title,TPDuyetNgay,TPDuyetStatus,TPDuyetNote,KTDuyet/Title,KTDuyetNgay,KTDuyetStatus,KTDuyetNote,Author/Title";
            const expand = "KhachHang,NhanVien,SanPhamTrongDonHang,TPDuyet,KTDuyet,Author";

            let itemObj = new ict24h.models.DonHang();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctDonHang = data;
                });
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem = vm.ctDonHang;

            //Copy lại dữ liệu khách hàng để lưu ngày mua cuối trước khi xóa
            let KhachHangCopy = vm.ctDonHang.KhachHang;
            KhachHangCopy.__metadata.etag = "*";
            
                delete saveItem.NhanVien;
                delete saveItem.KhachHang;
                delete saveItem.SanPhamTrongDonHang;
                delete saveItem.KTDuyet;
                delete saveItem.TPDuyet;
                delete saveItem.Author;

                // Trường hợp 1: Từ chối sẽ từ chối toàn bộ quy trình
                if (vm.ctDonHang.Status && vm.ctDonHang.Status ==='Submit' && vm.cacheDuyet.Status === 'Reject') {
                    saveItem.Status = 'Reject';
                    if (vm.ctDonHang.CurrentStep === 'TP'){
                        saveItem.TPDuyetId = $rootScope.CurrentUser.Id;
                        saveItem.TPDuyetNgay = new Date().toISOString();
                        saveItem.TPDuyetStatus = 'Reject';
                        saveItem.TPDuyetNote = vm.cacheDuyet.Note;
                    } else if (vm.ctDonHang.CurrentStep === 'KT') {
                        saveItem.KTDuyetId =  $rootScope.CurrentUser.Id;
                        saveItem.KTDuyetNgay = new Date().toISOString();
                        saveItem.KTDuyetStatus = 'Reject';
                        saveItem.KTDuyetNote = vm.cacheDuyet.Note;
                    }
                }
                // Handle trường hợp 2: Đồng ý duyệt
                if (vm.ctDonHang.Status && vm.ctDonHang.Status ==='Submit' && vm.cacheDuyet.Status === 'Approved'){
                    if (vm.ctDonHang.CurrentStep === 'TP'){
                        saveItem.CurrentStep = 'KT'; // NV | TP | KT
                        saveItem.TPDuyetId = $rootScope.CurrentUser.Id;
                        saveItem.TPDuyetNgay = new Date().toISOString();
                        saveItem.TPDuyetStatus = 'Approved';
                        saveItem.TPDuyetNote = vm.cacheDuyet.Note;
                    } else if (vm.ctDonHang.CurrentStep === 'KT') {
                        saveItem.Status = 'Approved';
                        saveItem.KTDuyetId =  $rootScope.CurrentUser.Id;
                        saveItem.KTDuyetNgay = new Date().toISOString();
                        saveItem.KTDuyetStatus = 'Approved';
                        saveItem.KTDuyetNote = vm.cacheDuyet.Note;
                    }
                }
            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin đơn hàng.", null, controllerId);

                    if (vm.ctDonHang.CurrentStep === 'KT' && vm.ctDonHang.Status ==='Approved') {
                        debugger
                        const NgayBan = moment(vm.ctDonHang.NgayBan),
                        NgayMuaCuoi = moment(KhachHangCopy.NgayMuaCuoi),
                        listName = "KhachHang";
                        if (!KhachHangCopy.NgayMuaCuoi || NgayMuaCuoi < NgayBan){
                            // Tạo obj model mới để lưu dữ liệu
                            let saveItem =  new ict24h.models.KhachHang();
                            saveItem = KhachHangCopy;
                            saveItem.NgayMuaCuoi = NgayBan;
                            debugger
                            datacontext.saveItem(listName, saveItem)
                                .then(function (data) {
                                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                                    common.logger.logSuccess("Đã lưu thông tin khách hàng.", null, controllerId);
                                    vm.btnSave = false;
                                })
                                .then(function () {
                                    $window.history.back();
                                })
                                .catch(function (error) {
                                    common.logger.logError('Lỗi không thể lưu thông tin khách hàng', error, controllerId);
                                    vm.btnSave = false;
                                });

                        } else {
                            $window.history.back();
                            vm.btnSave = false;
                        }
                    } else {
                        $window.history.back();
                        vm.btnSave = false;
                    }
                    
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin đơn hàng', error, controllerId);
                    vm.btnSave = false;
                });
        }





    }

})();