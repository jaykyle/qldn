(function () {
    'use strict';

    // define controller
    var controllerId = "dsNhaSanXuat";
    angular.module('app').controller(controllerId,
        ['$location', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($location, $routeParams, common, datacontext) {
        let vm = this;

        // đường dẫn để đến chi tiết item
        vm.gotoItem = gotoItem;
        // đường dẫn để thêm Item mới
        //vm.newItem = newItem;

        const listName = "NhaSanXuat";

        // init controller
        init();


        // navigate to the specified item
        function gotoItem(item) {
            if (item && item.Id) {
                $location.path('/nhasanxuat/' + item.Id );
            }
        }

        // #region private memebers
        // init controller
        function init() {
            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

            // Tải toàn bộ danh sách
            getListItems();
        }

        // Lấy danh sách tất cả
        function getListItems() {

            const select = "Id,Title,QuocGia";
            const expand = undefined; //'NhanVien,PhongBan';
            const filter = undefined;

            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data) {
                        vm.dsNhaSanXuat = data;
                    } else {
                        throw new Error('Lỗi không thể lấy danh sách nhà sản xuất');
                    }
                }).catch(function (error) {
                    common.logger.logError('Lỗi danh sách nhà sản xuất', error, controllerId);
                });
        }
        

        // Lọc bảng
        vm.sortColumn = 'Id';
        vm.reverse = true;
        vm.getSortClass = getSortClass;
        vm.sortData = sortData;
        // Hàm lọc bảng
        function sortData(column) {
            if (vm.sortColumn == column)
                vm.reverse = !vm.reverse;
            else
                vm.reverse = true;
            vm.sortColumn = column;
        }
        // Hàm đổi Class Icon
        function getSortClass(column) {
            if (vm.sortColumn == column) {
                return vm.reverse ? 'arrow-up cursor-pointer pull-right' : 'arrow-down cursor-pointer pull-right';
            }
            return 'fa fa-sort cursor-pointer pull-right';
        }
    };

})();