(function () {
    'use strict';

    // define controller
    var controllerId = "ctNhaSanXuat";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;

        vm.showDelBtn = false;

        vm.DsQuocGia = ['VIETNAM','SWITZERLAND','KOREA','POLAND','DOMINICAN REPUBLIC','UNITED KINGDOM','Australia','FRANCE','JAPAN','CHINA','MALAYSIA','PHILIPPINES','INDIA','CYPRUS','LITHUANIA','GREECE','GERMANY','USA','TAIWAN','ITALY','BELGIUM','HOLAND','PAKISTAN','HUNGARY','IRELAND','MEXICO','THAILAND','SLOVAKIA','BI']

        // if an ID is passed in, load the item
        const NhaSanXuatId = +$routeParams.idNhaSanXuat;

        const listName = "NhaSanXuat";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (NhaSanXuatId && +NhaSanXuatId > 0) {
                getItem(NhaSanXuatId);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctNhaSanXuat) {
                return moment(vm.ctNhaSanXuat.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctNhaSanXuat) {
                return moment(vm.ctNhaSanXuat.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.NhaSanXuat();
                saveItem = vm.ctNhaSanXuat;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin nhà sản xuất.", null, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin nhà sản xuất', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctNhaSanXuat)
                .then(function () {
                    common.logger.logSuccess("Đã xóa nhà sản xuất.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa nhà sản xuất', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctNhaSanXuat = new ict24h.models.NhaSanXuat();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Title,QuocGia";
            const expand = undefined;

            let itemObj = new ict24h.models.NhaSanXuat();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctNhaSanXuat = data;
                });
        }

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();