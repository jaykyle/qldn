(function () {
    'use strict';

    // define controller
    var controllerId = "ctDuAn";
    angular.module('app').
    controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', 'FileUploader', 'AttachmentSvc', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext, FileUploader, AttachmentSvc) {
        let vm = this;

        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        vm.deleteFile = deleteFile;

        vm.DsVanBan = {}; // Store list VanBan

        vm.showDelBtn = false;

        // if an ID is passed in, load the item
        const idItem = +$routeParams.idDuAn;

        const listName = "DuAn";

        vm.openCalendar = function (e, picker) {
            vm[picker].open = true;
        };
        vm.pickerNgayKy = {
            date: new Date()
        };
        vm.pickerNgayPhatHanh = {
            date: new Date()
        };
        vm.pickerNgayNop = {
            date: new Date()
        };
        vm.pickerNgayHetHan = {
            date: new Date()
        };
        // Tùy chọn cho định dạng input form
		vm.formFormat = {
			// Số đt
			phone: {
				phone: true,
				phoneRegionCode: 'VN'
			},
			// Dãy số
			number: {
				numeral: true
			},
			cmnd: {
				blocks: [3, 3, 3, 3, 3, 3],
				//prefix: 'PREFIX',
				//numeral: true,
				///delimiters: [' '],
			}
		}

        // initalize controller
        init();


        // initalize controller
        function init() {
            vm.TabsInfo = {};
            
            vm.TabsInfo.tabs = [
                { title:'Dynamic Title 1', content:'Dynamic content 1' },
                { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
              ];
            
            vm.TabsInfo.alertMe = function() {
                setTimeout(function() {
                  $window.alert('You\'ve selected the alert tab!');
                });
              };
            
            vm.TabsInfo.model = {
                name: 'Tabs'
              };
            // END TAB CONFIG
            
            if (idItem && +idItem > 0) {
                getItem(idItem);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            enbaleEdit(); // Call function check edit stage
        }

        // Hàm kiểm tra điều kiện cho phép chỉnh sửa
        function enbaleEdit(){
            if ($routeParams.action && $routeParams.action === 'edit') {
                vm.flagCanEdit = true;

                // Lấy danh sách từ list lọc  //listName,select,expand,filter
                getListExpand('PhongBan','Id,Title,Ma',null,null);
                getListExpand('CaNhan','Id,Title,Ma',null,null);
                getListExpand('NhomDuAn','Id,Title,Ma',null,null);
                getListExpand('LoaiCongTrinh','Id,Title,Ma',null,null);
                getListExpand('CapCongTrinh','Id,Title,Ma',null,null);
                getListExpand('TinhThanh','Id,Title,KhuVuc',null,null);
                getListExpand('DoiTac','Id,Title,Ma,LoaiDoiTac/Title','LoaiDoiTac',null);
                getSpWeb('SiteUsers','$select=Id,Title,Email&$filter=substringof(\'@ict24h.net\',Email)'); // Chỉ lấy tài khoản có email '@ict24h.net'
            } else {
                vm.flagCanEdit = false;

                // Get danh sách văn bản của dự án (tenDanhSach,listName,select,expand,filter)
                //getDsVanBan('ChuTruongDauTu','HoSoVanBan','Id,SoHieuVanBan,NgayKy,NoiDung,ToChucPhatHanh/Id,ToChucPhatHanh/Title','ToChucPhatHanh','((LoaiVanBanId eq 13) and DuAnId eq '+ idItem +')') //Loại van bản: Chủ trương lập dự án có ID: 13
                //getDsVanBan('PheDuyetKeHoachDauThauChuanBiDauTu','HoSoVanBan','Id,SoHieuVanBan,NgayKy,NoiDung,ToChucPhatHanh/Id,ToChucPhatHanh/Title','ToChucPhatHanh','((LoaiVanBanId eq 26) and DuAnId eq '+ idItem +')') //Loại van bản: Chủ trương lập dự án có ID: 13

                getDsVanBan();
                getListExpand('HoSoVanBan','Id,SoHieuVanBan,NgayKy,NoiDung,ToChucPhatHanh/Id,ToChucPhatHanh/Title,LoaiVanBan/Title,LoaiVanBanId','ToChucPhatHanh,LoaiVanBan','((NhomVanBanId ne null) and DuAnId eq '+ idItem +')');
                getListExpand('LoaiChiPhiDuAn','Id,Title,GiaTriDuAnDuocDuyet,GiaTriDuToanDuocDuyet',null,'((LoaiChiPhiId ne null) and DuAnId eq '+ idItem +')');
                getListExpand('NguonVon','Id,Title,Ma,KeHoachThu,KeHoachChi,ThucTeThu,ThucTeChi,SoDuVon,LoaiVon/Title','LoaiVon','(DuAnId eq '+ idItem +')');

            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.DuAn();
                saveItem = vm.ctDuAn;

                // Xóa các thuộc tính expand
                [
                    'ChuDauTu','NguoiQuyetDinhDauTu','ToChucThamDinhDuAn',
                    'DiaDiem','ToChucQuanLyDuAn','ToChucLapDuAn',
                    'ChuNhiemLapDuAn','NhomDuAn','LoaiCongTrinh','CapCongTrinh',
                    'Author','Editor','AttachmentFiles'
                ].forEach( (e) => delete saveItem[e] )

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    if (+vm.uploader.queue.length > 0){
                        const itemId = data.Id || data.d.Id;
                        saveUploadFile(null, listName, itemId)
                    } else {
                        // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                        common.logger.logSuccess("Đã lưu thông tin.", data, controllerId);
                        goBack();
                    }
                })
                .catch(function (error) {
                    common.logger.logError('Không thể lưu thông tin.', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Tham số listName, currentItem
                    datacontext.deleteItemOfList(listName, vm.ctDuAn)
                    .then(function (data) {
                        common.logger.logSuccess("Đã xóa thông tin.", data, controllerId);
                    })
                    .then(function () {
                        $window.history.back();
                    })
                    .catch(function (error) {
                        common.logger.logError('Không thể xóa.', error, controllerId);
                    });

                }
              })

            
        }

        // create a Item
        function createItem() {
            vm.ctDuAn = new ict24h.models.DuAn();
        }

        // load the item specified in the route
        function getItem(itemId) {

            // set array property fields item
            const select = [
                'Id','Title','Ma','Created','Modified',
                'LinhVuc','TinhTrang','NamThucHien','MoTa',
                'TienTrinh,DienTichDat,HinhThucQuanLy,PhuongAnGiaiPhongMatBang',
                'ChuDauTuId,ChuDauTu/Title,NguoiQuyetDinhDauTuId,NguoiQuyetDinhDauTu/Title',
                'ToChucThamDinhDuAnId,ToChucThamDinhDuAn/Title,DiaDiemId,DiaDiem/Title',
                'ToChucQuanLyDuAnId,ToChucQuanLyDuAn/Title,ToChucLapDuAnId,ToChucLapDuAn/Title',
                'ChuNhiemLapDuAnId,ChuNhiemLapDuAn/Title,NhomDuAnId,NhomDuAn/Title',
                'LoaiCongTrinhId,LoaiCongTrinh/Title,CapCongTrinhId,CapCongTrinh/Title',
                'Editor/Title,Author/Title',
                'AttachmentFiles/FileName,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileSize'
            ].join();
            const expand = [
                'ChuDauTu','NguoiQuyetDinhDauTu','ToChucThamDinhDuAn',
                'DiaDiem','ToChucQuanLyDuAn','ToChucLapDuAn',
                'ChuNhiemLapDuAn','NhomDuAn','LoaiCongTrinh','CapCongTrinh',
                'Author','Editor','AttachmentFiles'
            ].join();

            let itemObj = new ict24h.models.DuAn();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand).
            then(function (data) {
                vm.ctDuAn = data;
            }).
            catch(function (error) {
                common.logger.logError('Không thể lấy thông tin.', error, controllerId);
            });
        }

        // Hàm khai báo khi Document Ready
        angular.element(document).ready(function () {


        });

        /**
         * Lấy danh sách item từ list expand khi cần
         * @param {String} listName 
         * @param {String} select 
         * @param {String} expand 
         * @param {String} filter 
         */
        function getListExpand(listName,select,expand,filter){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        vm[listName] = data; 
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Lấy danh sách văn bản cho dự án
         * @param {String} listName,select,expand,filter
         * @param {String} tenDanhSach
         */
        function getDsVanBan(){
            const listName = 'HoSoVanBan',
            select ='Id,SoHieuVanBan,NgayKy,NoiDung,ToChucPhatHanh/Id,ToChucPhatHanh/Title,LoaiVanBan/Title,LoaiVanBanId',
            expand = 'ToChucPhatHanh,LoaiVanBan',
            filter = '((NhomVanBanId ne null) and (DuAnId eq '+ idItem +'))';
            
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getDsItem(listName, null, null, select, expand, filter)
                .then(function (data) {
                    if (data && data.length > 0) {
                        
                        for (let e of data){
                            e.TenLoaiVanBan = e.LoaiVanBan.Title;
                        }

                        vm.DsVanBan = groupBy(data, 'TenLoaiVanBan')

                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Get info from Sharepoint Web
         * @param {String} listName 
         * @param {String} query
         */
        function getSpWeb(listName,query){
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            datacontext.getSpWeb(listName,query)
                .then(function (data) {
                    if (data && data.results && data.results.length > 0) {
                        vm[listName] = data.results;
                        common.logger.log('Lấy dữ liệu thành công', data, controllerId);
                    }
                }).catch(function (error) {
                    common.logger.logError('Không thể lấy dữ liệu', error, controllerId);
                });
        }

        /**
         * Xử lý UPLOAD
         */
		vm.uploader = new FileUploader({
		    url: '' //url for file upload
        });
        
		vm.uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                
                //constraint for 10 files
                return this.queue.length < 10;
            }
        });

		//Code for SharePoint upload
		function saveUploadFile(event, listName, itemId) {
            if (+vm.uploader.queue.length > 0){
                const file = vm.uploader.queue[0]._file;
                //convert to arraybuffer
                AttachmentSvc.getFileBuffer(file)
                .then(function (data) {
                    if (data != null) {
                        
                        AttachmentSvc.saveFile(listName, itemId, data, file.name)
                        .then((data) => {
                            common.logger.log("Đã tải tệp tin.", data, controllerId);
                            // Lưu xong file thì xóa phần tử đầu tiên
                            vm.uploader.queue.shift();
                            // Chạy lại hàm
                            saveUploadFile(null, listName, itemId);
                        } )
                        .catch( (error) => {
                            common.logger.logError('Không thể upload tệp tin!', error, controllerId)
                            // Active Save button
                            vm.btnSave = false;
                        } );
                    }
                })
                .catch(function (err) {
                    common.logger.logError('Error on getting file array buffer', err, controllerId);
                    // Active Save button
                    vm.btnSave = false;
                });
            } else {
                // khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                common.logger.logSuccess("Lưu thông tin thành công", null, controllerId);
                goBack();
            }
            
        }
        
        // Handle deletel attachment file
        //delFile(listName, itemId, fileName)
        function deleteFile(fileName){
            swal({
                title: 'Bạn có chắc chắn muốn xóa?',
                text: "Sau khi xóa sẽ không thể khôi phục lại dữ liệu!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#dc3545',
                cancelButtonColor: '#6c757d',
                confirmButtonText: 'Đồng ý xóa!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {

                    // Khai báo tham số
                    const itemId = vm.ctDuAn.Id;
                    // Ẩn nút lưu
                    vm.btnSave = true;
                    AttachmentSvc.delFile(listName, itemId, fileName).
                    then((data) => {
                        // Khi xóa xong thì xuất thông tin
                        common.logger.logSuccess("Xóa tệp tin thành công", data, controllerId);
                        
                        // get item data
                        getItem(itemId);
                        // Active Save button
                        vm.btnSave = false;

                    } ).catch( (error) => {
                        common.logger.logError('Không thể xóa tệp tin!', error, controllerId)
                        // Active Save button
                        vm.btnSave = false;
                    } );

                }
            })
        }

        /**
         * Hàm nhóm kết quả theo một thuộc tính
         * @param {*} objectArray 
         * @param {*} property 
         */
        function groupBy(objectArray, property) {
            return objectArray.reduce(function (acc, obj) {
                var key = obj[property];
                if (!acc[key]) {
                    acc[key] = [];
                }
                acc[key].push(obj);
                return acc;
            }, {});
        }

        

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();