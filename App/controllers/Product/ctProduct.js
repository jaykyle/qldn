(function () {
    'use strict';

    // define controller
    var controllerId = "ctProduct";
    angular.module('app').controller(controllerId, ['$window', '$routeParams', 'common', 'datacontext', ctlFn]);

    // create controller
    function ctlFn($window, $routeParams, common, datacontext) {
        let vm = this;

       
        // utility method to convert universal time > local time using moment.js
        vm.created = localizedCreatedTimestamp;
        vm.modified = localizedModifiedTimestap;
        // navigate backwards in the history stack
        vm.goBack = goBack;
        // handle saves & deletes
        vm.goSave = goSave;
        vm.goDelete = goDelete;
        
        vm.getDsNhaSanXuat = getDsNhaSanXuat;

        vm.showDelBtn = false;
        vm.DsDVT = ['Viên','Gói','Hộp','Chai','Gram','Tube','Túi','Bộ','Cái','Ống','Lô','Sợi','Lít','Kg','Mét','Tép','Miếng','Cuộn','Bình','Thùng','Test','Viên','Gói'];

        vm.formFormat = {
            number : { numeral: true }
        }

        // if an ID is passed in, load the item
        const ProductId = +$routeParams.idProduct;

        const listName = "Product";

        // initalize controller
        init();


        // initalize controller
        function init() {

            if (ProductId && +ProductId > 0) {
                getItem(ProductId);
                vm.showDelBtn = true;
            } else {
                createItem();
            }

            // Lấy danh sách nhà sản xuất
            getDsNhaSanXuat();

            common.logger.log("controller loaded", null, controllerId);
            common.activateController([], controllerId);

        }

        // localized created time for the current item
        function localizedCreatedTimestamp() {
            if (vm.ctProduct) {
                return moment(vm.ctProduct.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // localized modified time for the current item
        function localizedModifiedTimestap() {
            if (vm.ctProduct) {
                return moment(vm.ctProduct.Created).format("M/D/YYYY h:mm A");
            } else {
                return '';
            }
        }

        // navigate backwards
        function goBack() {
            $window.history.back();
        }

        // handle save action
        function goSave() {
            vm.btnSave = true;

            // Tạo obj model mới để lưu dữ liệu
            let saveItem =  new ict24h.models.Product();
            
            saveItem.Ma = vm.ctProduct.Ma;
            saveItem.DVT = vm.ctProduct.DVT;
            saveItem.Title = vm.ctProduct.Title;
            saveItem.NhaSanXuatId = vm.ctProduct.NhaSanXuatId;
            
            saveItem.Gia = +vm.ctProduct.Gia;

            datacontext.saveItem(listName, saveItem)
                .then(function (data) {
                    // Sau khi lưu xong quy trình thì xuất thông báo lưu phiếu thành công
                    common.logger.logSuccess("Đã lưu thông tin sản phẩm.", null, controllerId);
                    vm.btnSave = false;
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể lưu thông tin sản phẩm', error, controllerId);
                    vm.btnSave = false;
                });
        }

        // handle delete action
        function goDelete() {

            // Tham số listName, currentItem
            datacontext.deleteItemOfList(listName, vm.ctProduct)
                .then(function () {
                    common.logger.logSuccess("Đã xóa sản phẩm.", null, controllerId);
                })
                .then(function () {
                    $window.history.back();
                })
                .catch(function (error) {
                    common.logger.logError('Lỗi không thể xóa sản phẩm', error, controllerId);
                });
        }

        // create a Item
        function createItem() {
            vm.ctNhanVien = new ict24h.models.Product();
        }

        // load the item specified in the route
        function getItem(itemId) {

            const select = "Id,Ma,Title,Gia,DVT,NhaSanXuatId";
            const expand = undefined;

            let itemObj = new ict24h.models.Product();


            // Tham số: listName, objItem, id, select, expand
            datacontext.getItemDetail(listName, itemObj, itemId, select, expand)
                .then(function (data) {
                    vm.ctProduct = data;
                });
        }

        // Lấy danh sách nhà sản xuất
        function getDsNhaSanXuat(keyword) {
			const listName = "NhaSanXuat",
				select = "Id,Title,QuocGia", //Id,Title,MaHang,GiaCongTy,GiaKenhMT
				itemLimit = 10;
			let filter = undefined;

			if (keyword && !keyword.Id) {
				filter = "(substringof('"+ keyword +"',MaHang)) or (substringof('"+ keyword +"',Title))";
			}
			// Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
			datacontext.getDsItem(listName, null, null, select, null, filter,null,null, itemLimit)
				.then( (data) => {
					if (data) {
						vm.dsNhaSanXuat = data;						
					} else {
						throw new Error('Lỗi không thể lấy danh sách nhà sản xuất');
					}
				})
				.catch( (error) => common.logger.logError('Lỗi danh sách nhà sản xuất', error, controllerId) );
		}

        // load all item type options
        // function getDsCoApDungLeTet() {
        // 	const choiceName = "CoApDungLeTet";
        // 	// Tham số: listName, choiceName
        // 	datacontext.getChoicesField(listName, choiceName)
        // 		.then(function (data) {
        // 			vm.dsCoApDungLeTet = data;
        // 		});
        // }

        





    }

})();