﻿(function () {
  'use strict';

  // define controller
  var controllerId = "dsUser";
  angular.module('app').controller(controllerId,
    ['$location', 'common', 'datacontext', dsUser]);

  // create controller
  function dsUser($location, common, datacontext) {
    var vm = this;

    // navigate to specified item
    vm.gotoItem = gotoItem;

    // init controller
    init();

    // load all learning paths
    getDsUser();

    // init controller
    function init() {
      common.logger.log("controller loaded", null, controllerId);
      common.activateController([], controllerId);
    }

    // navigate to specified item
    function gotoItem(danhMuc) {
      if (danhMuc && danhMuc.Id) {
        $location.path('/danhmuc/' + danhMuc.Id);
      }
    }
    
    // get learning paths & set to bindable collection on vm
    function getDsUser() {

      var listName = "siteusers";
      var query = "$select=Id,Title,Email";
      // Tham số: listName, query
      datacontext.getSpWeb(listName, query)
        .then(function (data) {
          if (data) {
            vm.dsUser = data.results;
          } else {
            throw new Error('error obtaining data');
          }
        })
        .catch(function (error) {
          common.logger.logError('error obtaining >> dsUser', error, controllerId);
        });
    }

  };
})();