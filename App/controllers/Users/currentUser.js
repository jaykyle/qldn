﻿(function () {
  'use strict';

  // define controller
  var controllerId = "currentUser";
  angular.module('app').controller(controllerId,
    ['$location', 'common', 'datacontext', currentUser]);

  // create controller
  function currentUser($location, common, datacontext) {
    var vm = this;
    vm.goBack = goBack;

    vm.findGroup = findGroup;
    vm.isGroup = isGroup;


    // init controller
    init();

    // load all learning paths
    getCurrentUser();

    // init controller
    function init() {
      common.logger.log("controller loaded", null, controllerId);
      common.activateController([], controllerId);
    }

    // get learning paths & set to bindable collection on vm
    function getCurrentUser() {

      var listName = "currentuser";
      var query = "$select=groups,Id,Title,Email&$expand=groups";
      // Tham số: listName, query
      datacontext.getSpWeb(listName, query)
        .then(function (data) {
          if (data) {
            vm.currentUser = data;

            /**
             * GHI CHÚ
             * chức vụ: 1.NV kinh doanh | 2.TP Kinh doanh | 3.Kế toán | 4.BGĐ | 5.Planning
             * Nhóm: KinhDoanh | TPKinhDoanh | KeToan | BGD | Planning
             */

            // Thêm danh mục menu
            if (data.Groups.results.find( e => e.Title ==='BGD' || 'Planning' || 'TpKinhDoanh' )){
              vm.currentUser.ShowMenuAdmin = true;
            } else {
              vm.currentUser.ShowMenuAdmin = false;
            }
            if (data.Groups.results.find( e => e.Title === 'Planning' )){
              vm.currentUser.ShowMenuBGDnPL = true;
            } else if (data.Groups.results.find( e => e.Title === 'BGD')) {
              vm.currentUser.ShowMenuBGDnPL = true;
            } else {
              vm.currentUser.ShowMenuBGDnPL = false;
            }
            if (data.Groups.results.find( e => e.Title === 'BGD' )){
              vm.currentUser.ShowMenuBGD = true;
            } else {
              vm.currentUser.ShowMenuBGD = false;
            }
            if (data.Groups.results.find( e => e.Title === 'KinhDoanh')){
              vm.currentUser.ShowMenuKD = true;
            } else {
              vm.currentUser.ShowMenuKD
            }
            if (data.Groups.results.find( e => e.Title === 'TpKinhDoanh' )){
              vm.currentUser.ShowMenuTPKD = true;
            } else {
              vm.currentUser.ShowMenuTPKD = false;
            }
            if (data.Groups.results.find( e => e.Title === 'KeToan' )){
              vm.currentUser.ShowMenuKT = true;
            } else {
              vm.currentUser.ShowMenuKT = false;
            }
            if (data.Groups.results.find( e => e.Title === 'Planning' )){
              vm.currentUser.ShowMenuPL = true;
            } else {
              vm.currentUser.ShowMenuPL = false;
            }

          } else {
            throw new Error('error obtaining data');
          }
        })
        .catch(function (error) {
          common.logger.logError('error obtaining >> Current User', error, controllerId);
        });
    }

    function goBack() {
      $location.path('/')
      common.logger.logSuccess("Bạn không có quyền truy cập trang này", null, controllerId);
    }

    /**
     * HÀM TÌM GROUP ĐƯỢC QUYỀN TRUY CẬP
     * Truyền vào arr group của user và arr các group đc quyền truy cập
     * Chạy vòng lặp for để truyền tên vào Arr
     * @param {Array} arrGroups 
     * @param {Array} nameToFind 
     */
    function findGroup(arrGroups, nameToFind) {
      if (arrGroups && arrGroups.length > 0 && nameToFind && nameToFind.length > 0) {
        vm.ResulFindGroup = [];
        for (let i = 0; i < nameToFind.length; i++ ) {
          let ketQuaSauKhiChay =  arrGroups.find((element) => element.Title === nameToFind[i]);
          // Nếu có tên group thì mới push và Array ResulFindGroup
          ketQuaSauKhiChay ? vm.ResulFindGroup.push( ketQuaSauKhiChay ) : '';
        }
        
      }
    }

    function isGroup(group, title) { 
      return group.Title === title;
    }

    function showMenuSlideKenhBan() {
      

    }

  };
})();