﻿/*
 * datacontext that uses the Anuglar $http service
 */

(function () {
    'use strict';

    // define factory
    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId, ['$rootScope', '$http', '$resource', '$q', 'config', 'common', 'spContext', datacontext]);

    function datacontext($rootScope, $http, $resource, $q, config, common, spContext) {
        // init factory
        init();

        // service public signature
        return {

            getDsItem: getDsItem,
            getChoicesField: getChoicesField,
            getSpWeb: getSpWeb,
            postSpFolder: postSpFolder,
            getSpSearch: getSpSearch,
            deleteItemOfList: deleteItemOfList,
            getItemDetail: getItemDetail,
            saveItem: saveItem,

        };

        // init service
        function init() {
            common.logger.log("service loaded", null, serviceId);
        }


        /****************
        Start write some code :D
        ****************/

        //Tạo function lấy list Web có cấu trúc URL riêng (ít dùng)
        function getSpWeb(listName, query) {
            let deferred = $q.defer();

            $http({
                method: 'GET',
                url: "_api/web/" + listName + "?" + query
            }).success((data) => {
                common.logger.log("retrieved SP Web via ngHttp", data, serviceId);
                deferred.resolve(data.d);
            }).error((error) => {
                var message = "data context ngHttp error: " + error.message;
                common.logger.logError(message, error, serviceId);
                deferred.reject(error);
            });
            return deferred.promise;
        };

        function postSpFolder(folderName, actionMethod, currentItem) {
            let deferred = $q.defer();

            // Tạo biến lưu tạm
            let _actionMethod = null;

            currentItem.__metadata ? '' : currentItem.__metadata = {};

            // thêm thuộc tính metadata cho item
            currentItem.__metadata.type = 'SP.Folder'

            if (actionMethod === 'update') {
                _actionMethod = {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json;odata=verbose;',
                        'Content-Type': 'application/json;odata=verbose;',
                        'X-RequestDigest': spContext.securityValidation,
                        'X-HTTP-Method': 'MERGE',
                        'If-Match': currentItem.__metadata.etag
                    },
                    body: currentItem,
                    url: "_api/web/GetFolderByServerRelativeUrl('/" + folderName + "')"
                }
            } else if (actionMethod === 'delete') {
                _actionMethod = {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json;odata=verbose;',
                        'Content-Type': 'application/json;odata=verbose;',
                        'X-RequestDigest': spContext.securityValidation,
                        'If-Match': '*'
                    },
                    url: "_api/web/GetFolderByServerRelativeUrl('/" + folderName + "')"
                }
            } else { // không thì sẽ là tạo mới
                _actionMethod = {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json;odata=verbose;',
                        'Content-Type': 'application/json;odata=verbose;',
                        'X-RequestDigest': spContext.securityValidation
                    },
                    body: {
                        'ServerRelativeUrl': folderName,
                        '__metadata': {
                            'type': 'SP.Folder'
                        }
                    },
                    url: "_api/web/GetFolderByServerRelativeUrl('TrainingVideos')"
                }
            }

            $http(_actionMethod).success((data) => {
                common.logger.log("retrieved SP Web via ngHttp", data, serviceId);
                deferred.resolve(data.d);
            }).error((error) => {
                var message = "data context ngHttp error: " + error.message;
                // common.logger.logError(message, error, serviceId);
                common.logger.log(message, error, serviceId);
                deferred.reject(error);
            });

            return deferred.promise;
        };

        //Tạo function lấy list Web có cấu trúc URL riêng (ít dùng)
        function getSpSearch(listName, query) {
            let deferred = $q.defer();

            $http({
                method: 'GET',
                url: "_api/search/query/?querytext='" + query + "'&querytemplate='Path:" + spContext.hostWeb.url + "/HelpDesk/Lists/" + listName + "/'"
            }).success((data) => {
                common.logger.log("nhận kết quả tìm kiếm qua ngHttp", data, serviceId);
                deferred.resolve(data.d);
            }).error((error) => {
                var message = "data context ngHttp error: " + error.message;
                common.logger.logError(message, error, serviceId);
                deferred.reject(error);
            });

            return deferred.promise;
        };


        // get the Sharepoint Item angular resource reference
        // Pragram: listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top
        function getSharepointItemResource(listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top) {

            // if a learning item is passed in...
            if (currentItem) {
                //  THEN if the item has an ID
                if (+currentItem.Id) {
                    //  THEN get the specific item
                    return $resource("_api/web/lists/getbytitle('" + listName + "')/items(:itemId)", {
                        itemId: currentItem.Id
                    }, {
                        get: {
                            method: 'GET',
                            params: {
                                '$select': select,
                                '$expand': expand,
                                '$filter': filter
                            },
                            headers: {
                                'Accept': 'application/json;odata=verbose'
                            }
                        },
                        post: {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json;odata=verbose;',
                                'Content-Type': 'application/json;odata=verbose;',
                                'X-RequestDigest': spContext.securityValidation,
                                'X-HTTP-Method': 'MERGE',
                                'If-Match': currentItem.__metadata.etag
                            }
                        },
                        delete: {
                            method: 'DELETE',
                            headers: {
                                'Accept': 'application/json;odata=verbose;',
                                'Content-Type': 'application/json;odata=verbose;',
                                'X-RequestDigest': spContext.securityValidation,
                                'If-Match': '*'
                            }
                        }
                    });
                } else {
                    //  ELSE creating item...
                    return $resource("_api/web/lists/getbytitle('" + listName + "')/items", {}, {
                        post: {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json;odata=verbose',
                                'Content-Type': 'application/json;odata=verbose;',
                                'X-RequestDigest': spContext.securityValidation
                            }
                        }
                    });
                }
            } else {
                // ELSE if an learning path ID filter is passed in, 
                if (danhMucIdFilter) {
                    //   THEN build the resource filtering for a specific learning path
                    //   ELSE create resource showing all items
                    return $resource("_api/web/lists/getbytitle('" + listName + "')/items", {}, {
                        get: {
                            method: 'GET',
                            params: {
                                '$select': select,
                                '$expand': expand,
                                '$filter': filter + ' eq ' + danhMucIdFilter,
                                '$orderby': orderby,
                                '$skiptoken': skip, // kiểm tra có giá trị Id của skip ko
                                '$top': top || 5000
                            },
                            headers: {
                                'Accept': 'application/json;odata=verbose;'
                            }
                        },
                    });
                } else {
                    // Đa phần trường hợp rơi vào trang Danh sách item
                    return $resource("_api/web/lists/getbytitle('" + listName + "')/items", {}, {
                        get: {
                            method: 'GET',
                            params: {
                                '$select': select,
                                '$expand': expand,
                                '$filter': filter,
                                '$orderby': orderby,
                                '$skiptoken': skip, // kiểm tra có giá trị Id của skip ko
                                '$top': top || 5000
                            },
                            headers: {
                                'Accept': 'application/json;odata=verbose;'
                            }
                        }
                    });
                }
            }
        }

        // get resourse to get Choices name available
        function getChoicesFieldResource(listName, choiceName) {
            return $resource('_api/web/lists/getbytitle(\'' + listName + '\')/Fields', {}, {
                get: {
                    method: 'GET',
                    params: {
                        '$select': 'Choices',
                        '$filter': 'InternalName eq \'' + choiceName + '\'',
                        '$orderBy': 'Choices'
                    },
                    headers: {
                        'Accept': 'application/json;odata=verbose;'
                    }
                }
            });
        }

        // Danh sách Item trong list Sharepoint 
        function getDsItem(listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top) {

            // get resource
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            let resource = getSharepointItemResource(listName, currentItem, danhMucIdFilter, select, expand, filter, orderby, skip, top);

            let deferred = $q.defer();
            resource.get({}, (data) => {
                deferred.resolve(data.d.results);
                common.logger.log("Đã nhận thông tin chi tiết danh sách items", data, serviceId);
            }, (error) => {
                deferred.reject(error);
                // common.logger.logError("Lỗi: không nhận được thông tin chi tiết danh sách items", error, serviceId);
                common.logger.log("Lỗi: không nhận được thông tin chi tiết danh sách items", error, serviceId);
            });
            return deferred.promise;
        }

        // Lấy tất cả giá trị của Choices Field trong Sharepoint
        function getChoicesField(listName, choiceName) {

            // get resource
            // Tham số: listName, choiceName
            let resource = getChoicesFieldResource(listName, choiceName);

            let deferred = $q.defer();
            resource.get({}, (data) => {
                deferred.resolve(data.d.results[0].Choices.results);
                // common.logger.log("Trả về kết quả của Choices fields", data, serviceId);
            }, (error) => {
                deferred.reject(error);
                // common.logger.logError("Lỗi không thể nhận danh sách của Choices fields", error, serviceId);
                common.logger.log("Lỗi không thể nhận danh sách của Choices fields", error, serviceId);
            });

            return deferred.promise;
        }

        // Hàm xóa 1 item trong lists
        function deleteItemOfList(listName, currentItem) {
            // get resource
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            let resource = getSharepointItemResource(listName, currentItem, null, null, null, null);

            let deferred = $q.defer();

            // use angular $resource to delete the item
            resource.delete(currentItem, (data) => {
                deferred.resolve(data);
                common.logger.log("Đã xóa Item ở trong lists", data, serviceId);
            }, (error) => {
                deferred.reject(error);
                common.logger.log("Lỗi: xóa Item ở trong list", error, serviceId);
                // common.logger.logError("Lỗi: xóa Item ở trong list", error, serviceId);
            });

            return deferred.promise;
        }

        /**
         * Hàm get chi tiết item
         * @param {String} listName 
         * @param {Object} objItem 
         * @param {Number} id 
         * @param {String} select 
         * @param {String} expand 
         */
        function getItemDetail(listName, objItem, id, select, expand) {
            let item = objItem;
            item.Id = id;

            // get resource
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            var resource = getSharepointItemResource(listName, item, null, select, expand);

            var deferred = $q.defer();
            resource.get({}, function (data) {
                deferred.resolve(data.d);
                common.logger.log("Nhận thông tin chi tiết Item", data, serviceId);
            }, function (error) {
                deferred.reject(error);
                // common.logger.logError("Lỗi không thể nhận thông tin Item", error, serviceId);
                common.logger.log("Lỗi không thể nhận thông tin Item", error, serviceId);
            });

            return deferred.promise;
        }

        // Save Item
        function saveItem(listName, saveItem) {
            // get resource
            // Tham số: listName, currentItem, danhMucIdFilter, select, expand, filter
            var resource = getSharepointItemResource(listName, saveItem, null, null, null, null);

            var deferred = $q.defer();

            resource.post(saveItem, function (data) {
                deferred.resolve(data);
                common.logger.log("Đã lưu thông tin Item", data, serviceId);
            }, function (error) {
                deferred.reject(error);
                // common.logger.logError("Lỗi: không thể lưu thông tin Item", error, serviceId);
                common.logger.log("Lỗi: không thể lưu thông tin Item", error, serviceId);
            });

            return deferred.promise;

        }



    }
})();