﻿(function () {
    'use strict'
    
  
    angular
      .module('app')
      .factory('ItemCount', ['$q', '$resource', '$window', '$http', 'spContext', function ($q, $resource, $window, $http, spContext) {
        var service,
          serviceId = 'ItemCount'; //name of document library
  
        service = {
            getSpCount: getSpCount
        };
  
        //Initialization
        function init() {
          console.log('Initiated..', serviceId);
        }
        init();
  
        return service;

        //save file to custom list
        function getSpCount(listName, select, expand, filter) {
            let selectQuery = '',
                filterQuery ='';

            let deferred = $q.defer();
            //POST request
            $http({
              method: 'GET',
              url: '_api/web/lists/getbytitle(\'' + listName + '\')/ItemCount'+ selectQuery + filterQuery,
              params: {
                '$select': select,
                '$expand': expand,
                '$filter': filter
                },
              headers: {
                'Accept': 'application/json;odata=verbose',
                'X-RequestDigest': spContext.securityValidation, //TODO: provide your digest value here,else it will throw security issues
              },
            }).then(function successCallback(data) {
              // this callback will be called asynchronously
              deferred.resolve(data);
              console.log('Successfully saved.', data, serviceId, false);
            }, function errorCallback(error) {
              // called asynchronously if an error occurs
              deferred.reject(error);
              console.log('Failed to save!!!.', error, serviceId, false);
            });
            return deferred.promise;
        }
  
        //save file to custom list
        function saveFile(listName, itemId, fileArrBuffer, fileName) {
          
          var deferred = $q.defer();
          //POST request
          $http({
            method: 'POST',
            url: '_api/web/lists/getbytitle(\'' + listName + '\')/items(' + itemId + ')/AttachmentFiles/add(FileName=\'' + fileName + '\')',
            headers: {
              'Accept': 'application/json;odata=verbose',
              'Content-Type': undefined,
              'X-RequestDigest': spContext.securityValidation, //TODO: provide your digest value here,else it will throw security issues
            },
            data: new Uint8Array(fileArrBuffer),
            transformRequest: []
          }).then(function successCallback(data) {
            // this callback will be called asynchronously
            deferred.resolve(data);
            console.log('Successfully saved.', data, serviceId, false);
          }, function errorCallback(error) {
            // called asynchronously if an error occurs
            deferred.reject(error);
            console.log('Failed to save!!!.', error, serviceId, false);
          });
          return deferred.promise;
        }
  
        // getting file array buffer
        function getFileBuffer(file) {
          var deferred = $q.defer();
          var reader = new $window.FileReader();
          reader.onloadend = function () {
            deferred.resolve(reader.result);
          }
          reader.onerror = function () {
            deferred.reject(reader.error);
          }
          reader.readAsArrayBuffer(file);
          return deferred.promise;
        }
      }]);
  })();