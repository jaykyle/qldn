(function () {
    'use strict';
    var serviceId = 'currentUser';
    var loggerSource = '[' + serviceId + '] ';
    angular.module('app').service(serviceId, [
        '$log', '$window', '$resource', '$rootScope', 'common', '$q', '$http', serviceFunction
    ]);

    function serviceFunction($log, $window, $resource, $rootScope, common, $q, $http) {
        var service = this;
        service.get = getCurrentUser;
        service.checkRole = checkRole;
        service.checkRoleNew = checkRoleNew;

        // init the service
        init();

        // init... akin to class constructor
        function init() {
            $log.log(loggerSource, 'service loaded', null);

            // Cứ gọi service này sẽ tự lấy current user lên $RootScope luôn
            getCurrentUser();
        }

        function getCurrentUser() {
            var listName = "CurrentUser";
            var query = "$select=Id,Title,Email,Groups/Id,Groups/Title&$expand=Groups";

            var deferred = $q.defer();

            $http({
                method: 'GET',
                headers: {
                    'Accept': 'application/json;odata=verbose',
                    'Content-Type': 'application/json;odata=verbose'
                },
                url: "_api/web/" + listName + "?" + query
            }).success(function (data) {
                common.logger.log("retrieved current user", data, serviceId);
                $rootScope.CurrentUser = data.d; // Set data cho RootScope
                deferred.resolve(data.d);
            }).error(function (error) {
                var message = "can't get current user: " + error.message;
                common.logger.logError(message, error, serviceId);
                deferred.reject(error);
            });
            return deferred.promise;
        }



        /**
         * Hàm kiểm tra quyền dựa trên tên groups
         * Mẹo tối ưu: checkRole nên để tên Group có nhiều user lên trước
         * @param {Array} checkRoles : Danh sách tên Group đc quyền truy cập
         * @param {Array} userGroups : Danh sách Group của user hiện tại
         */
        function checkRole(checkRoles, userGroups) { //checkRoles, userGroups
            var a = false;
            // Kiểm tra xem có info chưa,nếu chưa lấy thông tin rồi chạy hàm lại
            for (var i in checkRoles) {
                for (var g in userGroups) {
                    if (checkRoles[i] === userGroups[g].Title) {
                        return true;
                        // Không muốn ngắt để tối ưu mà cần lấy tên thì: a = userGroups[g].Title
                    }
                }
            }
            return a;
        }

        /**
         * Đầu tiên kiểm tra trên $RootScope đã có thông tin Current User chưa???
         */
        function checkRoleNew(thisUser, supAd, groups) {
            if (!$rootScope.CurrentUser) {
                getCurrentUser()
                .then( function(data) {
                    common.logger.log("retrieved current user", data, serviceId);
                    checkRoleAfterCheck(thisUser, supAd, groups);
                })
                .catch(function(error){
                    common.logger.logError(message, error, serviceId);
                })
            } else {
                checkRoleAfterCheck(thisUser, supAd, groups);
            }

        }


        /**
         * Hàm check quyền mới
         * Hàm viết như thế này nhưng vẫn chạy tuần tự từ trên xuống
         */
        function checkRoleAfterCheck(thisUser, supAd, groups) {
            let cU = $rootScope.CurrentUser;
            // B1. Kiểm tra xem có cần quyền User hiện tại không
            if (thisUser && +thisUser > 0 && +cU.Id === +thisUser) {
                return true;
            }
            // B2. Kiểm tra cU có nằm trong nhóm SupperAdmin không
            if (supAd && Array.isArray(supAd)) {
                for (let i in supAd) {
                    if (+cU.Id === +supAd[i]) {
                        return true;
                    }
                }
            }
            // B3. Kiểm tra cU có nằm trong group Phòng ban không
            if (groups && Array.isArray(groups)) {
                for (let j in groups) {
                    if (+cU.Groups.Id === +groups[j]) {
                        return true;
                    }
                }
            }

        }

    }
})();