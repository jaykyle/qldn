﻿(function () {
  'use strict'
  

  angular
    .module('app')
    .factory('AttachmentSvc', ['$q', '$resource', '$window', '$http', 'spContext', function ($q, $resource, $window, $http, spContext) {
      var service,
        serviceId = 'AttachmentSvc'; //name of document library

      service = {
        saveFile: saveFile,
        getFileBuffer: getFileBuffer,
        delFile: delFile
      };

      //Initialization
      function init() {
        console.log('Initiated..', serviceId);
      }
      init();

      return service;

      //save file to custom list
      function saveFile(listName, itemId, fileArrBuffer, fileName) {
        
        var deferred = $q.defer();
        //POST request
        $http({
          method: 'POST',
          url: '_api/web/lists/getbytitle(\'' + listName + '\')/items(' + itemId + ')/AttachmentFiles/add(FileName=\'' + fileName + '\')',
          headers: {
            'Accept': 'application/json;odata=verbose',
            'Content-Type': undefined,
            'X-RequestDigest': spContext.securityValidation, //TODO: provide your digest value here,else it will throw security issues
          },
          data: new Uint8Array(fileArrBuffer),
          transformRequest: []
        }).then(function successCallback(data) {
          // this callback will be called asynchronously
          deferred.resolve(data);
          console.log('Successfully saved.', data, serviceId, false);
        }, function errorCallback(error) {
          // called asynchronously if an error occurs
          deferred.reject(error);
          console.log('Failed to save!!!.', error, serviceId, false);
        });
        return deferred.promise;
      }

      // getting file array buffer
      function getFileBuffer(file) {
        var deferred = $q.defer();
        var reader = new $window.FileReader();
        reader.onloadend = function () {
          deferred.resolve(reader.result);
        }
        reader.onerror = function () {
          deferred.reject(reader.error);
        }
        reader.readAsArrayBuffer(file);
        return deferred.promise;
      }

      //handel delete attach file
      function delFile(listName, itemId, fileName) {
        
        var deferred = $q.defer();
        //POST request
        $http({
          method: 'DELETE',
          url: "_api/web/lists/GetByTitle('" + listName + "')/GetItemById(" + itemId + ")/AttachmentFiles/getByFileName('" + fileName + "')",
          headers: {
            'Accept': 'application/json;odata=verbose',
            'X-HTTP-Method': 'DELETE', 
            'X-RequestDigest': spContext.securityValidation, 
          },
        }).then(function successCallback(data) {
          // this callback will be called asynchronously
          deferred.resolve(data);
          console.log('Successfully deltelte file.', data, serviceId, false);
        }, function errorCallback(error) {
          // called asynchronously if an error occurs
          deferred.reject(error);
          console.log('Failed to delete file!!!.', error, serviceId, false);
        });
        return deferred.promise;
      }



      
    }]);
})();