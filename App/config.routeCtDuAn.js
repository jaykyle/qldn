(function () {
    'use strict';
  
    var app = angular.module('app');
  
    // get all the routes
    app.constant('routes', getRoutes());
  
    // config routes & their resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);
  
    function routeConfigurator($routeProvider, routes) {
      routes.forEach(function (route) {
        $routeProvider.when(route.url, route.config);
      });
  
      $routeProvider.otherwise({ redirectTo: '/' });
    }
  
    // build the routes
    function getRoutes() {
      return [
        {
          url: '/',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/DuAn/xemDuAn.html',
            title: 'Dự án',
            settings: {
              nav: 0,
              content: 'Dự án',
              icon: 'folder_special',
              quickLaunchEnabled: true
            }
          }
        },{
          url: '/duan',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/DuAn/xemDuAn.html',
            title: 'Thông tin dự án',
            settings: {
              nav: 1,
              content: 'Thông tin dự án',
              icon: 'folder_special',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/duan/:idDuAn/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DuAn/ctDuAn.html',
            title: 'Thông tin dự án',
            settings: {
              nav: 1,
              content: 'Thông tin dự án',
              icon: 'folder_special',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/duan/:idDuAn/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DuAn/xemDuAn.html',
            title: 'Thông tin dự án',
            settings: {
              nav: 1,
              content: 'Thông tin dự án',
              icon: 'folder_special',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/hosovanban',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/HoSoVanBan/dsHoSo.html',
            title: 'Hồ sơ văn bản',
            settings: {
              nav: 1,
              content: 'Hồ sơ văn bản',
              icon: 'all_inbox',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/hosovanban/:idHoSoVanBan/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/HoSoVanBan/ctHoSo.html',
            title: 'Hồ sơ văn bản',
            settings: {
              nav: 1,
              content: 'Hồ sơ văn bản',
              icon: 'all_inbox',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/hosovanban/:idHoSoVanBan/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/HoSoVanBan/xemHoSo.html',
            title: 'Hồ sơ văn bản',
            settings: {
              nav: 1,
              content: 'Hồ sơ văn bản',
              icon: 'all_inbox',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/chiphidutoan',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/ChiPhiDuToan/dsChiPhiDuToan.html',
            title: 'Chi phí dự toán',
            settings: {
              nav: 4,
              content: 'Chi phí dự toán',
              icon: 'monetization_on',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/chiphidutoan/:idChiPhiDuToan/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/ChiPhiDuToan/ctChiPhiDuToan.html',
            title: 'Chi tiết chi phí dự toán',
            settings: {
              nav: 1,
              content: 'Chi tiết chi phí dự toán',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/chiphidutoan/:idChiPhiDuToan/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/ChiPhiDuToan/xemChiPhiDuToan.html',
            title: 'Chi tiết chi phí dự toán',
            settings: {
              nav: 1,
              content: 'Chi tiết chi phí dự toán',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/loaichiphiduan',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/LoaiChiPhiDuAn/dsLoaiChiPhi.html',
            title: 'Loại chi phí',
            settings: {
              nav: 5,
              content: 'Loại chi phí',
              icon: 'shop_two',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/loaichiphiduan/:idLoaiChiPhiDuAn/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/LoaiChiPhiDuAn/ctLoaiChiPhi.html',
            title: 'Chi tiết loại chi phí',
            settings: {
              nav: 1,
              content: 'Chi tiết loại chi phí',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/loaichiphiduan/:idLoaiChiPhiDuAn/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/LoaiChiPhiDuAn/xemLoaiChiPhi.html',
            title: 'Chi tiết loại chi phí',
            settings: {
              nav: 1,
              content: 'Chi tiết loại chi phí',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/khaosatthietke',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KhaoSatThietKe/dsKhaoSatThietKe.html',
            title: 'Khảo sát thiết kế',
            settings: {
              nav: 4,
              content: 'Khảo sát thiết kế',
              icon: 'perm_media',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/giaiphongmatbang',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/GiaiPhongMatBang/GiaiPhongMatBang.html',
            title: 'Giải phóng mặt bằng',
            settings: {
              nav: 5,
              content: 'Giải phóng mặt bằng',
              icon: 'domain',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/hopdong',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/HopDong/dsHopDong.html',
            title: 'hợp đồng',
            settings: {
              nav: 4,
              content: 'Hợp đồng',
              icon: 'chrome_reader_mode',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/hopdong/:idHopDong/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/HopDong/ctHopDong.html',
            title: 'Chi tiết hợp đồng',
            settings: {
              nav: 1,
              content: 'Chi tiết hợp đồng',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/hopdong/:idHopDong/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/HopDong/xemHopDong.html',
            title: 'Chi tiết hợp đồng',
            settings: {
              nav: 1,
              content: 'Chi tiết hợp đồng',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/phuluchopdong',
          config: {
            templateUrl: cdnUrl + 'App/controllers/HopDong/PhuLuc/dsPhuLuc.html',
            title: 'phụ lục hợp đồng',
            settings: {
              nav: 4.1,
              content: 'Phụ lục hợp đồng',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/phuluchopdong/:idPhuLuc/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/HopDong/PhuLuc/ctPhuLuc.html',
            title: 'Chi tiết phụ lục hợp đồng',
            settings: {
              nav: 1,
              content: 'Chi tiết phụ lục hợp đồng',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/phuluchopdong/:idPhuLuc/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/HopDong/PhuLuc/xemPhuLuc.html',
            title: 'Chi tiết phụ lục hợp đồng',
            settings: {
              nav: 1,
              content: 'Chi tiết phụ lục hợp đồng',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/generalctrl/',
          config: {
            templateUrl: cdnUrl + 'App/controllers/General/listItem.html',
            title: 'General Controller List',
            settings: {
              nav: 1,
              content: 'General Controller List',
              icon: 'face',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/generalctrl/:idItem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/General/detailItem.html',
            title: 'General Controller Detail',
            settings: {
              nav: 1,
              content: 'General Controller Detail',
              icon: 'face',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/khoiluongdutoan',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KhoiLuongDuToan/dsKhoiLuong.html',
            title: 'Khối lượng dự toán',
            settings: {
              nav: 1,
              content: 'Khối lượng dự toán',
              icon: 'table_chart',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/khoiluongdutoan/:idKhoiLuongDuToan/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KhoiLuongDuToan/xemKhoiLuong.html',
            title: 'Khối lượng dự toán',
            settings: {
              nav: 2,
              content: 'Khối lượng dự toán',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/khoiluongdutoan/:idKhoiLuongDuToan/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KhoiLuongDuToan/ctKhoiLuong.html',
            title: 'Khối lượng dự toán',
            settings: {
              nav: 2,
              content: 'Khối lượng dự toán',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/kehoachchonnhathau',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KeHoachChonNhaThau/dsKeHoach.html',
            title: 'Kế hoạch chọn nhà thầu',
            settings: {
              nav: 1,
              content: 'Kế hoạch chọn nhà thầu',
              icon: 'assignment_ind',
              quickLaunchEnabled: true
            }
          }
        },
        {
          url: '/kehoachchonnhathau/:idItem/xem',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KeHoachChonNhaThau/xemKeHoach.html',
            title: 'Kế hoạch chọn nhà thầu',
            settings: {
              nav: 2,
              content: 'Kế hoạch chọn nhà thầu',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        },
        {
          url: '/kehoachchonnhathau/:idItem/sua',
          config: {
            templateUrl: cdnUrl + 'App/controllers/DetailProject/KeHoachChonNhaThau/ctKeHoach.html',
            title: 'Kế hoạch chọn nhà thầu',
            settings: {
              nav: 2,
              content: 'Kế hoạch chọn nhà thầu',
              icon: 'account_box',
              quickLaunchEnabled: false
            }
          }
        }
  
  
  
      ];
    }
  })();