(function () {
    'use strict';
    const app = angular.module('app');
    
    app.directive("tableSaw", ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element, attr) {
                if($scope.$last === true) {
                    $timeout(function () {
                        $(document).trigger("enhance.tablesaw");
                    });


                    
                }
                $scope.$watch('item', function () {
                    $timeout(function () {
                        $(document).trigger("enhance.tablesaw");
                    }, 1000);

                    
                })
            } // end link
        };
    }]);
})();